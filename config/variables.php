<?php

return [
    'shared' => [
        'yesno' => [
            '1' => 'EVET',
            '0' => 'HAYIR',
        ],
        'question_types' => [
            '0' => 'DÜZ METİN',
            '4' => 'SEÇENEK KUTUSU',
        ],
        'working_status' => [
            'Çalışıyor' => 'EVET',
            'Çalışmıyor' => 'HAYIR',
        ],
        'gender' => [
            'Kadın' => 'KADIN',
            'Erkek' => 'ERKEK',
        ],
        'education' => [
            'Yüksek Lisans' => 'Yüksek Lisans',
            'Lisans' => 'Lisans',
            'Önlisans' => 'Önlisans',
            'Lise' => 'Lise',
            'Orta' => 'Orta',
            'İlkÖğretim' => 'İlkÖğretim',
            'İlk' => 'İlk'
        ],

        'roles' => [
            'superadmin' => 'Süper Admin',
            'admin' => 'Yönetici',
            'teamleader' => 'Takım Lideri',
            'employee' => 'Çalışan',
        ]
    ],
    'educations' => [
        'is_required' => [
            '1' => 'EVET',
            '0' => 'HAYIR',
            '2' => 'KISMİ ZORUNLU',
        ],
        'is_required_form' => [
            '1' => 'EVET (TÜM ÇALIŞANLARA)',
            '0' => 'HAYIR (HİÇBİR ÇALIŞANA)',
            '2' => 'KISMİ ZORUNLU (BAZI ÇALIŞANLARA)',
        ],
        'is_required_label' => [
            '1' => '<span class="label label-important">EVET</span>',
            '0' => '<span class="label label-success">HAYIR</span>',
            '2' => '<span class="label label-warning">KISMİ ZORUNLU</span>',
        ],

        'terms' => [
            '' => 'Seçiniz',
            '1' => 'SADECE 1 KERE',
            '2' => 'AYDA BİR',
            '3' => '3 AYDA 1 KERE',
            '4' => '6 AYDA 1 KERE',
            '5' => 'YILDA 1 KERE',
            '6' => '3 YILDA 1 KERE',
            '7' => '5 YILDA 1 KERE',

        ],
    ],
    'education_plans' => [

        'status' => [
            '0' => 'PLANLANIYOR',
            '1' => 'ONAYLANDI',
            '2' => 'TAMAMLANDI',
            '3' => 'İPTAL',
        ],

        'status_label' => [
            '0' => '<span class="label label-warning">PLANLANIYOR</span>',
            '1' => '<span class="label label-info">ONAYLANDI</span>',
            '2' => '<span class="label label-success">TAMAMLANDI</span>',
            '3' => '<span class="label label-danger">İPTAL</span>',
        ],

    ]
];
