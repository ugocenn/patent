<?php

return [
    /*
    |----------------------------------------------------------------------------
    | Google application name
    |----------------------------------------------------------------------------
    */
    'application_name' => env('GOOGLE_APPLICATION_NAME', ''),

    /*
    |----------------------------------------------------------------------------
    | Google OAuth 2.0 access
    |----------------------------------------------------------------------------
    |
    | Keys for OAuth 2.0 access, see the API console at
    | https://developers.google.com/console
    |
    */

    //'client_id'        => env('GOOGLE_CLIENT_ID', '888310537222-9gpg6f8qn0es26bdafnpb4ob32q3pe8d.apps.googleusercontent.com'),
    'client_id'        => env('GOOGLE_CLIENT_ID', '818939644964-sgus6de3gcvunm4fike94sdpeea9j1cg.apps.googleusercontent.com'),
    //'client_secret'    => env('GOOGLE_CLIENT_SECRET', 'EmASygXv1LTb3_NoIXp5BvVM'),
    'client_secret'    => env('GOOGLE_CLIENT_SECRET', 'uRVeaQ5NwGPLgMJROAo8QDGH'),
    'redirect_uri'     => env('GOOGLE_REDIRECT', ''),
    'scopes'           => [\Google_Service_Sheets::DRIVE, \Google_Service_Sheets::SPREADSHEETS],
    'access_type'      => 'online',
    'approval_prompt'  => 'auto',
    'prompt'           => 'consent', //"none", "consent", "select_account" default:none

    // or Service Account
    'file'    => storage_path('patentsorgucanli12681-f1a07b440e29.json'),
    'GOOGLE_SERVICE_ACCOUNT_JSON_LOCATION'    => storage_path('patentsorgucanli12681-f1a07b440e29.json'),
    'enable'  => env('GOOGLE_SERVICE_ENABLED', true),



    /*
    |----------------------------------------------------------------------------
    | Google developer key
    |----------------------------------------------------------------------------
    |
    | Simple API access key, also from the API console. Ensure you get
    | a Server key, and not a Browser key.
    |
    */
    'developer_key' => env('GOOGLE_DEVELOPER_KEY', ''),

    /*
    |----------------------------------------------------------------------------
    | Google service account
    |----------------------------------------------------------------------------
    |
    | Set the credentials JSON's location to use assert credentials, otherwise
    | app engine or compute engine will be used.
    |
    */
    'service' => [
        /*
        | Enable service account auth or not.
        */
        'enable' => env('GOOGLE_SERVICE_ENABLED', false),

        /*
         * Path to service account json file. You can also pass the credentials as an array
         * instead of a file path.
         */
        'file'   => env('GOOGLE_SERVICE_ACCOUNT_JSON_LOCATION', storage_path('sheets-service-account.json')),
    ],

    /*
    |----------------------------------------------------------------------------
    | Additional config for the Google Client
    |----------------------------------------------------------------------------
    |
    | Set any additional config variables supported by the Google Client
    | Details can be found here:
    | https://github.com/google/google-api-php-client/blob/master/src/Google/Client.php
    |
    | NOTE: If client id is specified here, it will get over written by the one above.
    |
    */
    'config' => [],
];
