<?php
Route::get('sitemap.xml','App\Http\Controllers\SitemapController@index');
Route::get('/excel-cron', 'App\Http\Controllers\CronController@excel');
Route::get('/ajax/ajax-set-application-data/', 'App\Http\Controllers\AjaxController@setData')->name('ajax-application-store');
Route::post('/ajax/create-basket/', 'App\Http\Controllers\AjaxController@createBasket')->name('create-basket');
Route::post('/ajax/update-basket/', 'App\Http\Controllers\AjaxController@updateBasket')->name('update-basket');


Route::post('/iiyzicoSonuc', 'App\Http\Controllers\BasvuruController@iyzicoSonuc')->name('iyzicoSonuc');
Route::get('/online/{type}/{visitor?}/{search_id?}', 'App\Http\Controllers\BasvuruController@index')->name('basvuru');
Route::post('/online/{type}', 'App\Http\Controllers\BasvuruController@basvuruStore')->name('basvuru.store');
Route::post('/checkout/{type}', 'App\Http\Controllers\BasvuruController@basvuruStore')->name('basvuru.store');
Route::get('/iyzico', 'App\Http\Controllers\BasvuruController@payment')->name('iyzico');

Route::get('/', 'App\Http\Controllers\HomeController@index')->name('index');

Route::get('/marka-sonuc/{iletisimizni}/{marka}/{visitor}/{search_id}', 'App\Http\Controllers\HomeController@marka_sonuc')->name('marka-sonuc');

Auth::routes();

Route::get('/home', 'App\Http\Controllers\HomeController@welcome')->name('home');



Route::middleware(['auth'])->group(function () {
    Route::resource('/users', 'App\Http\Controllers\UserController');
    Route::resource('/companies', 'App\Http\Controllers\CompanyController');
    Route::resource('/searchs', 'App\Http\Controllers\SearchController');
    Route::resource('/customers', 'App\Http\Controllers\CustomerController');
    Route::resource('/web-pages', 'App\Http\Controllers\PageController');
    Route::resource('/product-classes', 'App\Http\Controllers\ProductClassController');
    Route::resource('/sectors', 'App\Http\Controllers\SectorController');
    Route::resource('/services', 'App\Http\Controllers\ServiceController');
    Route::resource('/packages', 'App\Http\Controllers\PackageController');
    Route::resource('/package-types', 'App\Http\Controllers\PackageTypeController');
    Route::resource('/categories', 'App\Http\Controllers\CategoryController');

    Route::get('/options', 'App\Http\Controllers\OptionController@index')->name('option.index');
    Route::post('/options', 'App\Http\Controllers\OptionController@save')->name('option.save');
    Route::get('/videos', 'App\Http\Controllers\VideoController@index')->name('videos.index');
    Route::post('/videos', 'App\Http\Controllers\VideoController@save')->name('video.save');

});


Route::get('/rehberler', 'App\Http\Controllers\HomeController@rehberler')->name('rehberler');

//Route::get('/icerik/{section}/{slug}', 'App\Http\Controllers\PageController@showPage')->name('show-page');
Route::get('/{slug}', 'App\Http\Controllers\PageController@showPage')->name('show-page');


