<div class="row clearfix">
    <div class="col-xl-6">
        <div class="form-group form-group-default required">
            <label>Marka</label>
            {!! Form::text("mark",null,['class'=>'form-control','required'=>'required','id'=>'mark']) !!}
        </div>
    </div>
    <div class="col-xl-6">
        <div class="form-group form-group-default required">
            <label>Sektör</label>
            {!! Form::text("sector",null,['class'=>'form-control','required'=>'required','id'=>'sector']) !!}
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-xl-6">
        <div class="form-group form-group-default required">
            <label>İsim Soyisim</label>
            {!! Form::text("name",null,['class'=>'form-control','required'=>'required','id'=>'name']) !!}
        </div>
    </div>
    <div class="col-xl-6">
        <div class="form-group form-group-default required">
            <label>Telefon</label>
            {!! Form::text("phone",null,['class'=>'form-control','required'=>'required','id'=>'phone']) !!}
        </div>
    </div>
</div>
