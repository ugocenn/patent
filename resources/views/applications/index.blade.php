@extends('layouts.app')
@section('title', 'Marka Başvuruları')
@section('content')
    <div class="content ">
        <br/>
        <!-- START CONTAINER FLUID -->
        <div class=" container-fluid   container-fixed-lg">
            <!-- START card -->
            <div class="card card-default">
                <div class="card-header ">
                    <div class="card-title">Marka Başvuruları
                    </div>
                    <div class="pull-right">
                        <a aria-label="" href="{{ route('applications.create') }}" class="btn btn-success btn-icon-left m-b-10" type="button"><i class="pg-icon">plus</i><span class="">Yeni Başvuru Oluştur</span></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                    @include('partials.alerts.error')
                    <div class="table-responsive">
                        <table id="datatable" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Marka</th>
                                <th>Sektör</th>
                                <th>İsim Soyisim</th>
                                <th>Telefon</th>
                                <th>İşlemler</th>
                            </tr>
                            </thead>


                            <tbody>
                            @foreach($applications as $application)
                            <tr>
                                <td>{{ $application->mark }}</td>
                                <td>{{ $application->sector }}</td>
                                <td>{{ $application->name }}</td>
                                <td>{{ $application->phone }}</td>


                                <td>
                                    <a href="{{ route('applications.edit', createHashId($application->id)) }}" aria-label="" type="button" class="btn btn-primary btn-cons btn-animated from-left">
                                        <span>Düzenle</span>
                                        <span class="hidden-block">
													<i class="pg-icon">edit</i>
												</span>
                                    </a>
                                </td>
                            </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END card -->
        </div>
        <!-- END CONTAINER FLUID -->
    </div>

@endsection
