@extends('frontend.app')
@section('title','Patent Sorgu')

@section('content')
    <div class="bg-img-hero-content rehber-hero rehber-no-header-hero" style="background-image: url({{asset('images/bg.jpg')}});">
        <div class="d-lg-flex align-items-lg-center title-container">
            <div class="container position-relative space-2 space-0--lg mt-lg-8">
                <div class="row justify-content-lg-between ">
                    <h1 class="hero-h1 text-white text-center mb-0 w-100 ">Ödeme Sonucu</h1>
                    <span class="hero-lead d-block text-center text-white w-100 mt-5 mx-4 mx-md-0 mt-md-4">

                    </span>
                </div>

            </div>
        </div>
    </div>
    <section class="container  pt-5 pb-5" style="max-width: 960px">
        <div class="row">
            <div class="col-12 content-row">

                <div class="d-flex flex-row" style="width: 584px;height: 326px;padding-top:50px; padding-left:47px;padding-right:57px;border-radius: 20px;background: rgba(255, 255, 255, 0.8);box-shadow: 0px 0px 10px rgba(5, 5, 5, 0.15), inset 1px 1px 1px #FFFFFF;backdrop-filter: blur(20px);-webkit-backdrop-filter: blur(20px);">
                    <div class="sonuc-svg">
                        <img src="{{asset('images/error-unlem.svg')}}">
                    </div>
                    <div class="d-flex flex-column" style="margin-left: 49px;">
                        <span class="d-block" style="font-size: 24px;line-height: 27px;font-weight: 600">Bir sorun var!<br>Ödeme Gerçekleşmedi</span>
                        <span class="d-block" style="font-size: 17px;line-height: 19px;font-weight: 600;margin-top: 45px;">Uzmanlarımız yine de sizlere en yakın sürede dönüş sağlayacak!</span>
                        <span class="d-block" style="font-size: 15px;line-height: 22px;margin-top: 18px;">Yaşadığınız sorun ile ilgili sizinle iletişimde olacağız!</span>
                    </div>
                </div>

                {!! $checkoutForm->getErrorMessage() !!}

            </div>
        </div>
    </section>

@endsection
