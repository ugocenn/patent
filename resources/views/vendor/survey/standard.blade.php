<div class="card">
    <div class="card-header bg-white p-4">
        <h2 class="mb-0">{{ $survey->name }}</h2>

        @if(!$eligible)
            Çalışan başına yalnızca <strong>{{ $survey->limitPerParticipant() }} </strong> girişi kabul ediyoruz.
        @endif

        @if($lastEntry)
            Cevaplarınızı en son <strong>{{ $lastEntry->created_at->diffForHumans() }}</strong> gönderdiniz.
        @endif

    </div>
    @foreach($survey->sections as $section)
        @include('survey::sections.single')
    @endforeach

    @foreach($survey->questions()->withoutSection()->get() as $question)
        @include('survey::questions.single')
    @endforeach

    @if($eligible)
        <button class="btn btn-primary">KAYDET</button>
    @endif
</div>
