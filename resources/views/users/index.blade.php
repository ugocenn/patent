@extends('layouts.app')
@section('title', 'Sistem Kullanıcıları')
@section('content')
    <div class="content ">
        <br/>
        <!-- START CONTAINER FLUID -->
        <div class=" container-fluid   container-fixed-lg">
            <!-- START card -->
            <div class="card card-default">
                <div class="card-header ">
                    <div class="card-title">Sistem Kullanıcıları
                    </div>
                    <div class="pull-right">
                        <a aria-label="" href="{{ route('users.create') }}" class="btn btn-success btn-icon-left m-b-10" type="button"><i class="pg-icon">plus</i><span class="">Yeni Sistem Kullanıcısı Ekle</span></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                    @include('partials.alerts.error')
                    <div class="table-responsive">
                        <table id="datatable" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>E-posta</th>
                                <th>Yetkisi</th>
                                <th>İşlemler</th>
                            </tr>
                            </thead>


                            <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td>
                                    <div class="thumbnail-wrapper d32 circular">
                                        <img width="40" height="40" src="{{ url('storage/'.$user->pictureLink) }}" data-src="{{ url('storage/'.$user->pictureLink) }}" data-src-retina="{{ url('storage/'.$user->pictureLink) }}" alt="">
                                    </div>
                                </td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ config('variables.shared.roles')[$user->role] }}</td>
                                <td>
                                    <a href="{{ route('users.edit', createHashId($user->id)) }}" aria-label="" type="button" class="btn btn-primary btn-cons btn-animated from-left">
                                        <span>Düzenle</span>
                                        <span class="hidden-block">
													<i class="pg-icon">edit</i>
												</span>
                                    </a>

                                </td>
                            </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END card -->
        </div>
        <!-- END CONTAINER FLUID -->
    </div>

@endsection
