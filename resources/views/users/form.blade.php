<div class="row clearfix">
    <div class="col-xl-6">
        <div class="form-group form-group-default required">
            <label>Adı Soyadı</label>
            {!! Form::text("name",null,['class'=>'form-control','required'=>'required','id'=>'name']) !!}
        </div>
    </div>
    <div class="col-xl-6">
        <div class="form-group form-group-default required">
            <label>E-posta</label>
            {!! Form::text("email",null,['class'=>'form-control','required'=>'required','id'=>'email', 'placeholder' => 'Sisteme giriş e-posta adresi']) !!}
        </div>
    </div>
</div>
@if(!isset($user))
<div class="row clearfix">
    <div class="col-xl-6">
        <div class="form-group form-group-default required">
            <label>Parola</label>
            <input type="password" class="form-control" name="password" required placeholder="En az 6 karakter">
        </div>
    </div>
    <div class="col-xl-6">
        <div class="form-group form-group-default">
            <label>Parola Onayla</label>
            <input type="password" class="form-control" name="password_confirmation" required placeholder="En az 6 karakter">
        </div>
    </div>
</div>
@endif
<div class="row clearfix">
    <div class="col-xl-6">
        <div class="form-group form-group-default form-group-default-select2">
            <label>Kullanıcı Rolü</label>
            {!! Form::select("role", $roles, null, ['data-placeholder' => 'Rol seçiniz', 'class'=>'full-width','id'=>'role','required'=>'required', 'data-init-plugin' => 'select2']) !!}
        </div>
    </div>
    <div class="col-xl-6">
        <div class="form-group form-group-default form-group-default-select2">
            <label>Aktif?</label>
            {!! Form::select("active", config('variables.shared.yesno'), null, ['data-placeholder' => 'Durum seçiniz', 'class'=>'full-width','id'=>'active','required'=>'required', 'data-init-plugin' => 'select2']) !!}
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-xl-12">
        <div class="form-group form-group-default">
            <label>Profil Resmi (İzin verilen: gif, png, jpg. En fazla 2Mb)</label>
            <input type="file" class="file-styled" name="pic_path" id="pic_path">
        </div>
    </div>
</div>
