@extends('layouts.app')
@section('title', 'Sistem Kullanıcısı Ekle')
@section('content')
    <div class="content ">
        <br/>
        <!-- START CONTAINER FLUID -->
        <div class=" container-fluid   container-fixed-lg">
            <!-- START card -->
            {!! Form::open(['route' => 'users.store','id'=>'admin-form','role'=>'form','autocomplete'=>'off', 'novalidate' => 'novalidate', 'files' => true])!!}
            <div class="card card-default">
                <div class="card-header ">
                    <div class="card-title">Sistem Kullanıcıları
                    </div>
                    <div class="pull-right">
                        <button aria-label="" class="btn btn-success btn-icon-left m-b-10" type="submit"><i class="pg-icon">tick</i><span class=""> KAYDET</span></button>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                    @include('partials.alerts.error')
                    <p class="fs-16 mw-80 m-b-40">Platforma yeni sistem kullanıcısı eklemek için aşağıdaki formu doldurabilirsiniz. Kullanıcı rolü olarak Yönetici, takım lideri veya çalışan değerlerinden birini seçmelisiniz. </p>
                    @include('users.form')
                </div>
            </div>
            {!! Form::close() !!}
            <!-- END card -->
        </div>
        <!-- END CONTAINER FLUID -->
    </div>

@endsection
@section('scripts')
    <script>
        (function($) {

            'use strict';

            $(document).ready(function() {
                // Validation method for budget, profit, revenue fields
                $.validator.addMethod("usd", function(value, element) {
                    return this.optional(element) || /^(\$?)(\d{1,3}(\,\d{3})*|(\d+))(\.\d{2})?$/.test(value);
                }, "Please specify a valid dollar amount");
                $('#admin-form').validate();
            });

        })(window.jQuery);
    </script>

@endsection
