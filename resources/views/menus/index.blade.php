@extends('layouts.app')
@section('title', 'Menü Listesi')
@section('content')
    <div class="content ">
        <br/>
        <!-- START CONTAINER FLUID -->
        <div class=" container-fluid   container-fixed-lg">
            <!-- START card -->
            <div class="card card-default">
                <div class="card-header ">
                    <div class="card-title">Menüler
                    </div>
                    <div class="pull-right">
                        <a aria-label="" href="{{ route('menus.create') }}" class="btn btn-success btn-icon-left m-b-10" type="button"><i class="pg-icon">plus</i><span class="">Yeni Menü Oluştur</span></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                    @include('partials.alerts.error')
                    <div class="table-responsive">
                        <table id="datatable" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Firma Adı</th>
                                <th>E-posta</th>
                                <th>Telefon</th>
                                <th>Adres</th>
                                <th>İşlemler</th>
                            </tr>
                            </thead>


                            <tbody>
                            @foreach($menus as $menu)
                            <tr>
                                <td>
                                    {{ $menu->name }}
                                    @if(count($menu->childs))
                                        @include('menus.childs',['childs' => $menu->childs])
                                    @endif
                                </td>


                                <td>
                                    <a href="{{ route('menus.edit', createHashId($menu->id)) }}" aria-label="" type="button" class="btn btn-primary btn-cons btn-animated from-left">
                                        <span>Düzenle</span>
                                        <span class="hidden-block">
													<i class="pg-icon">edit</i>
												</span>
                                    </a>

                                </td>
                            </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END card -->
        </div>
        <!-- END CONTAINER FLUID -->
    </div>

@endsection
