<div class="row clearfix">
    <div class="col-xl-4">
        <div class="form-group form-group-default required">
            <label>Menü Adı</label>
            {!! Form::text("name",null,['class'=>'form-control','required'=>'required','id'=>'name']) !!}
        </div>
    </div>
    <div class="col-xl-6">
        <div class="form-group form-group-default form-group-default-select2">
            <label>Parent</label>
            {!! Form::select("parent_id", $allMenus , null, ['data-placeholder' => 'Parent seçiniz', 'class'=>'full-width','id'=>'parent_id','required'=>'required', 'data-init-plugin' => 'select2']) !!}
        </div>
    </div>
</div>
