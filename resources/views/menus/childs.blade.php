@foreach($childs as $child)
    <li>
        {{ $child->name }}
        @if(count($child->childs))
            @include('menus.childs',['childs' => $child->childs])
        @endif
    </li>
    @endforeach
    </ul>
