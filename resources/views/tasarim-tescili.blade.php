@extends('frontend.app')
@section('title','Patent Sorgu')

@section('content')
    <div class="bg-img-hero-content basvuru-hero" style="background-image: url({{asset('images/bg.jpg')}});">
        <!-- Main Content -->
        <div class="d-lg-flex align-items-lg-center title-container">
            <div class="container position-relative space-2 space-0--lg mt-lg-8">
                <div class="row justify-content-lg-between ">
                    <h1 class="hero-h1 text-white text-center mb-0 w-100 ">TASARIM <br class="d-md-none"> TESCİLİ</h1>
                    <span class="hero-lead d-block text-center text-white w-100 mt-4">

                    </span>
                </div>

            </div>
        </div>
    </div>
    {!! Form::open(['url' => '/basvuru/tasarim-tescili','id'=>'admin-form','role'=>'form','autocomplete'=>'off', 'novalidate' => 'novalidate', 'files' => true])!!}
    <section class="container py-5 basvuruMainContainer">
            <div class="row">
                <div class="col-12 col-md-8">
                    @include('macros.alert')
                    <div id="basvuru-paketi" class="mt-n10 mt-md-auto">
                        <div class="basvuru-title mb-2 d-none d-md-block">Başvuru Paketinizi Seçin</div>
                        <div class="lead mt-3 mb-5 d-none d-md-block">Avantajları paketlerimizden birisini seçin, ya da Size Özel ile kendiniz oluşturun.</div>
                        <div class="row no-wrap overflow-auto" style="min-height: 356px;">
                            @php $i = 1; $a = ""; @endphp
                            @foreach($packages as $package)
                                @if($i == 1)
                                    @php $p_first = $package;$suggested_package =$package;  @endphp
                                @endif
                            <div class="col-md-4">
                                <div class="paket-dis pkt{{ $package->id }} {!! $package->is_suggested == 1 ? 'active' : '' !!}" data-gorseladeti="{{ $package->gorsel_adeti }}" data-packageid="{{ $package->id }}" data-classlist="{{ strlen($package->class_lists) >0 ? $package->class_lists : '0' }}" data-servicelist="{{ strlen($package->additional_services) > 0 ? $package->additional_services : '0' }}" data-servicelistprice="{{ strlen($package->additional_services) > 0 ? $package->getServices('price') : '0' }}" data-pckprice="{{ $package->price }}" data-pckname="{{ $package->name_mini }}">
                                    <div class="card paket">
                                        <div class="card-header">
                                            <div class="paket-icon text-center">{!! $package->is_star == 1 ? '<i class="bi bi-star-fill"></i>' : '' !!}</div>
                                            <div class="baslik-ust"></div>
                                            {!! $package->name !!}
                                        </div>
                                        <div class="card-body">
                                            <div class="ortaYazi">
                                                <div>{!! $package->description !!}</div>
                                            </div>

                                            <div class="w-100 paket-icon mt-4" style="height: 62px;">
                                                <img src="{!! $package->icon !!}" style="height: 62px;width: 62px;" />
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <span class="d-block w-100 text-center" style="font-size: 24px;font-weight:700">{!! str_replace('.00', '', $package->price) !!} <span style="font-size: 18px">₺</span>
                                            <br><span style="font-size: 13px">+ KDV</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                @php $i++; @endphp
                            @endforeach


                        </div>
                    </div>

                    <div id="markaniz" style="margin-top: 84px;">
                        <div class="basvuru-title mb-2">Neyi Tescil Ettirmek İstiyorsunuz</div>
                        <div class="lead mt-3 mb-5">Kısaca Faydalı Modelinizin Ürününüzün Betimleyici İsmini Yazın</div>

                        <div class="form-row">
                            <div class="col-lg-12">
                                <input type="text" value="Deha Patent" id="marka" name="marka" class="form-control inputBasvuru marka-text" />
                                <img class="edit-pencil position-absolute" src="{{asset('images/pencil.svg')}}" />
                            </div>
                        </div>
                        <br>
                        <div class="form-row">
                            <div class="col-lg-12">
                                <textarea name="basvuru_aciklamasi" class="form-control basvuru_aciklamasi" id="basvuru_aciklamasi" placeholder="Başvurunuzu Açıklayınız"></textarea>
                            </div>
                        </div>

                    </div>

                    <div id="fatura-bilgileri" style="margin-top: 84px;">
                        <div class="basvuru-title mb-2">Fatura ve Tescil Sahibi Bilgileriniz</div>
                        <div class="lead mt-3 mb-5">Fatura bilgilerinizi giriniz. Marka tescili şirket üzerine olmak zorunda değildir. Bireysel başvuru yapılabilir.</div>
                        <div class="form-row mb-3">
                            <div class="form-group col-md-6 ">
                                <input type="text" class="form-control inputBasvuru" id="inputName" name="inputName" placeholder="Ad Soyad veya Ünvan">
                            </div>
                            <div class="form-group col-md-6 ">
                                <input type="text" class="form-control inputBasvuru" name="inputTCNo" id="inputTCNo" placeholder="TCKN veya Vergi No">
                            </div>
                        </div>

                        <div class="form-row mb-3">
                            <div class="form-group col-md-6">
                                <input type="email" class="form-control inputBasvuru" name="inputEmail" id="inputEmail" placeholder="E-mail">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control inputBasvuru" name="inputTelefon" id="inputTelefon" placeholder="Telefon">
                            </div>
                        </div>

                        <div class="form-row mb-3">
                            <div class="form-group col-md-6">
                                <textarea name="address" class="form-control inputBasvuru" id="inputAdres" placeholder="Adres"></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <span class="font-weight-bolder ml-4">Fatura Türü</span>
                                <div class="ml-4 mt-4">
                                    <div class="default-checkbox custom-control-inline">
                                        <input type="radio" id="bireysel" name="fatura_turu" value="bireysel" class="custom-control-input" checked="checked">
                                        <label class="custom-control-label default-checkbox-label" for="bireysel">Bireysel</label>
                                    </div>
                                    <div class="default-checkbox custom-control-inline">
                                        <input type="radio" id="kurumsal" name="fatura_turu" value="kurumsal" class="custom-control-input">
                                        <label class="custom-control-label default-checkbox-label" for="kurumsal">Kurumsal</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div id="logonuz" style="margin-top: 60px;">
                        <div class="basvuru-title mb-2">Tasarım Tescil Görselleriniz <span class="font-weight-light" style="font-size: 17px;"><em>(İsteğe Bağlı)</em></span></div>
                        <div class="lead mt-3 mb-5">Tasarım Tescilinize Bağlı Görselleri Buradan Yükleyiniz.</div>
                        <div class="logo-upload d-flex w-100 flex-column">
                            <span>Logonuzu Yükleyin</span>
                            <i class="bi bi-file-earmark-arrow-up-fill"></i>
                            <span>PNG, JPEG - Max 1 MB</span>
                        </div>
                    </div>
                    <input type="file" id="inputDosya" name="dosyalar[]" maxlength="1" class="asd"  accept="gif|jpg|png|tiff|tif"/>
                    <div id="ilave-hizmetler" style="margin-top: 84px;">
                        <div class="basvuru-title mb-2">İlave Hizmetler (İsteğe Bağlı)</div>
                        <div class="lead mt-3 mb-5">Süreçte işinizi kolaylaştırabilecek diğer hizmetleri buradan ekleyebilirsiniz.</div>

                        <div class="row">
                            @php $service_defaults = []; @endphp
                            @foreach($services as $service)
                            <div class="form-group col-md-6 mb-4">
                                <div class="ilave-checkbox">
                                    <input type="checkbox" data-serviceprice="{{ $service->price }}" data-servicename="{{ $service->name }}" data-serviceid="{{ $service->id }}" id="serv{{ $service->id }}" class="ilave-control-input" {!! in_array($service->id, $service_defaults) ? 'checked="checked"' : '' !!} name="services[]" value="{{ $service->id }}">
                                    <label class="ilave-control-label d-flex flex-column position-relative" for="serv{{ $service->id }}">
                                        <div class="ilavePrice position-absolute text-right d-flex flex-column">
                                            <span class="ilaveNumber">{{ str_replace('.00', '', $service->price) }}<span class="ilaveTlIcon"> ₺</span></span>

                                            <span class="ilaveKdv">+KDV</span>
                                        </div>
                                        <span class="ilaveTitle">{!! $service->name !!}</span>
                                        <span class="ilaveDesc">{!! $service->description !!}</span>
                                    </label>
                                </div>
                            </div>
                            @endforeach

                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="basvuru-ozeti">
                        <div class="card">
                            <div class="card-header">
                                <h4>Başvuru Özetiniz</h4>
                            </div>
                            <div class="card-body d-flex flex-column">
                                <div class="d-flex flex-column">
                                    <span class="description">BAŞVURU</span>
                                    <span class="service-name" >Tasarım Tescili Başvurusu</span>
                                </div>

                                <div class="d-flex flex-column">
                                    <span class="description">TESCİL EDİLECEK ÜRÜN</span>
                                    <span class="service-name" id="ozet-tecil-marka"></span>
                                </div>
                                <div class="add-services-div">
                                    {!! $suggested_package->getServices('text') !!}
                                </div>

                                <div class="pck-name">
                                    <div class="d-flex flex-row justify-content-between mt-5 mb-3">
                                        <span class="description">{{ $suggested_package->name_mini }}</span>
                                        <span class="price flex-shrink-0">{{ number_format($suggested_package->price ,0,',','.') }} ₺</span>
                                    </div>
                                </div>

                                <div class="ilave-hizmetler">
                                    <div class="d-flex flex-row justify-content-between mb-3">
                                        <span class="description">İLAVE HİZMETLER</span>
                                        <span class="price flex-shrink-0">{{ number_format($suggested_package->getPrice('service') ,0,',','.') }} ₺</span>
                                    </div>
                                </div>


                                <div class="d-flex flex-row justify-content-between mb-3">
                                    <span class="description">KDV (%18)</span>
                                    <span class="price flex-shrink-0 kdv-div" >{{ number_format($suggested_package->getTotal('KDV', $suggested_package->price, $suggested_package->getPrice('class'), $suggested_package->getPrice('service') ) ,0,',','.') }} ₺</span>
                                </div>
                                <div class="d-flex flex-row justify-content-between">
                                    <span class="description">TOPLAM ÖDENECEK</span>
                                    <span class="total-price ml-auto flex-shrink-0 toplam-odenecek-div">{{ number_format($suggested_package->getTotal('TOTAL', $suggested_package->price, $suggested_package->getPrice('class'), $suggested_package->getPrice('service') ) ,0,',','.') }} ₺</span>
                                </div>
                            </div>
                        </div>

                        <div class="d-block d-flex white-shadow-box bg-white mt-n4">
                            <div class="d-flex flex-row">
                                <img class="mr-2" src="{{asset('images/lock-small.svg')}}" style="height: 19px;" />
                                <div class="d-flex flex-column">
                                    <span class="iade">İADE GARANTİSİ!</span>
                                    <span class="iade-info">Red Durumunda Ücretsiz 2. Başvuru</span>
                                </div>
                            </div>
                        </div>

                        <button type="button" class="btn-green-degrade mt-3 text-white font-weight-bolder d-block py-4 odeme-yap" >ÖDEME YAP</button>

                        <div class="white-shadow-box sozlesme-box mt-4 d-flex flex-row">

                            <div class="default-checkbox custom-control-inline">
                                <input type="checkbox" id="sozlesme_onay" name="sozlesme_onay" value="bireysel" class="custom-control-input" checked="checked">
                                <label class="custom-control-label default-checkbox-label" for="sozlesme_onay"></label>
                            </div>
                            <div>
                                Başvurum için gereken <a href="javascript:;"><span class="font-weight-bolder"><u>hizmet sözleşmesini</u></span></a>  okudum, onaylıyorum.
                            </div>

                        </div>

                    </div>

                    <div class="basvuru-ozeti-mobil d-sm-none position-fixed">
                        <div class="box-outer position-relative">
                            <img src="{{asset('images/up.svg')}}" class="basvuru-up-icon position-absolute" />
                            <div class="box-inside d-flex flex-column">
                                <div class="mobile-ozet-baslik text-white">Başvuru Özetiniz</div>
                                <div class="d-flex justify-content-between mt-2">
                                    <div class="d-flex flex-row">
                                        <img class="mr-2" src="{{asset('images/lock-small.svg')}}" />
                                        <div class="d-flex flex-column">
                                            <span class="iade">İADE GARANTİSİ!</span>
                                            <span class="iade-info">Red Durumunda Ücretsiz 2. Başvuru</span>
                                        </div>
                                    </div>
                                    <div class="mobil-price text-white mobil-total">{{ number_format($suggested_package->getTotal('TOTAL', $suggested_package->price, $suggested_package->getPrice('class'), $suggested_package->getPrice('service') ) ,0,',','.') }} ₺</div>
                                </div>

                                <button type="button" class="btn-green-degrade mt-3 text-white font-weight-bolder d-block odeme-yap">ÖDEME YAP</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="current_packet" id="current_packet" value="{{ $p_first->id }}">
            <input type="hidden" name="current_service_price" id="current_service_price" value="0">
            <input type="hidden" name="main_current" id="main_current" value="{{ $p_first->id }}">

            <input type="hidden" name="gorsel_adeti" id="gorsel_adeti" value="{{ $p_first->gorsel_adeti }}">
            <input type="hidden" name="as_count" id="as_count" value="0">
            <input type="hidden" name="ilave_hizmet_ucretleri" id="ilave_hizmet_ucretleri" value="0">
            <input type="hidden" name="basvuru_ucreti" id="basvuru_ucreti" value="{{ $p_first->price }}">
            <input type="hidden" name="kdv" id="kdv" value="{{ number_format(($p_first->price * 0.18) ,0,',','.') }}">
            <input type="hidden" name="total" id="total" value="0">
        <input type="hidden" name="basvuru_turu" id="basvuru_turu" value="tasarim_tescili">

    </section>
    {!! Form::close() !!}
@endsection

@section('css')


@endsection

@section('js')
    <script src="{{asset('assets/js/jquery.MultiFile.min.js')}}" type="text/javascript" language="javascript"></script>
    <script src="{{asset('js/sweetalert2.min.js')}}"></script>
    <script>
        $('.odeme-yap').on('click', function() {
            if (ValidateSearchParameters() === true) {
                $('#admin-form').submit();
            }
        });
        function ValidateSearchParameters() {


            let searchWord = $("#marka").val();

            if(searchWord.length == 3){
                searchWord = searchWord + ' ';
            }
            if (searchWord.length <= 3 || searchWord.length > 50) {
                swal({
                    title: '',
                    text: "Tescil Kapsamındaki Ürününüzün betimleyici adı en az 4 en çok 50 karakter uzunluğunda olmalıdır.",
                    type: 'warning',
                    confirmButtFonText: 'Tamam'
                });
                return false;
            }

            searchWord = $("#inputName").val();

            if(searchWord.length == 1){
                searchWord = searchWord + ' ';
            }
            if (searchWord.length <= 2) {
                swal({
                    title: '',
                    text: "Ad soyad veya ünvanınızın girilmesi gereklidir.",
                    type: 'warning',
                    confirmButtFonText: 'Tamam'
                });
                return false;
            }

            searchWord = $("#inputAdres").val();

            if(searchWord.length == 1){
                searchWord = searchWord + ' ';
            }
            if (searchWord.length <= 2) {
                swal({
                    title: '',
                    text: "Adres alanı girilmesi gereklidir.",
                    type: 'warning',
                    confirmButtFonText: 'Tamam'
                });
                return false;
            }

            let tel = $('#inputTCNo').val();

            if (tel.length != 11) {
                swal({
                    title: '',
                    text: "Eksik veya hatalı T.C. numarası girildi.",
                    type: 'warning',
                    confirmButtonText: 'Tamam'
                });
                return false;
            }
            searchWord = $("#inputEmail").val();
            if(searchWord.length == 1){
                searchWord = searchWord + ' ';
            }
            if (searchWord.length <= 2) {
                swal({
                    title: '',
                    text: "E-posta bilgisi girilmesi gereklidir.",
                    type: 'warning',
                    confirmButtFonText: 'Tamam'
                });
                return false;
            }

            searchWord = $("#inputTelefon").val();

            if (searchWord.length === 0) {
                swal({
                    title: '',
                    text: "Telefon numarası bilgisi girilmesi gereklidir.",
                    type: 'warning',
                    confirmButtFonText: 'Tamam'
                });
                return false;
            }

            return true;
        }
        $('.marka-text').keyup(function () {

            $('#ozet-tecil-marka').html($('.marka-text').val());
        });

        function rightSide(){

            let mini_name = '';
            let ilave_hizmet_text = '';
            let selected_packet = parseInt($('#current_packet').val());
            if (selected_packet > 0) {
                $('#basvuru_ucreti').val($('.pkt' + selected_packet).data('pckprice'));
                $('#ilave_hizmet_ucretleri').val(0);
                mini_name = '<div class="pck-name"><div class="d-flex flex-row justify-content-between mt-5 mb-3"><span class="description pck-mini-name">'+$('.pkt' + selected_packet).data('pckname')+'</span><span class="price flex-shrink-0">'+$('.pkt' + selected_packet).data('pckprice')+' ₺</span></div></div>';

            } else {
                mini_name = '';
                $('#basvuru_ucreti').val(0);
            }

            let p_ilave = 0;
            $('#ilave-hizmetler input:checked').each(function() {
                p_ilave += parseFloat($(this).data('serviceprice'));

            });

            $('#ilave_hizmet_ucretleri').val(p_ilave);
            ilave_hizmet_text = '<div class="d-flex flex-row justify-content-between mb-3"><span class="description">İLAVE HİZMETLER</span><span class="price flex-shrink-0">'+ p_ilave +' ₺</span></div>';
            //pck-mini-name
            $('.pck-name').html(mini_name);
            $('.ilave-hizmetler').html(ilave_hizmet_text);

            //ilave-siniflar




            let as_count = $('#as_count').val();

            if (as_count > 0) {
                //add-services-div in içine
                $('.add-services-div').html('');
                let service_text = '';
                let bottom = '</div>';
                let top = '<div class="d-flex flex-column mt-4 mb-3"><span class="description">İLAVE HİZMETLER</span>';
                $('#ilave-hizmetler input:checked').each(function() {
                    let servicename = $(this).data('servicename');
                    service_text += '<span class="service-name">'+servicename+'</span>';
                });
                $('.add-services-div').append(top + service_text + bottom);

            } else {
                $('.add-services-div').html('');
            }

            let h = parseFloat($('#ilave_hizmet_ucretleri').val());
            let b = parseFloat($('#basvuru_ucreti').val());

            let k = (h + b) * 0.18;
            $('#kdv').val(k);
            $('#basvuru_ucreti').val(b);
            $('#total').val(h + b + k );
            $('.kdv-div').html(k + ' ₺');
            let to = h + b + k;
            $('.toplam-odenecek-div').html(to + ' ₺' );
            $('.mobil-total').html(to + ' ₺' );

        }

        function checkValue(value,arr){
            var status = false;

            for(var i=0; i<arr.length; i++){
                var name = arr[i];
                if(name == value){
                    status = true;
                    break;
                }
            }
            return status;
        }

        let as = 0;
        $('#ilave-hizmetler').click(function (){
            as = $('#ilave-hizmetler input:checked').length;
            let serviceprice = 0;
            $('#ilave-hizmetler input:checked').each(function() {
                let servicename = $(this).data('servicename');
                serviceprice += parseFloat($(this).data('serviceprice'));
            });

            $('#as_count').val(as);
            $('#current_service_price').val(serviceprice);
            rightSide();
        });
        //$('#inputDosya').MultiFile($('#gorsel_adeti').val());
        //$('#inputDosya').attr('maxLength', $('#gorsel_adeti').val());
        $('.paket-dis').click(function (){

            $('.paket-dis').removeClass('active');
            $(this).addClass('active');
            let packageid = $(this).data('packageid');
            let gorselAdeti = $(this).data('gorseladeti');
            $('#gorsel_adeti').val(gorselAdeti);
            $('#inputDosya').removeClass('multi');
            $('#inputDosya').addClass('multi');

            $('.multi').MultiFile(gorselAdeti);

            if (packageid != 0) {



                $('#current_packet').val(packageid);


                $('#as_count').val(0);

            }
            rightSide();
        });





        $('input[name=marka]').val(JSON.parse(localStorage.getItem('markWord')));
        $('#ozet-tecil-marka').html(JSON.parse(localStorage.getItem('markWord')));
        $('#inputName').val(JSON.parse(localStorage.getItem('isim')));
        $('#inputTelefon').val(JSON.parse(localStorage.getItem('telefon')));
        //$('#arastirilan_sektor').html(JSON.parse(localStorage.getItem('sector_name')));


    </script>
@endsection
