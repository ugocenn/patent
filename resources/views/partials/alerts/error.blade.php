@if (count($errors) > 0)
    <div class="alert alert-danger bordered" role="alert">
        <ul class="list-unstyled">
            @foreach ($errors->all() as $error)
                <li>{!! $error !!}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(session('message'))
    <div class="alert alert-success bordered">
        <strong>{!! session('message')!!}</strong>
    </div>
@endif

@if(session('success'))
    <div class="alert alert-success bordered">
        <strong>{!! session('success')!!}</strong>
    </div>
@endif

@if(session('danger'))
    <div class="alert alert-danger bordered">
        <strong>{!! session('danger')!!}</strong>
    </div>
@endif

@if(session('warning'))
    <div class="alert alert-warning bordered">
        <strong>{!! session('warning')!!}</strong>
    </div>
@endif






