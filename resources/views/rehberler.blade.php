@extends('frontend.app')
@section('title','Rehberler | '.config('app.name'))
@section('description',$rehberler_cat->metadescription)
@section('keywords',$rehberler_cat->keywords)

@section('content')
    <div class="bg-img-hero-content rehber-hero" style="background-image: url({{asset('images/bg.jpg')}});">
        <div class="d-lg-flex align-items-lg-center title-container">
            <div class="container position-relative space-2 space-0--lg mt-lg-8">
                <div class="row justify-content-lg-between ">
                    <h1 class="hero-h1 text-white text-center mb-0 w-100 ">REHBERLER</h1>
                    <span class="hero-lead d-block text-center text-white w-100 mt-5 mx-4 mx-md-0 mt-md-4">
                        Sizin için hazırladığımız rehberlere göz atın!
                    </span>
                </div>

            </div>
        </div>
    </div>

    <section class="container-fluid bg-light services" >
        <div class="container" style="background-image: url({{asset('images/rehber.svg')}});background-repeat: no-repeat;background-position: right;">
            <div class="row">
                <div class="col-12">
                    <div class="ml-100 pb-230">
                        <span class="d-block subtitle">REHBERLER</span>
                        <h2>Popüler Rehberler</h2>
                        <span class="d-block lead mb-4">Sizin için hazırladığımız, popüler içeriklere buradan ulaşabilirsiniz. Okuyun, işletmenize değer katın, başkalarıyla paylaşın.</span>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="container position-relative pt-3 pt-lg-0 pb-5 mt-lg-n10">
        <div class="row">
            @foreach(getRehbers(2, 'rehberler', 0) as $rehber)
                <div class="col-12 col-md-6 col-lg-4">
                    <?php
                    $image = str_replace('/public', '', $rehber->image);
                    $image = str_replace('/depo/dosyalar/', '', $image);
                    ?>
<a href="{{ route('show-page', [$rehber->slug]) }}">
                    <div class="card content-card ml-auto" style="background-image: url({{ url('depo/dosyalar/188x255/'.$image) }});">
                        <span>REHBERLER</span>
                       <h3 class="mt-3 mb-4">{{ $rehber->title }}</h3>
                        <p class="mb-0">{!! $rehber->short_text !!}</p>
                        <i
                                class="bi bi-arrow-right"></i>
                    </div>
                </a>
                </div>
            @endforeach
        </div>
    </section>

    <section class="container-fluid bg-light services" >
        <div class="container" >
            <div class="row">
                <div class="col-12">
                    <div class="ml-100 pb-230">
                        <span class="d-block subtitle">REHBERLER</span>
                        <h2>Girişimciler İçin</h2>
                        <span class="d-block lead mb-4">Yeni girişimciler için hazırladığımız içeriklere buradan ulaşabilirsiniz. Okuyun, girişiminize değer katın, başkalarıyla paylaşın.</span>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="container position-relative pt-3 pt-lg-0 pb-5 mt-lg-n10">
        <div class="row">
            @foreach(getRehbers(2, 'girisimciler-icin', 0) as $rehber)
                <div class="col-12 col-md-6 col-lg-4">
                    <?php
                    $image = str_replace('/public', '', $rehber->image);
                    $image = str_replace('/depo/dosyalar/', '', $image);
                    ?>
 <a href="{{ route('show-page', [$rehber->slug]) }}">
                    <div class="card content-card ml-auto" style="background-image: url({{ url('depo/dosyalar/188x255/'.$image) }});">
                        <span>REHBERLER</span>
                       <h3 class="mt-3 mb-4">{{ $rehber->title }}</h3>
                        <p class="mb-0">{!! $rehber->short_text !!}</p>
                       <i
                                class="bi bi-arrow-right"></i>
                    </div></a>
                </div>
            @endforeach
        </div>
    </section>

    <section class="container-fluid bg-light services" >
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="ml-100 pb-230">
                        <span class="d-block subtitle">REHBERLER</span>
                        <h2>Haberler</h2>
                        <span class="d-block lead mb-4">Marka, Patent ve Tasarım dünyasından dikkatimizi çeken haberlere buradan erişebilirsiniz. Okuyun, yeniliklerden haberdar olun, başkalarıyla paylaşın.</span>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="container position-relative pt-3 pt-lg-0 pb-5 mt-lg-n10">
        <div class="row">
            @foreach(getRehbers(2, 'haberler', 0) as $rehber)
                <div class="col-12 col-md-6 col-lg-4">
                    <?php
                    $image = str_replace('/public', '', $rehber->image);
                    $image = str_replace('/depo/dosyalar/', '', $image);
                    ?>
                    <a href="{{ route('show-page', [$rehber->slug]) }}">
                    <div class="card content-card ml-auto" style="background-image: url({{ url('depo/dosyalar/188x255/'.$image) }});">
                        <span>REHBERLER</span>
                        <h3 class="mt-3 mb-4">{{ $rehber->title }}</h3>
                        <p class="mb-0">{!! $rehber->short_text !!}</p>
                        <i
                                class="bi bi-arrow-right"></i>
                    </div></a>
                </div>
            @endforeach
        </div>
    </section>
@endsection

@section('css')
@endsection

@section('js')
@endsection
