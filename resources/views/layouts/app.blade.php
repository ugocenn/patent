<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
@include('layouts.head')
<body class="fixed-header menu-pin menu-behind ">
<!-- BEGIN SIDEBPANEL-->
@include('layouts.sidebar')
<!-- END SIDEBAR -->
<!-- END SIDEBPANEL-->
<!-- START PAGE-CONTAINER -->
<div class="page-container ">
    <!-- START HEADER -->
    <div class="header ">
        <!-- START MOBILE SIDEBAR TOGGLE -->
        <a href="#" class="btn-link toggle-sidebar d-lg-none pg-icon btn-icon-link" data-toggle="sidebar">
            menu</a>
        <!-- END MOBILE SIDEBAR TOGGLE -->
        <div class="">
            <div class="brand inline   ">
                <img src="{{ url('assets/img/logo.svg') }}" alt="logo" data-src="{{ url('assets/img/logo.svg') }}" data-src-retina="{{ url('assets/img/logo.svg') }}"  height="34">
            </div>
            <!-- START NOTIFICATION LIST -->
            <ul class="d-lg-inline-block d-none notification-list no-margin d-lg-inline-block b-grey b-l b-r no-style p-l-20 p-r-20">
            {{ auth()->user()->company->name }}
            </ul>
            <!-- END NOTIFICATIONS LIST -->
        </div>
        <div class="d-flex align-items-center">
            <!-- START User Info-->
            <div class="dropdown pull-right d-lg-block d-none">
                <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" aria-label="profile dropdown">
              <span class="thumbnail-wrapper d32 circular inline">
      					<img src="{{ url('storage/'.auth()->user()->pictureLink) }}" alt="" data-src="{{ url('storage/'.auth()->user()->pictureLink) }}"
                             data-src-retina="{{ url('storage/'.auth()->user()->pictureLink) }}" width="32" height="32">
      				</span>
                </button>
                <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
                    <a href="#" class="dropdown-item"><span><b>{{ auth()->user()->name }}</b></span></a>
                    <div class="dropdown-divider"></div>
                    <a href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="dropdown-item">Çıkış</a>
                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                    <div class="dropdown-divider"></div>
                </div>
            </div>
            <!-- END User Info-->
        </div>
    </div>
    <!-- END HEADER -->
    <!-- START PAGE CONTENT WRAPPER -->
    <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        @yield('content')
        <!-- END PAGE CONTENT -->
        <!-- START COPYRIGHT -->
        <!-- START CONTAINER FLUID -->
        <!-- START CONTAINER FLUID -->
        @include('layouts.bottom_info')
        <!-- END COPYRIGHT -->
    </div>
    <!-- END PAGE CONTENT WRAPPER -->
</div>
<!-- END PAGE CONTAINER -->


@include('layouts.footer')
</body>
</html>
