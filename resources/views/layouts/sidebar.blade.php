<nav class="page-sidebar" data-pages="sidebar">
    <!-- BEGIN SIDEBAR MENU HEADER-->
    <div class="sidebar-header">
        <img src="{{asset('images/logo.svg')}}" alt="logo" class="brand" data-src="{{asset('images/logo.svg')}}" data-src-retina="{{asset('images/logo.svg')}}" width="135" >
        <div class="sidebar-header-controls">
            <button aria-label="Pin Menu" type="button" class="btn btn-icon-link invert d-lg-inline-block d-xlg-inline-block d-md-inline-block d-sm-none d-none" data-toggle-pin="sidebar">
                <i class="pg-icon"></i>
            </button>
        </div>
    </div>
    <!-- END SIDEBAR MENU HEADER-->
    <!-- START SIDEBAR MENU -->
    <div class="sidebar-menu">
        <!-- BEGIN SIDEBAR MENU ITEMS-->
        <ul class="menu-items">
            <li>
                <a href="{{ route('home') }}">
                    <span class="title">Anasayfa</span>
                </a>
                <span class="icon-thumbnail"><i class="pg-icon">home</i></span>
            </li>
            <li>
                <a href="{{ route('users.index') }}">
                    <span class="title">Kullanıcılar</span>
                </a>
                <span class="icon-thumbnail"><i class="pg-icon">users</i></span>
            </li>
            <li class="">
                <a href="{{ route('companies.index') }}" class="detailed">
                    <span class="title">Firma Tanımlamaları</span>
                </a>
                <span class="icon-thumbnail"><i class="pg-icon">social</i></span>
            </li>
            <li class="">
                <a href="{{ route('customers.index') }}" class="detailed">
                    <span class="title">Müşteriler</span>
                </a>
                <span class="icon-thumbnail"><i class="pg-icon">social</i></span>
            </li>
            <li class="">
                <a href="{{ route('searchs.index') }}" class="detailed">
                    <span class="title">Aramalar</span>
                </a>
                <span class="icon-thumbnail"><i class="pg-icon">social</i></span>
            </li>
            <li class="">
                <a href="{{ route('categories.index') }}" class="detailed">
                    <span class="title">Kategoriler</span>
                </a>
                <span class="icon-thumbnail"><i class="pg-icon">social</i></span>
            </li>
            <li class="">
                <a href="{{ route('web-pages.index') }}" class="detailed">
                    <span class="title">Sayfalar</span>
                </a>
                <span class="icon-thumbnail"><i class="pg-icon">social</i></span>
            </li>
            <li class="">
                <a href="{{ route('product-classes.index') }}" class="detailed">
                    <span class="title">Sınıflar</span>
                </a>
                <span class="icon-thumbnail"><i class="pg-icon">social</i></span>
            </li>

            <li class="">
                <a href="{{ route('sectors.index') }}" class="detailed">
                    <span class="title">Sektörler</span>
                </a>
                <span class="icon-thumbnail"><i class="pg-icon">social</i></span>
            </li>
            <li class="">
                <a href="{{ route('services.index') }}" class="detailed">
                    <span class="title">Ek Hizmetler</span>
                </a>
                <span class="icon-thumbnail"><i class="pg-icon">social</i></span>
            </li>
            <li class="">
                <a href="{{ route('package-types.index') }}" class="detailed">
                    <span class="title">Paket Türleri</span>
                </a>
                <span class="icon-thumbnail"><i class="pg-icon">social</i></span>
            </li>
            <li class="">
                <a href="{{ route('packages.index') }}" class="detailed">
                    <span class="title">Paketler</span>
                </a>
                <span class="icon-thumbnail"><i class="pg-icon">social</i></span>
            </li>

            <li class="">
                <a href="{{ route('videos.index') }}" class="detailed">
                    <span class="title">Video</span>
                </a>
                <span class="icon-thumbnail"><i class="pg-icon">social</i></span>
            </li>

        </ul>
        <div class="clearfix"></div>
    </div>
    <!-- END SIDEBAR MENU -->
</nav>
