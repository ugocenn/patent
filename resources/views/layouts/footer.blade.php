<!-- BEGIN VENDOR JS -->
<script src="{{ url('assets/plugins/pace/pace.min.js') }}" type="text/javascript"></script>
<!--  A polyfill for browsers that don't support ligatures: remove liga.js if not needed-->
<script src="{{ url('assets/plugins/liga.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/plugins/jquery/jquery-3.2.1.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/plugins/modernizr.custom.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/plugins/popper/umd/popper.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/plugins/jquery/jquery-easy.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/plugins/jquery-unveil/jquery.unveil.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/plugins/jquery-ios-list/jquery.ioslist.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/plugins/jquery-actual/jquery.actual.min.js') }}"></script>
<script src="{{ url('assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/plugins/select2/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/plugins/classie/classie.js') }}"></script>

<script src="{{ url('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('assets/plugins/datatables/dataTables.bootstrap4.js') }}"></script>
<script src="{{ url('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ url('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ url('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ url('assets/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ url('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ url('assets/plugins/datatables/buttons.flash.min.js') }}"></script>
<script src="{{ url('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ url('assets/plugins/datatables/dataTables.keyTable.min.js') }}"></script>
<script src="{{ url('assets/plugins/datatables/dataTables.select.min.js') }}"></script>
<script src="{{ url('assets/js/uniform.min.js') }}"></script>
<script src="{{ url('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ url('assets/plugins/jquery-inputmask/jquery.inputmask.min.js') }}"></script>
<script src="{{ url('pages/js/pages.js') }}"></script>
<script src="{{ url('assets/js/scripts.js') }}" type="text/javascript"></script>
<!-- Datatables init -->
<script src="{{ url('assets/js/datatables.init.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/plugins/moment/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/plugins/bootstrap-typehead/typeahead.bundle.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/plugins/bootstrap-typehead/typeahead.jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/plugins/bootstrap-tag/bootstrap-tagsinput.min.js') }}" type="text/javascript"></script>
<script src="{{ url('vendor/plugin/tinymce/tinymce.min.js') }}" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
@yield('scripts')
@stack('scripts')

<script>
    function confirmOpsDelete(id) {
        bootbox.dialog({
            message: "İçeriği silmek isteğinize emin misiniz?",
            title: "İçerik silme",
            buttons: {
                danger: {
                    label: "Sil",
                    className: "btn-danger",
                    callback: function() {
                        document.getElementById('ops-delete-form-' + id).submit();
                    }
                },
                main: {
                    label: "İptal",
                    className: "btn-primary",
                    callback: function() {}
                }
            }
        });
    }
</script>
