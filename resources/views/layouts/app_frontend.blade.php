<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
@include('layouts.head')
<body class="fixed-header ">
<!-- BEGIN SIDEBPANEL-->

<!-- END SIDEBAR -->
<!-- END SIDEBPANEL-->
<!-- START PAGE-CONTAINER -->
<div class="page-container ">
    <!-- START HEADER -->
    <div class="header ">
        <!-- START MOBILE SIDEBAR TOGGLE -->
        <a href="#" class="btn-link toggle-sidebar d-lg-none pg-icon btn-icon-link" data-toggle="sidebar">
            menu</a>
        <!-- END MOBILE SIDEBAR TOGGLE -->
        <div class="">
            <div class="brand inline   ">
                <img src="{{ url('assets/img/logo.svg') }}" alt="logo" data-src="{{ url('assets/img/logo.svg') }}" data-src-retina="{{ url('assets/img/logo.svg') }}" height="40">
            </div>
            <!-- START NOTIFICATION LIST -->
            <ul class="d-lg-inline-block d-none notification-list no-margin d-lg-inline-block b-grey b-l b-r no-style p-l-20 p-r-20">

            </ul>
            <!-- END NOTIFICATIONS LIST -->
        </div>
        <div class="d-flex align-items-center">
            @if(isset($login_button))
                <a aria-label="" href="{{ route('login') }}" class="btn btn-success btn-icon-left m-b-10" style="color:white"><i class="pg-icon">shield_lock</i><span class="">PANEL GİRİŞİ</span></a>
            @endif
        </div>
    </div>
    <!-- END HEADER -->
    <!-- START PAGE CONTENT WRAPPER -->
    <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
    @yield('content')
    <!-- END PAGE CONTENT -->
        <!-- START COPYRIGHT -->
        <!-- START CONTAINER FLUID -->
        <!-- START CONTAINER FLUID -->
    @include('layouts.bottom_info')
    <!-- END COPYRIGHT -->
    </div>
    <!-- END PAGE CONTENT WRAPPER -->
</div>
<!-- END PAGE CONTAINER -->


@include('layouts.footer')
</body>
</html>
