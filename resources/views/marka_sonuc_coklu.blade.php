@extends('frontend.app')

@section('title', $marka . ' Marka Sorgusu | '.config('app.name'))


@section('content')
    <div class="modal"></div>
    <div id="loader" class="displayNone"></div>
    <div id="reCaptchaDiv" class="displayNone">
        <div id="captcha_container0" class="g-recaptcha-with-ajax"></div>
    </div>
    <div class="bg-img-hero-content" style="background-image: url({{asset('images/bg.jpg')}});">
        <!-- Main Content -->
        <div class="d-lg-flex align-items-lg-center title-container">
            <div class="container position-relative space-2 space-0--lg mt-lg-8">
                <div class="row justify-content-lg-between ">
                    <h1 class="hero-h1 text-white text-center mb-0 w-100 ">SONUÇLAR</h1>
                    <div class="hero-lead d-block text-center text-white w-100 mt-4 marka-sonuc-lead">
                        <div class="pink-degrade d-inline">“<div class="d-inline" id="arastirilan_marka"></div>”</div> <div class="d-inline">sorgusu</div> <br class="d-md-none"><div class="d-inline pink-degrade">“<div class="d-inline" id="arastirilan_sektor"></div>”</div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <section class="container position-relative basvuru-box-container" style="max-width:960px;">

        <div class="row position-absolute w-100 flex-lg-row justify-content-center justify-content-md-between media-box-left-container"  >
            <div class="col">
                <div class="transparent-w-auto-card">
                    <div class="media-box-left">
                        <div class="media">
                            <img src="{{asset('images/confirm-icon.svg')}}" class="mr-0 d-none d-lg-block" alt="">
                            <div class="media-body ml-0 ml-lg-5">
                                <h5 class="mt-0 mb-0 ">Benzerleri Bulundu... <br> <span class="font-weight-bolder" >Ancak Başvuru Yapılabilir!</span> </h5>
                                <img src="{{asset('images/green-line.svg')}}" class="green-line">
                                <span class="d-block mt-4">Sorguladığınız sınıfı ya da sektörü kontrol edin. <br><strong>Kendi sektörünüzde bu marka eşsiz olabilir.</strong></span>
                                <span class="d-block" style="font-size: 17px; font-weight: 700;margin-top: 35px;">Nasıl mı?</span>
                                <span class="d-block mt-3" >Önüne ya da arkasına <strong>eklemeler yapılabilir.</strong>  <br>
                                        Markanızda küçük <strong>değişiklikler yapılabilir.</strong>
                                    </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class=" d-none d-md-block position-relative ">
                <div class="transparent-w-auto-card">
                    <div class="media-box-right">
                        <div class="media">
                            <?php
                            $kampanya = getPackageTypesOldPrice(1);
                            ?>

                            <div class="media-body ml-0">
                                <h5 class="mt-0 mb-0" style="font-weight:700">Hızlı Başvuru </h5>
                                @if(isset($kampanya))
                                <span class="gradient2 border-radius-10 fsize15 lheight22 fweight600 p-2 text-white position-absolute top-30px right-18px">{{getPackageKampanyaText(1)}}</span>
                                @endif
                                    <span class="d-block mt-4"><i class="bi bi-shield-fill-check"></i> 10 Yıl Boyunca Koruma</span>
                                <span class="d-block mt-2"><i class="bi bi-clock-fill"></i> 20 Dakikada Sistemde</span>
                                <span class="d-block mt-2"><i class="bi bi-person-fill"></i> Şirket ya da Şahıs Farketmez</span>
                                @if(isset($kampanya))
                                <span class="d-block w-100 text-right mt-1 old_price" ><span class="strike">{{$kampanya}} ₺ <span style="font-size: 18px">+ KDV</span></span></span>
                                @endif
                                <span class="d-block w-100 text-right @if(isset($kampanya)) mt-1 @else mt-4 @endif" style="font-size: 24px;font-weight:700">{{ getPackageTypes(1) }} ₺ <span style="font-size: 18px">+ KDV</span></span>
                                <a href="{{route('basvuru',['type'=>$package->slug, 'visitor' => $visitor, 'search_id' => $search_id])}}"  class="btn btn-outline-success btn-block mt-4 py-3 online-basvuru" style="font-weight: 700;letter-spacing: 0.3em">HEMEN BAŞVUR</a>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="refund-guarantee d-flex flex-row position-absolute w-100">
                    <div class="mr-3"><i class="bi bi-lock-fill"></i></div>
                    <div class="d-flex flex-column">
                        <span class="font-weight-bold">İADE GARANTİSİ!</span>
                        <span class="" style="font-size: 13px;" >Red Durumunda Ücretsiz 2. Başvuru</span>
                    </div>

                </div>
            </div>
        </div>


    </section>

    <section class="container" style="max-width:960px;">
        <div class="row">
            <h3 class="h3-baslik mb-lg-4 mb-4 mt-4font-weight-bold">Aramanızla İlişkili Sonuçlar</h3>

            <div id="arama_sonuclari" class="w-100" style="padding-left: 15px;padding-right: 15px;">

            </div>



        </div>
    </section>

    <section class="container mb-md-5 mt-md-5" style="max-width:960px;">
        <div class="row">
            <div class="search-info mb-md-5">
                <div class="container">
                    <div class="row">
                        <div class="col-md-2 text-center">
                            <i class="bi bi-info-circle-fill" style="font-size: 76px;color: white;"></i>
                        </div>
                        <div class="col-md-9">
                            <p class="mb-4" style="font-size: 17px;">Araştırma sonuçları bilgi amaçlı olup, bu sonuçlara göre yapılacak marka başvuruları tescil edilir ya da edilemez kesin yargısına varılmamalıdır.</p>
                            <p style="font-size: 17px;">Detaylı bilgi için 444 7 932 numaralı hattan ya da Whatsapp hattımızdan bizimle iletişime geçebilirsiniz.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="container d-md-none mt-5 mb-5 pb-5 pt-3">
        <div class="row">
            <div class="col">
                <div class="position-relative mx-auto">
                    <div class="transparent-w-auto-card" style="box-shadow: 0px 0px 10px rgba(5, 5, 5, 0.15), inset 1px 1px 1px #FFFFFF;">
                        <div class="media-box-right">
                            <div class="media">

                                <div class="media-body ml-0">
                                    <h5 class="mt-0 mb-0" style="font-weight:700">Hızlı Başvuru </h5>
                                    @if(isset($kampanya))
                                        <span class="gradient2 border-radius-10 fsize15 lheight22 fweight600 p-2 text-white position-absolute top-30px right-18px">{{getPackageKampanyaText(1)}}</span>
                                    @endif
                                    <span class="d-block mt-4"><i class="bi bi-shield-fill-check"></i> 10 Yıl Boyunca Koruma</span>
                                    <span class="d-block mt-2"><i class="bi bi-clock-fill"></i> 20 Dakikada Sistemde</span>
                                    <span class="d-block mt-2"><i class="bi bi-person-fill"></i> Şirket ya da Şahıs Farketmez</span>
                                    @if(isset($kampanya))
                                        <span class="d-block w-100 text-right mt-1 old_price" ><span class="strike">{{$kampanya}} ₺ <span style="font-size: 18px">+ KDV</span></span></span>
                                    @endif
                                    <span class="d-block w-100 text-right @if(isset($kampanya)) mt-1 @else mt-4 @endif" style="font-size: 24px;font-weight:700">{{ getPackageTypes(1) }} ₺ <span style="font-size: 18px">+ KDV</span></span>
                                    <a  href="{{route('basvuru',['type'=>$package->slug, 'visitor' => $visitor, 'search_id' => $search_id])}}" data-url="online-marka-tescili" class="btn btn-outline-success btn-block rounded mt-4 py-3 online-basvuru" style="font-weight: 700;letter-spacing: 0.3em">HEMEN BAŞVUR</a>


                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="refund-guarantee d-flex flex-row position-absolute w-100">
                        <div class="mr-3"><i class="bi bi-lock-fill"></i></div>
                        <div class="d-flex flex-column">
                            <span class="font-weight-bold">İADE GARANTİSİ!</span>
                            <span class="" style="font-size: 13px;" >Red Durumunda Ücretsiz 2. Başvuru</span>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </section>

    <section class="yeni-sorgu yeni-sorgu-coklu" style="background-image: url({{asset('images/bg.jpg')}});">
        <div class="d-lg-flex align-items-lg-center">
            <div class="container position-relative space-2 space-0--lg mt-lg-8">
                <div class="row justify-content-lg-between ">
                    <div class="order-2 order-md-1 col-md-5 mt-md-5">
                        <form id="markaFormOne">
                            <div class="marka-form mt-3 mt-lg-0" style="height:auto;">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="marka" id="txtMarkWord"
                                           placeholder="MARKA ADI"/>
                                </div>
                                <div class="form-group">
                                    <select class="" name="sektor" id="sektor">
                                        @foreach($sectors as $sector)
                                            <option value="{{$sector->sinif_degeri}}">{{$sector->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <button type="submit"
                                        class="btn btn-block btn-primary"
                                        id="btnSearch"


                                >HEMEN SORGULA</button>
                            </div>
                        </form>

                    </div>

                    <div class="order-1 order-md-2 col-md-6 mb-7 mb-lg-0 mt-md-5">
                        <h1 class="hero-h1 text-white text-right mb-0 mt-4 font-weight-bolder">YENİDEN <br class="d-block"/> SORGULA</h1>
                        <span
                            class="hero-span d-block text-white  text-right w-100">Yeni bir marka ya da aynı <br
                                class="d-lg-block">markayı farklı sınıfta sorgula</span>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="container mt-5 pt-md-5" style="max-width:960px;">
        <div class="row">
            <span class="d-block d-md-none mt-5 pt-5 w-100"></span>
            <h3 class="h3-baslik mb-lg-4 font-weight-bold mt-5 ml-md-5">Süreç Nasıl İşliyor?</h3>

            <p class="ml-md-5 mt-4" style="font-size: 17px; line-height: 26px; margin-left: 15px;">Biz sizin için tüm süreci yönetiriz. Tüm süreçler avukatlar aracılığı ile tamamlanır. İşlemleriniz aynı gün içerisinde başlar ve tamamlanır. Tarafınıza işlemin yapıldığına dair resmi evrak numarası iletilir. Tüm süreç tarafımızdan takip edilir.
            </p>

        </div>
    </section>

    <section class="container marka-adim-section">
        <div class="row position-relative pb-100">
            <hr class="w-100 position-absolute info-hr d-none d-lg-block" style="top: 47px;">
            <div class="col-12 col-md-6 col-lg-4 mb-5 mb-lg-0">
                <div class="info ml-lg-auto">
                    <div class="icon text-center mb-4 mx-auto justify-content-center">
                        <div class="lines"><img src="{{asset('images/wallet.svg')}}" style="height:40px;margin-top:40px;"/></div>

                    </div>
                    <div class="info-content m-lg-auto">
                        <span class="d-block mb-2">ADIM 1</span>
                        <h4 class="info-title mb-3">Ödeme Süreci</h4>
                        <p>İster havale ile ister Kredi Kartı ile ödeyebilirsiniz.</p>
                    </div>

                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 mb-5 mb-lg-0">
                <div class="info m-lg-auto">
                    <div class="icon text-center mb-4 mx-auto justify-content-center">
                        <div class="lines"><img src="{{asset('images/calling.svg')}}" style="height:40px;margin-top:40px;"/></div>
                    </div>
                    <div class="info-content m-lg-auto">
                        <span class="d-block mb-2">ADIM 2</span>
                        <h4 class="info-title mb-3">Başvuru Numarası</h4>
                        <p>İşlemleriniz hemen başlar. 20 Dakika içerisinde sisteme geçirilir. Aynı gün içerisinde başvuru numaranız tarafınıza iletilir.</p>
                    </div>

                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 mb-5 mb-lg-0">
                <div class="info mr-lg-auto">
                    <div class="icon text-center mb-4 mx-auto justify-content-center">
                        <div class="lines"><img src="{{asset('images/document.svg')}}" style="height:40px;margin-top:40px;"/></div>
                    </div>
                    <div class="info-content m-lg-auto">
                        <span class="d-block mb-2">ADIM 3</span>
                        <h4 class="info-title mb-3">Belge Teslimi</h4>
                        <p>Tüm süreçler ile ilgili bilgilendirilirsiniz. Belge teslimi 4 ila 6 ay sürebilir.</p>
                    </div>

                </div>
            </div>
        </div>
    </section>

    @if($video->status == 1)
        <section class="container" style="max-width:960px;">
            <div class="row">
                <div class="col-12" >

                    <div class="service-box d-flex flex-column" data-bs-toggle="modal" data-bs-target="#myModal" @if((new \Jenssegers\Agent\Agent())->isDesktop())  style="background-image: url({{asset($video->image)}});
                             background-repeat: no-repeat;
                             background-position-x: right;
                             " @endif>
                        <i class="bi bi-play-circle-fill fsize46 lheight46" style="margin-top: 86px;margin-bottom:37px;"></i>
                        <span class="standart-baslik mb-3" style="z-index: 1">{{$video->name}}</span>
                        <p class="mb-5" @if((new \Jenssegers\Agent\Agent())->isDesktop()) style="width: 460px;z-index:1" @endif>{{$video->description}}</p>

                        @if((new \Jenssegers\Agent\Agent())->isDesktop())
                            <div class="position-absolute" style="background: linear-gradient(90deg, #FFFFFF 0%, rgba(255, 255, 255, 0) 84.94%);height: 319px; width:508px;left:353px;z-index: 0" ></div>
                        @endif
                    </div>

                </div>

            </div>
        </section>
    @endif

    <section class="container" style="max-width:960px;">
        <div class="row row-cols-1 row-cols-md-2">

            <div class="col">
                <div class="info-box d-flex flex-column">
                    <div class="d-flex baslik">Marka Tescil Süresi Kaç Yıl?</div>
                    <span class="d-block mt-4">Tescilli bir marka başvuru tarihi itibari ile <span class="font-weight-bold">10 yıl süre</span> ile koruma altına alınır. Markanızı 10 yıllık periyotlar halinde süresiz bir şekilde uzatabilirsiniz. Yenileme süreci tescil tarihinin tamamlanmasına 6 ay kala başlamaktadır. Bu süre geldiğinde sizleri bilgilendiririz.</span>
                </div>
            </div>
            <div class="col">
                <div class="info-box d-flex flex-column">
                    <div class="d-flex baslik ">Marka Sınıfları Ne Anlama Gelir?</div>
                    <span class="d-block mt-4">Her bir marka sınıfı farklı sektör yada hizmet alanını temsil etmektedir. Markanız bir veya birden fazla sektörde koruma altına alabilirsiniz.</span>
                </div>
            </div>
            <div class="col">
                <div class="info-box d-flex flex-column">
                    <div class="d-flex baslik ">Başvuru için Gerekli Belgeler Nelerdir?</div>
                    <span class="d-block mt-4">
                        -Varsa logonuz,<br>
                        -Adres bilginiz,<br>
                        -Şirket yada Kişisel Bilgileriniz ile başvurunuz aynı gün içerisinde yapılır.</span>
                </div>
            </div>
            <div class="col">
                <div class="info-box d-flex flex-column">
                    <div class="d-flex baslik ">Vekilimiz Kim?</div>
                    <span class="d-block mt-4">Şirketimizi vekili Bursa Barosuna bağlı avukattır. Tüm başvuru ve tescil süreçleri avukat aracılığı ile tamamlanır.</span>
                </div>
            </div>
        </div>

    </section>

    <section class="container" style="max-width:960px;">
        <div class="row">
            <div class="col">
                <div class="d-flex flex-row info-box " style="min-height: auto" >
                    <div class="d-flex flex-column flex-md-row w-100 justify-content-between">
                        <div class="d-flex flex-column col">
                            <div class="d-flex baslik"><a href="#">Sorularınız mı var?</a></div>
                            <span class="d-block mt-4 mb-4 mb-md-0">Size 30 dakikada ulaşmamız için tıklayın sizin sorgunuzu önceliklendirelim!</span>
                        </div>
                        <div class="d-flex col">
                            <a href="https://api.whatsapp.com/send?phone=905333859013&text=" class="btn btn-third my-auto d-flex align-items-center justify-content-center w-100">HEMEN ULAŞIN</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">


                <div class="modal-body">

                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <!-- 16:9 aspect ratio -->
                    <div class="embed-responsive embed-responsive-16by9">
                        {!! $video->embed  !!}

                    </div>


                </div>

            </div>
        </div>
    </div>
    <div class="modal"></div>
    <div id="lottie" ></div>
    <div id="overlay"></div>
@endsection

@section('css')
    <link href="{{asset('js/select2.min.css')}}" rel="stylesheet"/>
    <style>
        .modal-dialog {
            max-width: 800px;
            margin: 30px auto;
        }

        .modal-content{
            background: none!important;
            border:none;
        }

        .modal-body {
            position:relative;
            padding:0px;
        }
        .close {
            position:absolute;
            right:-30px;
            top:0;
            z-index:999;
            font-size:2rem;
            font-weight: normal;
            color:#fff;
            opacity:1;
        }
        .grecaptcha-badge{
            opacity: 0;
            z-index: -9999;
            left:0
        }
    </style>
@endsection

@section('js')
    <script>
        var myModalEl = document.getElementById('myModal')
        myModalEl.addEventListener('hidden.bs.modal', function (event) {
            $("#myModal iframe").attr("src", $("#myModal iframe").attr("src"));
        })

        $('#arastirilan_marka').html(JSON.parse(localStorage.getItem('markWord')));
        $('#arastirilan_sektor').html(JSON.parse(localStorage.getItem('sector_name')));
        let sonuclar = JSON.parse(localStorage.getItem('sonuc_data'));
        var i;
        let sonuc_html = "";
        let sonucImg = "";
        let dividerImg = "{{asset('images/vertical-divider.svg')}}";
        for (i = 0; i < sonuclar.length; i++) {
            if(sonuclar[i].LogoUrl != null){
                sonucImg = '<img src="'+ sonuclar[i].LogoUrl+'" style="width: 80px; height:80px;border-radius: 20px">'
            }else{
                sonucImg = "";
            }
            sonuc_html = '<div class="result_card d-flex flex-wrap flex-md-nowrap flex-lg-row w-100 mb-3">'+

                '<div class="mr-3 d-none d-md-block order-0" style="width: 80px;">'+sonucImg+'</div>'+

                '<div class="d-flex flex-column order-1 marka">\
                    <span class="d-block baslik mb-md-2">MARKA</span>\
                    <span class="d-block d-flex overflow-hidden" style="height: 76px;">\
                        <span class="align-self-center">'+ sonuclar[i].Marka+'</span>\
    </span>\
</div>\
<img src="'+ dividerImg +'" class="mx-3 order-2">\
<div class="d-flex flex-column order-3 siniflar">\
    <span class="d-block baslik mb-md-2">SINIFLAR</span>\
    <span class="d-block d-flex" style="height: 76px;">\
        <span class="align-self-center overflow-hidden" style="max-height:76px;">'+ sonuclar[i].Siniflar+'</span>\
    </span>\
</div>\
<img src="'+ dividerImg +'" class="mx-3 order-4 d-none d-md-block">\
<div class="d-flex flex-column order-7 order-md-5 durum">\
    <span class="d-block baslik mb-md-2">DURUM</span>\
    <div class="d-flex font-weight-bolder" style="height: 76px;">\
        <span class="align-self-center">'+ sonuclar[i].Durumu+'</span>\
    </div>\
</div>\
<img src="'+ dividerImg +'" class="mx-3 order-8 d-none d-md-block">\
<div class="d-flex flex-column order-5 order-md-7 tescil_tarihi">\
    <span class="d-block baslik mb-md-2">TESCİL TARİHİ</span>\
    <div class="d-block d-flex" style="height: 76px;">\
        <span class="align-self-center">'+ sonuclar[i].TescilTarihi+'</span>\
    </div>\
</div>\
<img src="'+ dividerImg +'" class="mx-3 order-6">\
<div class="d-flex flex-column order-9 sahibi">\
    <span class="d-block baslik mb-md-2">SAHİBİ</span>\
    <span class="d-block d-flex" style="height:76px;font-size: 11px">\
        <span class="align-self-center">'+ sonuclar[i].Sahibi+'</span>'+
                '</span>'+
                '</div></div>';
            $('#arama_sonuclari').append(sonuc_html);
        }


    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bodymovin/5.7.6/lottie.min.js"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>
    <script src="{{asset('js/sweetalert2.min.js')}}"></script>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"></script>
    <script>
        $(document).ready(function () {
            @if((new \Jenssegers\Agent\Agent())->isDesktop())

            $('#sektor').select2();

            @endif

            if (localStorage.getItem('markWord')){$('#txtMarkWord').val(JSON.parse(localStorage.getItem('markWord')))}
            if (localStorage.getItem('isim')){$('#isim').val(JSON.parse(localStorage.getItem('isim')))}
            if (localStorage.getItem('telefon')){$('#telefon').val(JSON.parse(localStorage.getItem('telefon')))}

        });
        @php
            $url = str_replace('http://', 'https://', route('ajax-application-store'));
            $url2 = str_replace('http://', 'https://', route('index'));
        @endphp

        $('.online-basvuru').click(function (){

            $.ajax({

                url: "",
                type: "POST",
                xhrFields: {
                    withCredentials: true
                },
                data: {
                    markWord: markWord,
                    draw: 1,
                    start: 0,
                    length: 30,
                    sectors: sectors,
                    grecaptchaResponse: grecaptchaResponse
                },
                success: function (data) {
                }
            });


        });

    </script>

    <script>
        var captchaWidgetId;
        var onloadCallback = function() {
            captchaWidgetId = grecaptcha.render('btnSearch', {
                'sitekey' : '6LfNsoAaAAAAAA2Xg9amYHUXbMPJEnmQlMvUC_nx',
                'callback' : onSubmit
            });
        };
        var onSubmit = function(token) {
            //console.log ('token'+token)
            if (ValidateSearchParameters() === true) {
                document.getElementById("overlay").style.display = "block";
                document.getElementById("lottie").style.display = "block";
                var animationLottie = bodymovin.loadAnimation({
                    container: document.getElementById('lottie'), // Required
                    path: '{{asset('assets/animation.json')}}', // Required
                    renderer: 'svg/canvas/html', // Required
                    loop: true, // Optional
                    autoplay: false, // Optional

                })
                animationLottie.play()


                Search(token)
            }
            grecaptcha.reset(captchaWidgetId)
        };
        $('#btnSearch').on('click', function(e) {
            e.preventDefault();
        });

        $(document).ready(function () {
            @if((new \Jenssegers\Agent\Agent())->isDesktop())

            $('#sektor').select2();

            @endif

            if (localStorage.getItem('markWord')){$('#txtMarkWord').val(JSON.parse(localStorage.getItem('markWord')))}


        });

        var captchaWidgetId;
        var recaptchaCalls = 0;



        function GetCheckedSectors() {
            var checkedSectors = $("#sektor").val();
            return checkedSectors;
        }

        function ValidateSearchParameters() {
            if ($("#txtMarkWord").val().includes("<") || $("#txtMarkWord").val().includes(">")) {
                swal({
                    title: '',
                    text: "Araştırılacak marka '<' veya '>' karakterlerini içermemelidir.",
                    type: 'warning',
                    confirmButtonText: 'Tamam'
                });
                return false;
            }

            var searchWord = $("#txtMarkWord").val();

            if(searchWord.length == 3){
                searchWord = searchWord + ' ';
            }
            if (searchWord.length <= 3 || searchWord.length > 50) {
                swal({
                    title: '',
                    text: "Araştırılacak marka en az 3 en çok 50 karakter uzunluğunda olmalıdır.",
                    type: 'warning',
                    confirmButtonText: 'Tamam'
                });
                return false;
            }

            if (GetCheckedSectors().length === 0) {
                swal({
                    title: '',
                    text: "Araştırılacak sektör veya sınıfları belirtmediniz.",
                    type: 'warning',
                    confirmButtonText: 'Tamam'
                });
                return false;
            }


            return true;
        }

        function Search(token) {

            var searchWord = $("#txtMarkWord").val();

            if(searchWord.length == 3){
                searchWord = searchWord + ' ';
            }
            var markWord = searchWord;
            var isim = JSON.parse(localStorage.getItem('isim'));
            var telefon = JSON.parse(localStorage.getItem('telefon'));
            var sectors = GetCheckedSectors();
            var sector_name = $("#sektor option:selected").text();
            var checkbox = $("#iletisimIzni");
            var iletisimIzni = JSON.parse(localStorage.getItem('iletisimIzni'));

            var grecaptchaResponse = token;

            $.ajax({

                url: "https://www.markanabak.com.tr/MarkanaBak/api/v1/TrademarkSearch.ashx",
                type: "POST",
                xhrFields: {
                    withCredentials: true
                },
                data: {
                    markWord: markWord,
                    draw: 1,
                    start: 0,
                    length: 30,
                    sectors: sectors,
                    grecaptchaResponse: grecaptchaResponse,
                    exactSearch:true
                },


                beforeSend: function () {

                },
                complete: function () {

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    swal({
                        title: '',
                        text: jqXHR.responseText,
                        type: 'warning',
                        confirmButtonText: 'Tamam'
                    });
                },
                success: function( data )
                {

                    veri = JSON.parse(data);
                    localStorage.setItem('markWord', JSON.stringify(markWord));

                    localStorage.setItem('sectors', JSON.stringify(sectors));
                    localStorage.setItem('sector_name', JSON.stringify(sector_name));


                    localStorage.setItem('sonuc_data', JSON.stringify(veri.data));
                    localStorage.setItem('sonuc_draw', JSON.stringify(veri.draw));
                    localStorage.setItem('sonuc_recordsFiltered', JSON.stringify(veri.recordsFiltered));
                    localStorage.setItem('sonuc_recordsTotal', JSON.stringify(veri.recordsTotal));
//
                    @php
                        $url = str_replace('http://', 'https://', route('ajax-application-store'));
                    @endphp

                    var visitor = localStorage.getItem("visitor") ? JSON.parse(localStorage.getItem("visitor")) : null;
                    $.getJSON("{{ $url }}?type=1&mark="+ JSON.stringify(markWord) + "&name=" + JSON.stringify(isim) + "&phone=" + JSON.stringify(telefon) + "&sector=" + JSON.stringify(sector_name)+ "&sonuc_sayisi=" + JSON.parse(localStorage.getItem('sonuc_recordsTotal'))+ "&iletisimizni=" + JSON.stringify(iletisimIzni) + "&visitor=" + visitor, function(result){
                        localStorage.setItem('visitor', JSON.stringify(result.visitor));
                        localStorage.setItem('search_id', JSON.stringify(result.search_id));

                        // document.getElementById('btnSearch').removeAttribute("disabled");
                        document.getElementById("overlay").style.display = "none";
                        document.getElementById("lottie").style.display = "none";

                        window.location.href = "/marka-sonuc/"+iletisimIzni+"/"+markWord+"/"+JSON.parse(localStorage.getItem('visitor'))+"/"+JSON.parse(localStorage.getItem('search_id'))+"";
                    });

                    //




                },

            });
        }

        $(".online-basvuru-diger").click(function(){
            var visitor = localStorage.getItem("visitor") ? JSON.parse(localStorage.getItem("visitor")) : null;
            var url = $(this).data('url');
            var param1 = '';
            if(visitor != null){
                param1 = '/'+visitor;
            }
            $(location).attr('href','/online/'+url+param1);

        });

        function onSubmit(token) {
            //document.getElementById("markaFormOne").submit();
        }




    </script>

@endsection
