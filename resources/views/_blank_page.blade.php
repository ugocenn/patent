@extends('frontend.app')
@section('title','Patent Sorgu')

@section('content')
    <div class="bg-img-hero-content" style="background-image: url({{asset('images/bg.jpg')}});">
        <!-- Main Content -->
        <div class="d-lg-flex align-items-lg-center title-container">
            <div class="container position-relative space-2 space-0--lg mt-lg-8">
                <div class="row justify-content-lg-between ">
                    <h1 class="hero-h1 text-white text-center mb-0 w-100 ">BAŞLIK</h1>
                    <span class="hero-lead d-block text-center text-white w-100 mt-4">
                        varsa alt metin
                    </span>
                </div>

            </div>
        </div>
    </div>

    <section class="container-fluid bg-light pt-5" >
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="ml-100 pb-3">
                        <span class="d-block subtitle">ÜST BİLGİ</span>
                        <h2>İçerik başlık</h2>
                        <span class="d-block lead mb-4">ALT BİLGİ</span>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="container py-5" >
            <div class="row">
                <div class="col-12">

                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vel dictum sem. Aenean ac placerat nisi, eu gravida sapien. Proin purus sapien, interdum ut venenatis ut, semper ac est. Curabitur bibendum scelerisque quam faucibus venenatis. Vivamus mattis neque vitae ex ullamcorper ullamcorper. Maecenas cursus felis consequat, sagittis nisl eget, pellentesque diam. In risus augue, varius ac mauris ac, porttitor blandit felis. Aenean maximus placerat nisl, sit amet molestie sapien. Nulla fringilla blandit arcu, nec feugiat quam porta at. Quisque blandit sagittis imperdiet. Pellentesque fringilla lacus in augue tincidunt efficitur.
                        </p>
                        <p>
                            Donec nulla augue, maximus non cursus at, ornare sit amet mi. Nullam tristique elit et enim viverra, in feugiat diam vulputate. Vestibulum venenatis ultricies cursus. Cras non lacinia neque. Phasellus erat nulla, tempus ut dui id, placerat tempus odio. Vestibulum egestas eleifend sodales. Pellentesque diam dolor, dictum quis consectetur in, pretium at justo. Nullam odio massa, placerat id euismod non, consequat et enim. Donec rutrum suscipit sollicitudin. Mauris mattis ligula dictum porttitor tincidunt. Nam enim ante, sodales sed lacus eu, rutrum tempor urna. Nullam rutrum suscipit felis porta bibendum. Maecenas sapien erat, sagittis ac consectetur eget, ornare a odio. Praesent sed arcu tempus, porttitor urna eget, fringilla lorem. Nunc id mi nec lacus consectetur tempus ac et enim. Quisque eleifend dui sit amet cursus interdum.
                        </p>
                        <p>
                            Praesent a turpis nec nibh dignissim efficitur. Quisque eget pellentesque quam, et tincidunt sem. Cras urna tellus, rutrum et pretium nec, cursus eget nulla. Nunc imperdiet efficitur viverra. Etiam viverra laoreet maximus. Nam sodales risus tellus, at pulvinar urna egestas quis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent sagittis posuere arcu ut porta. Proin at sollicitudin mauris. Suspendisse dapibus in lectus a scelerisque. Quisque elementum justo nec ante mollis eleifend.
                        </p>
                        <p>
                            Mauris tincidunt posuere justo, vitae bibendum nibh vestibulum ut. Curabitur elementum urna nec lacus faucibus, a dignissim nisi hendrerit. Aliquam lectus purus, vestibulum ac suscipit vitae, vestibulum eu eros. Nulla non arcu felis. Nam vitae sollicitudin risus. Aenean condimentum dolor quam, sed malesuada elit faucibus et. Sed finibus quis orci a maximus. Fusce varius ex leo, a fermentum tellus cursus at. Curabitur placerat elementum massa a interdum. Vestibulum sed est ante. Sed faucibus sapien non ultricies tincidunt. Cras pellentesque tellus ipsum, ac malesuada est laoreet sed.
                        </p>
                        <p>
                            Vestibulum eu suscipit lectus. Curabitur fermentum velit faucibus, sollicitudin arcu sed, sodales erat. Praesent elementum sem nunc, id volutpat magna molestie vel. Aenean sit amet nunc volutpat, tempor massa non, congue magna. Morbi condimentum sem sit amet mollis vestibulum. Sed lacinia dolor et rhoncus ullamcorper. Ut tincidunt malesuada massa eu accumsan. Curabitur posuere pulvinar diam a fermentum. Quisque vitae libero feugiat, consequat felis quis, fringilla elit. Praesent tempus magna vel erat maximus ornare. Sed nec tortor justo. Aliquam eget venenatis lectus. Fusce hendrerit blandit hendrerit.
                        </p>
                </div>
            </div>

    </section>
@endsection

@section('css')
@endsection

@section('js')
@endsection
