<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">
    <meta name="facebook-domain-verification" content="jv3efyl4q1t1ldal0v9n3k6qmcnqx9" />

    <link href="{{asset('bootstrap/dist/css/bootstrap.css?v=1.2')}}" rel="stylesheet"/>

    <link rel="canonical" href="{{ url()->current() }}">
    @yield('css')
    <link href="{{asset('css/theme.css?v=1.5')}}" rel="stylesheet"/>
    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('favicon/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('favicon/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('favicon/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('favicon/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('favicon/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('favicon/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('favicon/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('favicon/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('favicon/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{asset('favicon/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('favicon/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('favicon/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('favicon/manifest.json')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="images/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
</head>
<body>
{!! $option->google_tags !!}
<header>
    <div class="container">
        <nav class="navbar navbar-expand-md navbar-dark d-flex">
            <div class="flex-shrink-0 logo">
                <a href="{{URL::to('/')}}">
                    <img src="{{asset('images/logo.svg')}}" class="img-fluid mw-100 h-auto" alt="Patent Sorgu" title="Patent Sorgu"/>
                </a>

            </div>
            <div class="flex-grow-1">
                <ul class="navbar-nav me-auto mb-md-0 justify-content-end">
                    <li class="nav-item @if(Request::segment(3)=='biz-kimiz') active @endif d-none d-sm-block">
                        <a class="nav-link pr-4" aria-current="page" href="https://www.patentsorgu.com/biz-kimiz">BİZ KİMİZ</a>
                    </li>
                    <li class="nav-item @if(Request::segment(1)=='rehberler') active @endif d-none d-sm-block">
                        <a class="nav-link pr-4" aria-current="page" href="{{route('rehberler')}}">REHBERLER</a>
                    </li>
                    <li class="nav-item nav-item-btn">
                        <a class="btn btn-sm btn-secondary" href="tel:{{$option->telefon}}" role="button">
                            {{$option->telefon}}
                        </a>
                    </li>
                    <li class="nav-item nav-item-btn">
                        <a class="pl-2 pl-sm-3" href="https://api.whatsapp.com/send?phone={!! $option->whatsapp !!}&text=" role="button">
                            <img src="{{asset('images/whatsapp.svg')}}" class="img-fluid mw-100 h-auto wp-top" />
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</header>

@yield('content')

<footer class="bg-dark-blue text-white">
    <div class="container ">
        <div class="footer-top d-flex justify-content-between flex-lg-row flex-column">
            <div class="text-center text-lg-left">
                <img src="{{asset('images/logo-vertical.svg')}}" class="footer-logo d-block mx-auto"  />

                <p class="footer-content d-block mt-5">Her türlü, tasarım tescil, yenileme, adres değişikliği, ünvan değişikliği gibi işlemlerde yanınızdayız.</p>
            </div>
            <div class="pt-90 text-center text-lg-right">
                <a class="btn btn-sm btn-secondary" href="tel:4447932" role="button">
                    444 7 932
                </a>

                <a class="pl-3" href="https://api.whatsapp.com/send?phone=904447932&text=" role="button">
                    <img src="{{asset('images/whatsapp.svg')}}" class="img-fluid mw-100 h-auto wp-top">
                </a>

                <div class="d-block social-media-title">Sosyal Medya'da Takip Edin</div>

                <div class="d-block social-icons mb-3 mb-lg-3">
                    <a href="{!! $option->facebook !!}" target="_blank" class="mr-4"><i class="bi bi-facebook"></i></a>
                    <a href="{!! $option->instagram !!}" target="_blank" class="mr-4"><i class="bi bi-instagram"></i></a>
                    <a href="{!! $option->linkedin !!}" target="_blank" class="mr-4"><i class="bi bi-linkedin"></i></a>
                    <a href="{!! $option->youtube !!}" target="_blank" class="mr-0"><i class="bi bi-youtube"></i></a>
                </div>

            </div>
        </div>
        <div class="footer-menu d-flex flex-column flex-md-row justify-content-between">
            <ul class="list-inline mb-0 mt-2 mt-md-0 text-center text-lg-left">
                <li class="list-inline-item d-block d-md-inline-block "> <a class="text-reset" href="{{url('./biz-kimiz')}}">Biz Kimiz </a></li>
                <li class="list-inline-item d-block d-md-inline-block"> <a class="text-reset" href="{{url('./teslimat-ve-iade-politikasi')}}">Teslimat ve İade Politikası </a></li>
                <li class="list-inline-item d-block d-md-inline-block"> <a class="text-reset" href="{{url('./yasal-bilgiler')}}">Yasal Bilgiler </a></li>
                <li class="list-inline-item d-block d-md-inline-block"> <a class="text-reset" href="{{url('./mesafeli-satis-sozlesmesi')}}">Mesafeli Satış Sözleşmesi </a></li>
                <li class="list-inline-item d-block d-md-inline-block"> <a class="text-reset" href="{{url('./kvkk-ve-gizlilik-politikasi')}}">Gizlilik Politikası </a></li>
            </ul>
            <img src="{{asset('images/credit-card-white.svg')}}" class="mt-4 mt-md-0"/>
        </div>

    </div>
    <div class="bg-footer-bottom w-100 footer-bottom ">
        <div class="container p-3 text-center text-lg-right">
            <span class="d-block">© 2021 Deha Patent Kuruluşudur.</span>
        </div>

    </div>
</footer>
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>


@yield('js')
</body>
</html>
