<script>
    tinymce.init({
        selector: '.editorezy',
        language: 'tr',
        menubar: false,
        plugins: [
            'advlist autolink lists image charmap print preview anchor textcolor',
            'searchreplace visualblocks code',
            'insertdatetime table contextmenu paste code moxiemanager autoresize link'
        ],

        toolbar: 'undo redo | insert | styleselect | table | fontsizeselect bold italic underline forecolor | alignleft aligncenter alignright | bullist numlist outdent indent | image code link',
        fontsize_formats: '7px 8px 9px 10px 11px 12px 14px 16px 18px 24px 36px 48px',
        content_css: '/assets/css/pdf.css',
        statusbar: true,
        moxiemanager_filelist_context_menu: 'copy paste | view edit download addfavorite | zip unzip',
        moxiemanager_filelist_manage_menu: 'copy paste | view edit download addfavorite | zip unzip',
        moxiemanager_filelist_main_toolbar: 'upload manage'
    });
</script>
