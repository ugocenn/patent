@if(count($errors))
      <div class="alert alert-warning alert-bordered">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <ul>
          @foreach($errors->all() as $error)
          <li>{!! $error !!}</li>
          @endforeach
        </ul>
      </div>
    @endif


    @if(session('message'))
    <div class="alert alert-success alert-bordered">
      <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
      <strong>{!! session('message')!!}</strong>
    </div>
    @endif
    @if(strlen(session()->get('danger'))>0)
    <div class="alert alert-danger alert-bordered">
									<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                  <ul>
                      <li>{!! session()->get('danger') !!}</li>
                  </ul>
							    </div>
    @endif
