@extends('layouts.app')
@section('title', 'Video')
@section('content')
    <div class="content ">
        <br/>

        <div class=" container-fluid   container-fixed-lg">
            {!! Form::open(['route' => 'video.save','id'=>'admin-form','role'=>'form','autocomplete'=>'off', 'novalidate' => 'novalidate'])!!}
            <div class="card card-default">
                <div class="card-header ">
                    <div class="card-title">Video
                    </div>
                    <div class="pull-right">
                        <button aria-label="" class="btn btn-success btn-icon-left m-b-10" type="submit"><i class="pg-icon">tick</i><span class=""> KAYDET</span></button>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                    @include('partials.alerts.error')

                    <div class="row clearfix">
                        <div class="col-xl-12">
                            <div class="form-group form-group-default required">
                                <label>Video Name</label>
                                {!! Form::text("name",$video->name,['class'=>'form-control','required'=>'required','id'=>'name']) !!}
                            </div>
                            <div class="form-group form-group-default required">
                                <label>Video Açıklama</label>
                                {!! Form::text("description",$video->description,['class'=>'form-control','required'=>'required','id'=>'description']) !!}
                            </div>
                            <div class="form-group form-group-default input-group">
                                <div class="form-input-group">
                                    <label>Resim (590*318)</label>
                                    {!!  Form::text('image', $video->image, ['required','class'=>'form-control','id'=>'image'])!!}
                                </div>
                                <div class="input-group-append ">
              <span class="input-group-text"><a href="javascript:;" onclick="moxman.browse({extensions:'jpg,jpeg,png,gif,svg',fields: 'image', no_host: true});" class="btn btn-info"><i class="glyphicon glyphicon-plus"></i> Ekle</a>
              </span>
                                </div>
                            </div>
                            <div class="form-group form-group-default ">
                                <label>Embed</label>
                                {!! Form::textarea("embed",$video->embed,['class'=>'form-control','id'=>'embed', 'style'=>'height:200px;']) !!}
                            </div>
                            <div class="form-group form-group-default ">
                                <label>Status</label>

                                {!! Form::select("status", ['0' => 'HAYIR', '1' => 'EVET'], $video->status, ['class'=>'form-control','id'=>'status']) !!}
                            </div>

                            <textarea rows="2" cols="2" class='form-control elastic editorezy' style="display:none;"></textarea>

                        </div>

                    </div>
                </div>
            </div>
            {!! Form::close() !!}
            <!-- END card -->
        </div>
        <!-- END CONTAINER FLUID -->
    </div>

@endsection
@push('scripts')
    @include('macros.editors')
@endpush




