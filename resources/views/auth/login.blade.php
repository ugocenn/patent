@extends('layouts.login_app')

@section('content')
    <div class="login-wrapper ">
        <!-- START Login Background Pic Wrapper-->
        <div class="bg-pic">
            <!-- START Background Caption-->
            <div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
                <h1 class="semi-bold text-white">
                    Markanızı / Patentinizi ücretsiz olarak sorgulayın. Sorgu sonuçlarını hemen görüntüleyin.</h1>
                <p class="small">
                    Patentsorgu.com Marka, Marka Tescili, Patent, Endüstriyel Tasarım, Marka Sorgulama, Faydalı Model konularında destek vermektedir.
                </p>
            </div>
            <!-- END Background Caption-->
        </div>
        <!-- END Login Background Pic Wrapper-->
        <!-- START Login Right Container-->
        <div class="login-container bg-white">
            <div class="p-l-50 p-r-50 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
                <img src="{{ url('assets/img/logo.svg') }}" alt="logo" data-src="{{ url('assets/img/logo.svg') }}" data-src-retina="{{ url('assets/img/logo.svg') }}" width="250">
                <h2 class="p-t-25">Patent & Marka Sorgulama</h2>
                <p class="m-t-1">Markanızı / Patentinizi ücretsiz olarak sorgulayın. Sorgu sonuçlarını hemen görüntüleyin.</p>
                <!-- START Login Form -->
                <form id="form-login" class="p-t-15" role="form" method="POST" action="{{ route('login') }}">
                    @csrf
                    <!-- START Form Control-->
                    <div class="form-group form-group-default">
                        <label>E-posta</label>
                        <div class="controls">
                            <input type="text" name="email" placeholder="E-posta adresinizi girin" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" required autocomplete="email" autofocus>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>
                    <!-- END Form Control-->
                    <!-- START Form Control-->
                    <div class="form-group form-group-default">
                        <label>Parola</label>
                        <div class="controls">
                            <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Parolanızı girin" required autocomplete="current-password">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>
                    <!-- START Form Control-->
                    <div class="row">
                        <div class="col-md-6 d-flex align-items-center justify-content-end">
                            <button aria-label="" class="btn btn-primary btn-lg m-t-10" type="submit">OTURUM AÇ</button>
                        </div>
                    </div>
                    <div class="m-b-5 m-t-30">

                    </div>
                    <div>

                    </div>
                    <!-- END Form Control-->
                </form>
                <!--END Login Form-->
                <div class="pull-bottom sm-pull-bottom">
                    <div class="m-b-30 p-r-80 sm-m-t-20 sm-p-r-15 sm-p-b-20 clearfix">
                        <div class="col-sm-9 no-padding m-t-10">
                            <p class="small-text normal hint-text">
                                ©2019-2020 Tüm Hakları Saklıdır. patentsorgu/® patentsorgu Ltd.Şti nin kayıtlı bir markasıdır. <a href="">Çerez Politikası</a>, <a href=""> Gizlilik ve Kurallar</a>.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Login Right Container-->
    </div>
@endsection
