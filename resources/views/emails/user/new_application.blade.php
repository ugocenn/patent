@component('mail::message')
# Deha
Sayın <b>Deha</b>;<br />{{ $search->package_type->name }} içi yapılan arama bilgileri aşağıdadır.<br/>Ad:{{ $search->customer->isim }}<br/>Sektör:{{ $search->sector }}<br/>Marka:{{ $search->mark }}<br/>Telefon:{{ $search->customer->tel }}<br/>Arama Tarihi:{{ $search->created_at }}

@endcomponent
