@component('mail::message')
# {{ $employee->company->name }} {{ $survey->name }}
Sayın <b>{{ $employee->name }}</b>;<br />Şirketiniz tarafından oluşturulan {{ $survey->name }} anketine  aşağıdaki butona tıklayarak ulaşabilirsiniz.<br/>

@component('mail::button', ['url' => route('surveys.live', createHashId($survey->id)).'?email='.$employee->email])
    Anketi Görüntüle
@endcomponent

@endcomponent
