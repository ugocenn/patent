@extends('layouts.app')
@section('title', 'Müşteriler')
@section('content')
    <div class="content ">
        <br/>
        <!-- START CONTAINER FLUID -->
        <div class=" container-fluid   container-fixed-lg">
            <!-- START card -->
            <div class="card card-default">
                <div class="card-header ">
                    <div class="card-title">Müşteriler
                    </div>
                    <div class="pull-right">

                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                    @include('partials.alerts.error')
                    <div class="table-responsive">
                        <table id="datatable" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>İsim</th>
                                <th>T.C.</th>
                                <th>E-posta</th>
                                <th>Telefon</th>
                                <th>Adres</th>
                                <th>Fatura Türü</th>
                                <th>İletişim İzni</th>
                                <th>Yapılan Arama Sayısı</th>
                                <th>İşlemler</th>
                            </tr>
                            </thead>


                            <tbody>
                            @foreach($customers as $customer)
                            <tr>
                                <td>{{ $customer->isim }}</td>
                                <td>{{ $customer->tc }}</td>
                                <td>{{ $customer->eposta }}</td>
                                <td>{{ $customer->tel }}</td>
                                <td>{{ $customer->adres }}</td>
                                <td>{{ $customer->fatura_turu }}</td>
                                <td>{!! $customer->iletisim_izni == 1 ? '<span class="label label-success">EVET</span>' :'<span class="label label-danger">HAYIR</span>' !!}</td>
                                <td>{{ count($customer->searchs) }}</td>
                                <td>
                                    <a href="javascript:void(0);" onclick="return confirmOpsDelete('{{ createHashId($customer->id) }}')" role="menuitem" class="text-danger">
                                        <i class="pg-trash"></i> Sil
                                    </a>
                                    {{ Form::open([
                                        'method' => 'DELETE',
                                        'url' => route("customers.destroy", createHashId($customer->id)),
                                        'id' => 'ops-delete-form-' . createHashId($customer->id)
                                        ]) }}
                                    {{ Form::close() }}
                                </td>

                            </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END card -->
        </div>
        <!-- END CONTAINER FLUID -->
    </div>
@endsection
