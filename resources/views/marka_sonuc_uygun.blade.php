@extends('frontend.app')
@section('title', $marka . ' Marka Sorgusu | '.config('app.name'))

@section('content')
    <div class="bg-img-hero-content" style="background-image: url({{asset('images/bg.jpg')}});">
        <!-- Main Content -->
        <div class="d-lg-flex align-items-lg-center title-container">
            <div class="container position-relative space-2 space-0--lg mt-lg-8">
                <div class="row justify-content-lg-between ">
                    <h1 class="hero-h1 text-white text-center mb-0 w-100 ">SONUÇLAR</h1>
                    <div class="hero-lead d-block text-center text-white w-100 mt-4 marka-sonuc-lead">
                        <div class="pink-degrade d-inline">“<div class="d-inline arastirilan_marka"></div>”</div> <div class="d-inline">sorgusu</div> <br class="d-md-none"><div class="d-inline pink-degrade">“<div class="d-inline" id="arastirilan_sektor"></div>”</div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <section class="container position-relative basvuru-box-container basvuru-box-container-uygun" style="max-width:960px;">

        <div class="row position-absolute w-100 flex-lg-row justify-content-center justify-content-lg-between media-box-left-container"  >
            <div class="col">
                <div class="transparent-w-auto-card">
                    <div class="media-box-left">
                        <div class="media">
                            <img src="{{asset('images/green-tick.svg')}}" class="mr-0 d-none d-lg-block" alt="">
                            <div class="media-body ml-0 ml-lg-5">
                                <h5 class="mt-0 mb-0 ">Bu Marka Şu An <span class="font-weight-bolder" >Boşta!</span> </h5>
                                <img src="{{asset('images/green-line-small.svg')}}" class="green-line-small">
                                <h5 class="mt-0 mb-0 ">Hemen Başvurun! </h5>
                                <span class="d-block mt-4">Sorgulamış olduğunuz <strong class="arastirilan_marka">Deha Patent</strong> başvuruya uygun.</span>
                                <span class="d-block" style="font-size: 17px; font-weight: 700;margin-top: 20px;">Neden Harika Bir İsim?</span>
                                <span class="d-block mt-3" >
                                    <span class="arastirilan_marka"></span> özgün bir isim!<br>
Telafuzu oldukça kolay!<br>
Oldukça Kısa Bir İsim  <br>

                                    </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class=" d-none d-lg-block position-relative">
                <div class="transparent-w-auto-card">
                    <div class="media-box-right">
                        <div class="media">
                            <?php
                            $kampanya = getPackageTypesOldPrice(1);
                            ?>

                            <div class="media-body ml-0">
                                <h5 class="mt-0 mb-0" style="font-weight:700">Hızlı Başvuru </h5>
                                @if(isset($kampanya))
                                    <span class="gradient2 border-radius-10 fsize15 lheight22 fweight600 p-2 text-white position-absolute top-30px right-18px">{{getPackageKampanyaText(1)}}</span>
                                @endif
                                <span class="d-block mt-4"><i class="bi bi-shield-fill-check"></i> 10 Yıl Boyunca Koruma</span>
                                <span class="d-block mt-2"><i class="bi bi-clock-fill"></i> 20 Dakikada Sistemde</span>
                                <span class="d-block mt-2"><i class="bi bi-person-fill"></i> Şirket ya da Şahıs Farketmez</span>
                                @if(isset($kampanya))
                                    <span class="d-block w-100 text-right mt-1 old_price" ><span class="strike">{{$kampanya}} ₺ <span style="font-size: 18px">+ KDV</span></span></span>
                                @endif
                                <span class="d-block w-100 text-right @if(isset($kampanya)) mt-1 @else mt-4 @endif" style="font-size: 24px;font-weight:700">{{ getPackageTypes(1) }} ₺ <span style="font-size: 18px">+ KDV</span></span>
                                <a href="{{route('basvuru',['type'=>$package->slug, 'visitor' => $visitor, 'search_id' => $search_id])}}" class="btn btn-outline-success btn-block mt-4 py-3 online-basvuru" style="border-radius: 10px;font-weight: 700;letter-spacing: 0.3em">HEMEN BAŞVUR</a>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="refund-guarantee d-flex flex-row position-absolute w-100">
                    <div class="mr-3"><i class="bi bi-lock-fill"></i></div>
                    <div class="d-flex flex-column">
                        <span class="font-weight-bold">İADE GARANTİSİ!</span>
                        <span class="" style="font-size: 13px;" >Red Durumunda Ücretsiz 2. Başvuru</span>
                    </div>

                </div>
            </div>
        </div>


    </section>





    <section class="container d-md-none mt-0 mb-5 pb-5 pt-0">
        <div class="row">
            <div class="col">
                <div class="position-relative mx-auto" >
                    <div class="transparent-w-auto-card" style="box-shadow: 0px 0px 10px rgba(5, 5, 5, 0.15), inset 1px 1px 1px #FFFFFF;">
                        <div class="media-box-right">
                            <div class="media">

                                <div class="media-body ml-0">
                                    <h5 class="mt-0 mb-0" style="font-weight:700">Hızlı Başvuru </h5>
                                    @if(isset($kampanya))
                                        <span class="gradient2 border-radius-10 fsize15 lheight22 fweight600 p-2 text-white position-absolute top-30px right-18px">{{getPackageKampanyaText(1)}}</span>
                                    @endif
                                    <span class="d-block mt-4"><i class="bi bi-shield-fill-check"></i> 10 Yıl Boyunca Koruma</span>
                                    <span class="d-block mt-2"><i class="bi bi-clock-fill"></i> 20 Dakikada Sistemde</span>
                                    <span class="d-block mt-2"><i class="bi bi-person-fill"></i> Şirket ya da Şahıs Farketmez</span>
                                    @if(isset($kampanya))
                                        <span class="d-block w-100 text-right mt-1 old_price" ><span class="strike">{{$kampanya}} ₺ <span style="font-size: 18px">+ KDV</span></span></span>
                                    @endif
                                    <span class="d-block w-100 text-right @if(isset($kampanya)) mt-1 @else mt-4 @endif" style="font-size: 24px;font-weight:700">{{ getPackageTypes(1) }} ₺ <span style="font-size: 18px">+ KDV</span></span>
                                    <a  href="{{route('basvuru',['type'=>$package->slug, 'visitor' => $visitor, 'search_id' => $search_id])}}" data-url="online-marka-tescili" class="btn btn-outline-success btn-block rounded mt-4 py-3 online-basvuru" style="font-weight: 700;letter-spacing: 0.3em">HEMEN BAŞVUR</a>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="refund-guarantee d-flex flex-row position-absolute w-100">
                        <div class="mr-3"><i class="bi bi-lock-fill"></i></div>
                        <div class="d-flex flex-column">
                            <span class="font-weight-bold">İADE GARANTİSİ!</span>
                            <span class="" style="font-size: 13px;" >Red Durumunda Ücretsiz 2. Başvuru</span>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </section>


    <section class="container mt-0 pt-0" style="max-width:960px;">
        <div class="row">

            <h3 class="h3-baslik mb-lg-4 font-weight-bold mt-md-5 ml-md-5">Süreç Nasıl İşliyor?</h3>

            <p class="ml-md-5 mt-4" style="font-size: 17px; line-height: 26px; margin-left: 15px;">Biz sizin için tüm süreci yönetiriz. Tüm süreçler avukatlar aracılığı ile tamamlanır. İşlemleriniz aynı gün içerisinde başlar ve tamamlanır. Tarafınıza işlemin yapıldığına dair resmi evrak numarası iletilir. Tüm süreç tarafımızdan takip edilir.
            </p>

        </div>
    </section>

    <section class="container marka-adim-section">
        <div class="row position-relative pb-100">
            <hr class="w-100 position-absolute info-hr d-none d-lg-block" style="top: 47px;">
            <div class="col-12 col-md-6 col-lg-4 mb-5 mb-lg-0">
                <div class="info ml-lg-auto">
                    <div class="icon text-center mb-4 mx-auto justify-content-center">
                        <div class="lines"><img src="{{asset('images/wallet.svg')}}" style="height:40px;margin-top:40px;"/></div>

                    </div>
                    <div class="info-content m-lg-auto">
                        <span class="d-block mb-2">ADIM 1</span>
                        <h4 class="info-title mb-3">Ödeme Süreci</h4>
                        <p>İster havale ile ister Kredi Kartı ile ödeyebilirsiniz.</p>
                    </div>

                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 mb-5 mb-lg-0">
                <div class="info m-lg-auto">
                    <div class="icon text-center mb-4 mx-auto justify-content-center">
                        <div class="lines"><img src="{{asset('images/calling.svg')}}" style="height:40px;margin-top:40px;"/></div>
                    </div>
                    <div class="info-content m-lg-auto">
                        <span class="d-block mb-2">ADIM 2</span>
                        <h4 class="info-title mb-3">Başvuru Numarası</h4>
                        <p>İşlemleriniz hemen başlar. 20 Dakika içerisinde sisteme geçirilir. Aynı gün içerisinde başvuru numaranız tarafınıza iletilir.</p>
                    </div>

                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 mb-5 mb-lg-0">
                <div class="info mr-lg-auto">
                    <div class="icon text-center mb-4 mx-auto justify-content-center">
                        <div class="lines"><img src="{{asset('images/document.svg')}}" style="height:40px;margin-top:40px;"/></div>
                    </div>
                    <div class="info-content m-lg-auto">
                        <span class="d-block mb-2">ADIM 3</span>
                        <h4 class="info-title mb-3">Belge Teslimi</h4>
                        <p>Tüm süreçler ile ilgili bilgilendirilirsiniz. Belge teslimi 4 ila 6 ay sürebilir.</p>
                    </div>

                </div>
            </div>
        </div>
    </section>

    @if($video->status == 1)
    <section class="container" style="max-width:960px;">
        <div class="row">
            <div class="col-12" >

                <div class="service-box d-flex flex-column" data-bs-toggle="modal" data-bs-target="#myModal"
                     @if((new \Jenssegers\Agent\Agent())->isDesktop())  style="background-image: url({{asset($video->image)}});
                         background-repeat: no-repeat;
                         background-position-x: right;
                         " @endif>
                    <i class="bi bi-play-circle-fill fsize46 lheight46" style="margin-top: 86px;margin-bottom:37px;"></i>
                    <span class="standart-baslik mb-3" style="z-index: 1">{{$video->name}}</span>
                    <p class="mb-5" @if((new \Jenssegers\Agent\Agent())->isDesktop()) style="width: 460px;z-index:1" @endif>{{$video->description}}</p>

                    @if((new \Jenssegers\Agent\Agent())->isDesktop())
                    <div class="position-absolute" style="background: linear-gradient(90deg, #FFFFFF 0%, rgba(255, 255, 255, 0) 84.94%);height: 319px; width:508px;left:353px;z-index: 0" ></div>
                    @endif
                </div>

            </div>

        </div>
    </section>
    @endif
    <section class="container" style="max-width:960px;">
        <div class="row row-cols-1 row-cols-md-2">
            <div class="col">
                <div class="info-box d-flex flex-column">
                    <div class="d-flex baslik">Marka Tescil Süresi Kaç Yıl?</div>
                    <span class="d-block mt-4">Tescilli bir marka başvuru tarihi itibari ile <span class="font-weight-bold">10 yıl süre</span> ile koruma altına alınır. Markanızı 10 yıllık periyotlar halinde süresiz bir şekilde uzatabilirsiniz. Yenileme süreci tescil tarihinin tamamlanmasına 6 ay kala başlamaktadır. Bu süre geldiğinde sizleri bilgilendiririz.</span>
                </div>
            </div>
            <div class="col">
                <div class="info-box d-flex flex-column">
                    <div class="d-flex baslik ">Marka Sınıfları Ne Anlama Gelir?</div>
                    <span class="d-block mt-4">Her bir marka sınıfı farklı sektör yada hizmet alanını temsil etmektedir. Markanız bir veya birden fazla sektörde koruma altına alabilirsiniz.</span>
                </div>
            </div>
            <div class="col">
                <div class="info-box d-flex flex-column">
                    <div class="d-flex baslik ">Başvuru için Gerekli Belgeler Nelerdir?</div>
                    <span class="d-block mt-4">
                        -Varsa logonuz,<br>
                        -Adres bilginiz,<br>
                        -Şirket yada Kişisel Bilgileriniz ile başvurunuz aynı gün içerisinde yapılır.</span>
                </div>
            </div>
            <div class="col">
                <div class="info-box d-flex flex-column">
                    <div class="d-flex baslik ">Vekilimiz Kim?</div>
                    <span class="d-block mt-4">Şirketimizi vekili Bursa Barosuna bağlı avukattır. Tüm başvuru ve tescil süreçleri avukat aracılığı ile tamamlanır.</span>
                </div>
            </div>
        </div>

    </section>

    <section class="container" style="max-width:960px;">
        <div class="row">
            <div class="col">
                <div class="d-flex flex-row info-box " style="min-height: auto" >
                    <div class="d-flex flex-column flex-md-row w-100 justify-content-between">
                        <div class="d-flex flex-column col">
                            <div class="d-flex baslik"><a href="#">Sorularınız mı var?</a></div>
                            <span class="d-block mt-4 mb-4 mb-md-0">Size 30 dakikada ulaşmamız için tıklayın sizin sorgunuzu önceliklendirelim!</span>
                        </div>
                        <div class="d-flex col">
                            <a href="https://api.whatsapp.com/send?phone=905333859013&text=" class="btn btn-third my-auto d-flex align-items-center justify-content-center w-100">HEMEN ULAŞIN</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">


                <div class="modal-body">

                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <!-- 16:9 aspect ratio -->
                    <div class="embed-responsive embed-responsive-16by9">
                        {!! $video->embed  !!}

                    </div>


                </div>

            </div>
        </div>
    </div>







@endsection

@section('css')
    <style>
        .modal-dialog {
            max-width: 800px;
            margin: 30px auto;
        }

        .modal-content{
            background: none!important;
            border:none;
        }

        .modal-body {
            position:relative;
            padding:0px;
        }
        .close {
            position:absolute;
            right:-30px;
            top:0;
            z-index:999;
            font-size:2rem;
            font-weight: normal;
            color:#fff;
            opacity:1;
        }
    </style>
@endsection

@section('js')
    <script>


        var myModalEl = document.getElementById('myModal')
        myModalEl.addEventListener('hidden.bs.modal', function (event) {
            $("#myModal iframe").attr("src", $("#myModal iframe").attr("src"));
        })



        $('.arastirilan_marka').html(JSON.parse(localStorage.getItem('markWord')));

        $('#arastirilan_sektor').html(JSON.parse(localStorage.getItem('sector_name')));


    </script>
@endsection
