<div class="row clearfix">
    <div class="col-xl-3">
        <div class="form-group form-group-default required">
            <label>Sayfa Başlığı</label>
            {!! Form::text("title",null,['class'=>'form-control','required'=>'required','id'=>'title']) !!}
        </div>
    </div>
    <div class="col-xl-3">
        <div class="form-group form-group-default required">
            <label>Sayfa Bağlantısı</label>
            {!! Form::text("slug",null,['class'=>'form-control','required'=>'required','id'=>'slug']) !!}
        </div>
    </div>
    <div class="col-xl-3">
        <div class="form-group form-group-default form-group-default-select2">
            <label>Kategori</label>
            {!! Form::select("category_id", $categories , null, [ 'class'=>'full-width','id'=>'category_id','required'=>'required', 'data-init-plugin' => 'select2']) !!}
        </div>
    </div>
    <div class="col-xl-3">
        <div class="form-group form-group-default form-group-default-select2">
            <label>Durumu</label>
            {!! Form::select("active", [1 => 'AKTİF', 0 => 'PASİF'] , null, ['data-placeholder' => 'Durum seçiniz', 'class'=>'full-width','id'=>'active','required'=>'required', 'data-init-plugin' => 'select2']) !!}
        </div>
    </div>
</div>



<div class="row clearfix">
    <div class="col-xl-4">
        <div class="form-group form-group-default input-group">
            <div class="form-input-group">
                <label>Sayfa Görseli</label>
                {!!  Form::text('image', null, ['required','class'=>'form-control','readonly'=>'readonly','id'=>'image'])!!}
            </div>
            <div class="input-group-append ">
              <span class="input-group-text"><a href="javascript:;" onclick="moxman.browse({extensions:'jpg,jpeg,png,gif',fields: 'image', no_host: true});" class="btn btn-info"><i class="glyphicon glyphicon-plus"></i> Ekle</a>
              </span>
            </div>
        </div>
    </div>
    <div class="col-xl-4">
        <div class="form-group form-group-default input-group">
            <div class="form-input-group">
                <label>Sayfa Detay Görseli</label>
                {!!  Form::text('page_header_image', null, ['required','class'=>'form-control','readonly'=>'readonly','id'=>'page_header_image'])!!}
            </div>
            <div class="input-group-append ">
              <span class="input-group-text"><a href="javascript:;" onclick="moxman.browse({extensions:'jpg,jpeg,png,gif',fields: 'page_header_image', no_host: true});" class="btn btn-info"><i class="glyphicon glyphicon-plus"></i> Ekle</a>
              </span>
            </div>
        </div>
    </div>
    <div class="col-xl-4">
        <div class="form-group form-group-default form-group-default-select2">
            <label>Konumu</label>
            {!! Form::select("location", [3 => 'DEFAULT', 0 => 'SLİDER', 1 => 'ORTA BÖLÜM', 2 => 'TOPLU'] , null, ['data-placeholder' => 'Konum seçiniz', 'class'=>'full-width','id'=>'location','required'=>'required', 'data-init-plugin' => 'select2']) !!}
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-xl-12">
        <div class="form-group form-group-default required">
            <label>Sayfa Kısa İçerik</label>
            <textarea class="form-control" id="short_text" name="short_text" cols="50" rows="30"  style="height: 80px;" >{!! isset($page) ? $page->short_text : null !!}</textarea>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-xl-12">
        <div class="form-group form-group-default required">
            <label>Sayfa İçeriği</label>
            <textarea rows="2" cols="4" class='form-control elastic editorezy'  id="page_content" name="page_content" >@if(isset($page)){!! $page->content !!}@elseif(!empty(old('content'))){!! old('content')  !!}@endif</textarea>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-xl-6">
        <div class="form-group form-group-default required">
            <label>Meta Açıklama</label>
            <textarea class="form-control" id="metadescription" name="metadescription" cols="50" rows="30"  style="height: 80px;" >{!! isset($page) ? $page->metadescription : null !!}</textarea>
        </div>
    </div>
    <div class="col-xl-6">
        <div class="form-group form-group-default required">
            <label>Kelimeler</label>
            <textarea class="form-control" id="keywords" name="keywords" cols="50" rows="30"  style="height: 80px;" >{!! isset($page) ? $page->keywords : null !!}</textarea>
        </div>
    </div>
</div>
@push('scripts')
    @include('macros.editors')

    <script>
        function slug(text) {
            var trMap = {
                'çÇ':'c',
                'ğĞ':'g',
                'şŞ':'s',
                'üÜ':'u',
                'ıİ':'i',
                'öÖ':'o'
            };
            for(var key in trMap) {
                text = text.replace(new RegExp('['+key+']','g'), trMap[key]);
            }
            return  text.replace(/[^-a-zA-Z0-9\s]+/ig, '') // remove non-alphanumeric chars
                .replace(/\s/gi, "-") // convert spaces to dashes
                .replace(/[-]+/gi, "-") // trim repeated dashes
                .toLowerCase();

        }

        $('#title').keyup(function () {
            var pageSlug = slug($('#title').val());
            $('#slug').val(pageSlug);
        });

    </script>
@endpush
