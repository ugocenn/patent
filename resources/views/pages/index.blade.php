@extends('layouts.app')
@section('title', 'Sayfa Listeleri')
@section('content')
    <div class="content ">
        <br/>
        <!-- START CONTAINER FLUID -->
        <div class=" container-fluid   container-fixed-lg">
            <!-- START card -->
            <div class="card card-default">
                <div class="card-header ">
                    <div class="card-title">Sayfalar
                    </div>
                    <div class="pull-right">
                        <a aria-label="" href="{{ route('web-pages.create') }}" class="btn btn-success btn-icon-left m-b-10" type="button"><i class="pg-icon">plus</i><span class="">Yeni Sayfa Oluştur</span></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                    @include('partials.alerts.error')
                    <div class="table-responsive">
                        <table id="datatable-page-index" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Kategori</th>
                                <th>Sayfa Başlığı</th>
                                <th>Görsel</th>
                                <th>Konum</th>
                                <th>Kısa Yazı</th>
                                <th>Durumu</th>
                                <th width="125px;">Tarih</th>
                                <th>İşlemler</th>
                            </tr>
                            </thead>


                            <tbody>
                            @foreach($pages as $page)
                            <tr>
                                <td>{{ $page->category->name }}</td>
                                <td>{{ $page->title }} <a href="{{url('./icerik/'.$page->category->slug.'/'.$page->slug)}}" target="_blank"><i class="pg-icon" style="font-size: 14px;">external_link</i></a> </td>
                                <td><img src="{!! $page->image !!}" style="height:50px;"></td>
                                <td>{{ $page->short_text }}</td>
                                <td>
                                @if($page->location == 0)
                                    SLİDER
                                    @elseif($page->location == 1)
                                    ORTA BÖLÜM
                                    @elseif($page->location == 2)
                                    TOPLU
                                    @elseif($page->location == 3)
                                    DEFAULT
                                    @endif
                                </td>
                                <td>{{ $page->active }}</td>
                                <td >{{ \Carbon\Carbon::parse($page->created_at)->format('d.m.Y H:i') }}</td>
                                <td>
                                    <a href="{{ route('web-pages.edit', createHashId($page->id)) }}" aria-label=""  class="text-success">
                                        <span>Düzenle</span>
                                    </a>
                                    <a href="javascript:void(0);" onclick="return confirmOpsDelete('{{ createHashId($page->id) }}')" role="menuitem" class="text-danger">
                                        <i class="pg-trash"></i> Sil
                                    </a>
                                    {{ Form::open([
                                        'method' => 'DELETE',
                                        'url' => route("web-pages.destroy", createHashId($page->id)),
                                        'id' => 'ops-delete-form-' . createHashId($page->id)
                                        ]) }}
                                    {{ Form::close() }}


                                </td>
                            </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END card -->
        </div>
        <!-- END CONTAINER FLUID -->
    </div>

@endsection
