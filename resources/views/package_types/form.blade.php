<div class="row clearfix">
    <div class="col-xl-4">
        <div class="form-group form-group-default">
            <label>Paket Türü Adı</label>
            {!! Form::text("name",null,['class'=>'form-control','required'=>'required','id'=>'name']) !!}
        </div>
    </div>
    <div class="col-xl-4">
        <div class="form-group form-group-default">
            <label>Paket Türü Bağlantısı</label>
            {!! Form::text("slug",null,['class'=>'form-control','required'=>'required','id'=>'slug']) !!}
        </div>
    </div>
    <div class="col-xl-4">
        <div class="form-group form-group-default required">
            <label>Durumu</label>
            {!! Form::select("active", ['1' => 'AKTİF', '0' => 'PASİF'], null, ['class'=>'form-control','id'=>'active']) !!}
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-xl-9">
        <div class="form-group form-group-default required">
            <label>Paket Türü Açıklaması</label>
            <textarea rows="2" cols="2" class='form-control  elastic editorezy'  id="description" name="description" >@if(isset($package_type)){!! $package_type->description !!}@elseif(!empty(old('description'))){!! old('description')  !!}@endif</textarea>
        </div>
    </div>
    <div class="col-xl-3">
        <div class="form-group form-group-default input-group">
            <div class="form-input-group">
                <label>Paket İkonu</label>
                {!!  Form::text('icon', null, ['required','class'=>'form-control','id'=>'icon'])!!}
            </div>
            <div class="input-group-append ">
              <span class="input-group-text"><a href="javascript:;" onclick="moxman.browse({extensions:'jpg,jpeg,png,gif,svg',fields: 'icon', no_host: true});" class="btn btn-info"><i class="glyphicon glyphicon-plus"></i> Ekle</a>
              </span>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        function slug(text) {
            var trMap = {
                'çÇ':'c',
                'ğĞ':'g',
                'şŞ':'s',
                'üÜ':'u',
                'ıİ':'i',
                'öÖ':'o'
            };
            for(var key in trMap) {
                text = text.replace(new RegExp('['+key+']','g'), trMap[key]);
            }
            return  text.replace(/[^-a-zA-Z0-9\s]+/ig, '') // remove non-alphanumeric chars
                .replace(/\s/gi, "-") // convert spaces to dashes
                .replace(/[-]+/gi, "-") // trim repeated dashes
                .toLowerCase();

        }

        $('#name').keyup(function () {
            var pageSlug = slug($('#name').val());
            $('#slug').val(pageSlug);
        });

    </script>
    @include('macros.editors')
@endpush



