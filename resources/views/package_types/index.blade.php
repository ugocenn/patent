@extends('layouts.app')
@section('title', 'Paket Türü Listeleri')
@section('content')
    <div class="content ">
        <br/>
        <!-- START CONTAINER FLUID -->
        <div class=" container-fluid   container-fixed-lg">
            <!-- START card -->
            <div class="card card-default">
                <div class="card-header ">
                    <div class="card-title">Sektör Listeleri
                    </div>
                    <div class="pull-right">
                        <a aria-label="" href="{{ route('package-types.create') }}" class="btn btn-success btn-icon-left m-b-10" type="button"><i class="pg-icon">plus</i><span class="">Yeni Paket Türü Oluştur</span></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                    @include('partials.alerts.error')
                    <div class="table-responsive">
                        <table id="datatable" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Paket Türü</th>
                                <th>Bağlantı</th>
                                <th>İşlemler</th>
                            </tr>
                            </thead>


                            <tbody>
                            @foreach($package_types as $package_type)
                            <tr>
                                <td><img height="32px;" height="32px;" src="{!! $package_type->icon !!}"></td>
                                <td>{{ $package_type->name }}</td>
                                <td>{{ $package_type->slug }}</td>


                                <td>
                                    <a href="{{ route('package-types.edit', createHashId($package_type->id)) }}" aria-label=""  class="text-success">
                                        <span>Düzenle</span>
                                    </a>
                                    <a href="javascript:void(0);" onclick="return confirmOpsDelete('{{ createHashId($package_type->id) }}')" role="menuitem" class="text-danger">
                                        <i class="pg-trash"></i> Sil
                                    </a>
                                    {{ Form::open([
                                        'method' => 'DELETE',
                                        'url' => route("package-types.destroy", createHashId($package_type->id)),
                                        'id' => 'ops-delete-form-' . createHashId($package_type->id)
                                        ]) }}
                                    {{ Form::close() }}


                                </td>
                            </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END card -->
        </div>
        <!-- END CONTAINER FLUID -->
    </div>

@endsection
