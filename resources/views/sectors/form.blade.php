<div class="row clearfix">
    <div class="col-xl-12">
        <div class="form-group form-group-default required">
            <label>Sektör Adı</label>
            {!! Form::text("name",null,['class'=>'form-control','required'=>'required','id'=>'name']) !!}
        </div>
        <div class="form-group form-group-default required">
            <label>Sınıf Numaraları</label>
            {!! Form::text("sinif_degeri",null,['class'=>'form-control','required'=>'required','id'=>'sinif_degeri', 'placeholder'=>'1-2-3-4-5-']) !!}

        </div>
        <em>Sınıf numaralarını arada ve en sonda tire - işareti olacak şekilde giriniz. Tek sınıf girilecekse en sona tire eklemeyiniz.</em>
        <div class="form-group form-group-default required">
            <label>Sıralama</label>
            {!! Form::number("sort",null,['class'=>'form-control','required'=>'required','id'=>'sort']) !!}

        </div>
    </div>

</div>


