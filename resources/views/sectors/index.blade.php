@extends('layouts.app')
@section('title', 'Sektör Listeleri')
@section('content')
    <div class="content ">
        <br/>
        <!-- START CONTAINER FLUID -->
        <div class=" container-fluid   container-fixed-lg">
            <!-- START card -->
            <div class="card card-default">
                <div class="card-header ">
                    <div class="card-title">Sektör Listeleri
                    </div>
                    <div class="pull-right">
                        <a aria-label="" href="{{ route('sectors.create') }}" class="btn btn-success btn-icon-left m-b-10" type="button"><i class="pg-icon">plus</i><span class="">Yeni Sektör Oluştur</span></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                    @include('partials.alerts.error')
                    <div class="table-responsive">
                        <table id="datatable" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Sektör</th>
                                <th>Sınıfları</th>
                                <th>Sıralama</th>
                                <th>İşlemler</th>
                            </tr>
                            </thead>


                            <tbody>
                            @foreach($sectors as $sector)
                            <tr>
                                <td>{{ $sector->name }}</td>
                                <td>{{ $sector->sinif_degeri }}</td>
                                <td>{{ $sector->sort }}</td>
                                <td>
                                    <a href="{{ route('sectors.edit', createHashId($sector->id)) }}" aria-label=""  class="text-success">
                                        <span>Düzenle</span>
                                    </a>
                                    <a href="javascript:void(0);" onclick="return confirmOpsDelete('{{ createHashId($sector->id) }}')" role="menuitem" class="text-danger">
                                        <i class="pg-trash"></i> Sil
                                    </a>
                                    {{ Form::open([
                                        'method' => 'DELETE',
                                        'url' => route("sectors.destroy", createHashId($sector->id)),
                                        'id' => 'ops-delete-form-' . createHashId($sector->id)
                                        ]) }}
                                    {{ Form::close() }}


                                </td>

                            </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END card -->
        </div>
        <!-- END CONTAINER FLUID -->
    </div>

@endsection
