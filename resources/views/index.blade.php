@extends('frontend.app')
@section('title',$option->site_name)
@section('description',$option->site_desc)
@section('keywords',$option->site_keywords)

@section('content')

    <div class="bg-img-hero" style="background-image: url({{asset('images/bg.jpg')}});">
        <!-- Main Content -->
        <div class="d-lg-flex align-items-md-center form-container">
            <div class="container position-relative space-2 space-0--lg mt-lg-8">
                <div class="row justify-content-md-between ">
                    <div class="order-2 order-md-1 col-md-5">
                        <form id="markaFormOne">
                        <div class="marka-form mt-3 mt-lg-0">
                            <div class="form-group">
                                <input type="text" class="form-control" name="marka" id="txtMarkWord"
                                       placeholder="MARKA ADI"/>
                            </div>
                            <div class="form-group">
                                <select class="" name="sektor" id="sektor">
                                    @foreach($sectors as $sector)
                                        <option value="{{$sector->sinif_degeri}}">{{$sector->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="isim" id="isim" placeholder="İSİM"/>
                            </div>
                            <div class="form-group">
                                <input type="number" class="form-control" name="telefon" id="telefon"
                                       placeholder="TELEFON"/>
                            </div>
                            <div class="form-group form-check">
                                <input type="checkbox" class="form-check-input" id="iletisimIzni" name="iletisimIzni" checked>
                                <label class="form-check-label" for="iletisimIzni" >
                                    Sorgulanan Marka için <a href="https://patentsorgu.com/kvkk-ve-gizlilik-politikasi" target="_blank"><span
                                            class="boldUndeline">İletişim İzni</span></a> vermeyi kabul ediyorum.
                                </label>
                            </div>
                            <button type="submit"
                                    class="btn btn-block btn-primary"
                                    id="btnSearch"


                            >HEMEN SORGULA</button>
                        </div>
                        </form>
                    </div>

                    <div class="order-1 order-md-2 col-md-6 mb-7 mb-lg-0">
                        <h1 class="hero-h1 text-white text-md-right mb-0 mt-4">ÜCRETSİZ <br class="d-block d-md-none"/>MARKA
                            <span class="d-none d-md-block">ve PATENT</span> SORGULAMA</h1>
                        <span
                            class="hero-span d-block text-white  text-md-right">Online Marka<br
                                class="d-none d-md-block">Başvurusu</span>
                    </div>
                </div>

            </div>
        </div>
    </div>

    @foreach(getRehbers(0, 'hizmetler', 1) as $rehber)
        <?php
        $image = str_replace('/public', '', $rehber->image);
        $image = str_replace('/depo/dosyalar/', '', $image);

        ?>
        <section class="tek-rehber d-none d-lg-block">
            <div class="container position-relative">
                <a href="{{ route('show-page', [$rehber->slug]) }}">
                <div class="card content-card ml-auto"
                     style="background-image: url({{ url('depo/dosyalar/188x255/'.$image) }});">

                    <span>{{ $rehber->category->name }}</span>
                   <h3 class="mt-3 mb-4">{{ $rehber->title }}</h3>
                    <p class="mb-0">{!! $rehber->short_text !!}</p>
                    <i
                            class="bi bi-arrow-right"></i>

                </div>
            </a>
            </div>
        </section>
    @endforeach



    <section class="container marka-adim-section">
        <div class="row position-relative pb-100">
            <hr class="w-100 position-absolute info-hr d-none d-lg-block" style="top: 47px;">
            <div class="col-12 col-md-6 col-lg-4 mb-5 mb-lg-0">
                <div class="info ml-lg-auto">
                    <div class="icon text-center mb-4 mx-auto justify-content-center">
                        <div class="lines"><img src="{{asset('images/marka-sorgula.svg')}}" style="height:68px"/></div>

                    </div>
                    <div class="info-content m-lg-auto">
                        <span class="d-block mb-2">ADIM 1</span>
                        <h4 class="info-title mb-3">Markanızı Hemen Sorgulayın!</h4>
                        <p>Markanı ücretsiz ve hızlı bir şekilde sorgula!</p>
                    </div>

                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 mb-5 mb-lg-0">
                <div class="info m-lg-auto">
                    <div class="icon text-center mb-4 mx-auto justify-content-center">
                        <div class="lines"><img src="{{asset('images/basvuru-yap.svg')}}" style="height:68px"/></div>
                    </div>
                    <div class="info-content m-lg-auto">
                        <span class="d-block mb-2">ADIM 2</span>
                        <h4 class="info-title mb-3">Hemen Başvuru Yapın!</h4>
                        <p>20 Dakika İçerisinde Talep Ettiğiniz Marka ile ilgili Arayalım!</p>
                    </div>

                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 mb-5 mb-lg-0">
                <div class="info mr-lg-auto">
                    <div class="icon text-center mb-4 mx-auto justify-content-center">
                        <div class="lines"><img src="{{asset('images/basvurunuzu-gerceklestirelim.svg')}}"
                                                style="height:68px"/></div>
                    </div>
                    <div class="info-content m-lg-auto">
                        <span class="d-block mb-2">ADIM 3</span>
                        <h4 class="info-title mb-3">Başvurunuzu Gerçekleştirelim</h4>
                        <p>Güvenli Ödemeni Yap, 20 Dakika İçerisinde Başvurun Gerçekleşsin</p>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section class="container-fluid bg-light services">
        <div class="@if((new \Jenssegers\Agent\Agent())->isDesktop()) container @endif">
            @foreach(getRehbers(2, 'hizmetler', 1) as $rehber)
                <div class="row">
                    <div class="col-12">
                        <div class="ml-100 pb-230">
                            <span class="d-block subtitle">{{ $rehber->category->name }}</span>
                            <h2>{{ $rehber->category->title }}</h2>
                            <span class="d-block lead mb-4">{!! $rehber->category->description !!}</span>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </section>

    <section class="container position-relative pt-3 pt-lg-0 pb-5 mt-lg-n10">
        <div class="row">
            @foreach(getRehbers(2, 'hizmetler', 3) as $rehber)
                <?php
                $image = str_replace('/public', '', $rehber->image);
                $image = str_replace('/depo/dosyalar/', '', $image);

                ?>
                <div class="col-12 col-md-6 col-lg-4">
                    <a href="{{ route('show-page', [$rehber->slug]) }}">
                    <div class="card content-card mx-auto ml-md-auto" style="background-image: url({{ url('depo/dosyalar/188x255/'.$image) }});">
                        <span>{{ $rehber->category->name }}</span>
                        <h3 class="mt-3 mb-4">{{ $rehber->title }}</h3>
                        <p class="mb-0">{!! $rehber->short_text !!}</p>
                        <i
                                class="bi bi-arrow-right"></i>
                    </div></a>
                </div>
            @endforeach
        </div>
    </section>


    <section class="container pt-100 pt-70">
        <div class="row">

                <div class="col-12 text-lg-center">
                    <div class="mb-120">
                        <span class="d-block subtitle">DİĞER SERVİSLER</span>
                        <h2>Tüm Servisler</h2>
                        <span class="d-block lead mb-4 mx-auto">Her türlü, tasarım tescil, yenileme, adres değişikliği, ünvan değişikliği gibi işlemlerde yanınızdayız.</span>
                    </div>
                </div>

        </div>
        <div class="row">
            @foreach(getPackageTypes() as $package_type)
                <div class="col-12 col-lg-6">

                    <div class="service-box d-flex online-basvuru-diger" data-url="{{ $package_type->slug }}">
                        <div class="service-icon align-self-center text-left order-2 order-md-1"><img  src="{!! $package_type->icon !!}"></div>
                        <div class="service-body order-1 order-md-2">
                            <span class="d-block subtitle">{{ $package_type->name }}</span>
                            <h3>{{ $package_type->name }}</h3>
                            <span class="d-block bottom-lead mb-4">{!! $package_type->description !!}</span>
                        </div>
                        <div class="service-button align-self-center d-none d-md-block order-3">
                            <a data-url="{{ $package_type->slug }}" href="javascript:" class="btn btn-third my-auto online-basvuru-diger">BAŞVUR</a>
                        </div>
                    </div>

                </div>
            @endforeach
        </div>
    </section>

    <section class="container-fluid bg-light services">
        <div class="@if((new \Jenssegers\Agent\Agent())->isDesktop()) container @endif">
            @foreach(getRehbers(2, 'rehberler', 1) as $rehber)
                <div class="row">
                    <div class="col-12">
                        <div class="ml-100 pb-230">
                            <span class="d-block subtitle">{{ $rehber->category->name }}</span>
                            <h2>{{ $rehber->category->title }}</h2>
                            <span class="d-block lead mb-4">{!! $rehber->category->description !!}</span>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </section>

    <section class="container position-relative pt-3 pt-lg-0 pb-5 mt-lg-n10">
        <div class="row">
            @foreach(getRehbers(2, 'rehberler', 0) as $rehber)
                <div class="col-12 col-md-6 col-lg-4">
                    <?php
                        $image = str_replace('/public', '', $rehber->image);
                        $image = str_replace('/depo/dosyalar/', '', $image);

                    ?>
                    <a href="{{ route('show-page', [$rehber->slug]) }}">
                    <div class="card content-card mx-auto ml-md-auto" style="background-image: url({{ url('depo/dosyalar/188x255/'.$image) }});">
                        <span>REHBERLER</span>
                        <h3 class="mt-3 mb-4">{{ $rehber->title }}</h3>
                        <p class="mb-0">{!! $rehber->short_text !!}</p>
                        <i
                                class="bi bi-arrow-right"></i>
                    </div></a>
                </div>
            @endforeach

            <div class="col-12 col-md-6 col-lg-4">
                <div class="card content-card mx-auto ml-md-auto gradient-purple"
                     style="background-image: url({{asset('images/tum-rehberler-bg.svg')}});">
                    <span>REHBERLER</span>
                    <a href="{{route('rehberler')}}"><h3 class="mt-3 mb-4">Tüm Rehberler!</h3></a>
                    <p class="mb-0">&nbsp;</p>
                    <a href="{{route('rehberler')}}" class="text-right"><i class="bi bi-arrow-right"></i></a>
                </div>
            </div>

        </div>
    </section>
    <div class="modal"></div>
    <div id="lottie" ></div>
    <div id="overlay"></div>

@endsection


@section('css')
    <link href="{{asset('js/select2.min.css')}}" rel="stylesheet"/>
    <style>
        .grecaptcha-badge{
            opacity: 0;
            z-index: -9999;
            left:0
        }
        </style>
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bodymovin/5.7.6/lottie.min.js"></script>
    <script>

    </script>
    <script src="{{asset('js/select2.min.js')}}"></script>
    <script src="{{asset('js/sweetalert2.min.js')}}"></script>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"></script>
    <script>
        var captchaWidgetId;
        var onloadCallback = function() {
            captchaWidgetId = grecaptcha.render('btnSearch', {
                'sitekey' : '6LfNsoAaAAAAAA2Xg9amYHUXbMPJEnmQlMvUC_nx',
                'callback' : onSubmit
            });
        };
        var onSubmit = function(token) {
            //console.log ('token'+token)
            if (ValidateSearchParameters() === true) {
                  document.getElementById("overlay").style.display = "block";
                  document.getElementById("lottie").style.display = "block";
                  var animationLottie = bodymovin.loadAnimation({
                      container: document.getElementById('lottie'), // Required
                      path: '{{asset('assets/animation.json')}}', // Required
                   renderer: 'svg/canvas/html', // Required
                   loop: true, // Optional
                   autoplay: false, // Optional

               })
              animationLottie.play()


                Search(token)
            }
            grecaptcha.reset(captchaWidgetId)
        };
       $('#btnSearch').on('click', function(e) {
            e.preventDefault();




        });



        // $("form").submit(function(e){
        //     e.preventDefault();
        // });
        $(document).ready(function () {
            @if((new \Jenssegers\Agent\Agent())->isDesktop())

                $('#sektor').select2();

             @endif

            if (localStorage.getItem('markWord')){$('#txtMarkWord').val(JSON.parse(localStorage.getItem('markWord')))}
            if (localStorage.getItem('isim')){$('#isim').val(JSON.parse(localStorage.getItem('isim')))}
            if (localStorage.getItem('telefon')){$('#telefon').val(JSON.parse(localStorage.getItem('telefon')))}
            if (localStorage.getItem('iletisimIzni')){
                if(JSON.parse(localStorage.getItem('iletisimIzni')==1)){
                    $('#iletisimIzni').prop('checked', true);
                }else{
                    $('#iletisimIzni').prop('checked', false);
                }
            }

        });

        var captchaWidgetId;
        var recaptchaCalls = 0;



        function GetCheckedSectors() {
            var checkedSectors = $("#sektor").val();
            return checkedSectors;
        }

        function GetIsimKontrol() {
            var checkedIsim = $("#isim").val();
            return checkedIsim;
        }

        function GetTelefonKontrol() {
            var checkedTelefon = $("#telefon").val();
            return checkedTelefon;
        }
        function ValidateSearchParameters() {
            if ($("#txtMarkWord").val().includes("<") || $("#txtMarkWord").val().includes(">")) {
                swal({
                    title: '',
                    text: "Araştırılacak marka '<' veya '>' karakterlerini içermemelidir.",
                    type: 'warning',
                    confirmButtonText: 'Tamam'
                });
                return false;
            }

            var searchWord = $("#txtMarkWord").val();

            if(searchWord.length == 3){
                searchWord = searchWord + ' ';
            }
            if (searchWord.length <= 3 || searchWord.length > 50) {
                swal({
                    title: '',
                    text: "Araştırılacak marka en az 3 en çok 50 karakter uzunluğunda olmalıdır.",
                    type: 'warning',
                    confirmButtonText: 'Tamam'
                });
                return false;
            }

            if (GetCheckedSectors().length === 0) {
                swal({
                    title: '',
                    text: "Araştırılacak sektör veya sınıfları belirtmediniz.",
                    type: 'warning',
                    confirmButtonText: 'Tamam'
                });
                return false;
            }

            if (GetIsimKontrol().length === 0) {
                swal({
                    title: '',
                    text: "Lütfen isim alanını boş bırakmayınız.",
                    type: 'warning',
                    confirmButtonText: 'Tamam'
                });
                return false;
            }

            if (GetTelefonKontrol().length === 0) {
                swal({
                    title: '',
                    text: "Lütfen telefon numaranızı yazınız.",
                    type: 'warning',
                    confirmButtonText: 'Tamam'
                });
                return false;
            }

            return true;
        }

        function Search(token) {

            var searchWord = $("#txtMarkWord").val();

            if(searchWord.length == 3){
                searchWord = searchWord + ' ';
            }
            var markWord = searchWord;
            var isim = GetIsimKontrol();
            var telefon = GetTelefonKontrol();
            var sectors = GetCheckedSectors();
            var sector_name = $("#sektor option:selected").text();
            var checkbox = $("#iletisimIzni");
            if($(checkbox).is(":checked")){
                iletisimIzni = 1;
            }
            else if($(checkbox).is(":not(:checked)")){
                iletisimIzni = 0;
            }
            var grecaptchaResponse = token;

            $.ajax({

                url: "https://www.markanabak.com.tr/MarkanaBak/api/v1/TrademarkSearch.ashx",
                type: "POST",
                xhrFields: {
                    withCredentials: true
                },
                data: {
                    markWord: markWord,
                    draw: 1,
                    start: 0,
                    length: 30,
                    sectors: sectors,
                    grecaptchaResponse: grecaptchaResponse,
                    exactSearch:true
                },


                beforeSend: function () {

                },
                complete: function () {

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    swal({
                        title: '',
                        text: jqXHR.responseText,
                        type: 'warning',
                        confirmButtonText: 'Tamam'
                    });
                },
                success: function( data )
                {

                    veri = JSON.parse(data);
                    localStorage.setItem('markWord', JSON.stringify(markWord));
                    localStorage.setItem('isim', JSON.stringify(isim));
                    localStorage.setItem('telefon', JSON.stringify(telefon));
                    localStorage.setItem('sectors', JSON.stringify(sectors));
                    localStorage.setItem('sector_name', JSON.stringify(sector_name));
                    localStorage.setItem('iletisimIzni', JSON.stringify(iletisimIzni));

                    localStorage.setItem('sonuc_data', JSON.stringify(veri.data));
                    localStorage.setItem('sonuc_draw', JSON.stringify(veri.draw));
                    localStorage.setItem('sonuc_recordsFiltered', JSON.stringify(veri.recordsFiltered));
                    localStorage.setItem('sonuc_recordsTotal', JSON.stringify(veri.recordsTotal));
//
                    @php
                    $url = str_replace('http://', 'https://', route('ajax-application-store'));
                    @endphp

                    var visitor = localStorage.getItem("visitor") ? JSON.parse(localStorage.getItem("visitor")) : null;
                    $.getJSON("{{ $url }}?type=1&mark="+ JSON.stringify(markWord) + "&name=" + JSON.stringify(isim) + "&phone=" + JSON.stringify(telefon) + "&sector=" + JSON.stringify(sector_name)+ "&sonuc_sayisi=" + JSON.parse(localStorage.getItem('sonuc_recordsTotal'))+ "&iletisimizni=" + JSON.stringify(iletisimIzni) + "&visitor=" + visitor, function(result){
                        localStorage.setItem('visitor', JSON.stringify(result.visitor));
                        localStorage.setItem('search_id', JSON.stringify(result.search_id));

                       // document.getElementById('btnSearch').removeAttribute("disabled");
                        document.getElementById("overlay").style.display = "none";
                        document.getElementById("lottie").style.display = "none";

                        window.location.href = "./marka-sonuc/"+iletisimIzni+"/"+markWord+"/"+JSON.parse(localStorage.getItem('visitor'))+"/"+JSON.parse(localStorage.getItem('search_id'))+"";
                    });

                    //




                },

            });
        }

        $(".online-basvuru-diger").click(function(){
            var visitor = localStorage.getItem("visitor") ? JSON.parse(localStorage.getItem("visitor")) : null;
            var url = $(this).data('url');
            var param1 = '';
            if(visitor != null){
                param1 = '/'+visitor;
            }
            $(location).attr('href','/online/'+url+param1);

        });

        function onSubmit(token) {
            //document.getElementById("markaFormOne").submit();
        }




    </script>
@endsection
