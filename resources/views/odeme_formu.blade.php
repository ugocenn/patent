@extends('frontend.app')
@section('title','Ödeme Ekranı | ' .config('app.name'))

@section('content')
    <div class="bg-img-hero-content basvuru-hero" style="background-image: url({{asset('images/bg.jpg')}});">
        <!-- Main Content -->
        <div class="d-lg-flex align-items-lg-center title-container">
            <div class="container position-relative space-2 space-0--lg mt-lg-8">
                <div class="row justify-content-lg-between ">
                    <h1 class="hero-h1 text-white text-center mb-0 w-100 ">ÖDEME <br class="d-md-none"> EKRANI</h1>
                    <span class="hero-lead d-block text-center text-white w-100 mt-4">

                    </span>
                </div>

            </div>
        </div>
    </div>
    {!! Form::open(['url' => 'deneme','id'=>'admin-form','role'=>'form','autocomplete'=>'off', 'novalidate' => 'novalidate', 'files' => true])!!}
    <section class="container py-5 basvuruMainContainer">
            <div class="row">
                <div class="col-12 col-md-8">

                    {!! $paymentinput !!}
                    <div id="iyzipay-checkout-form" class="responsive"></div>

                    <div class="basvuru-title mb-2 mt-5">Havale / EFT ile Ödeme</div>
                    <div class="d-flex align-items-center flex-lg-row flex-column mt-3 mb-5">
                        <div class="lead col-7">Havale ile ödeme için bilgilerimiz.</div>

                        <span class="position-relative border-radius-20 px-3 py-2 col-5" style=" background: rgba(0, 0, 0, 0.04);font-size: 15px;">
                                <i class="bi bi-info-circle-fill position-absolute" style="font-size: 18px;left: -7px;top: 14px;"></i> Lütfen Açıklama kısmına Marka İsminizi yazınız.
                            </span>
                    </div>
                    <div class="service-box d-flex" >
                        <div class="service-body order-1 order-md-2 w-100">

                            <h3 id="hesap_no">TR29 0020 5000 0970 4604 7000 01</h3>
                            <strong>Hesap Sahibi</strong>
                            <span class="d-block bottom-lead mb-4"><p>Deha Patent ve Danışmanlık Hizmetleri Limited Şirketi</p></span>
                        </div>
                        <div class="service-button align-self-center d-none d-md-block order-3">
                            <a href="javascript:" id="iban_kopyala" class="btn btn-third my-auto " style="width: 100px; line-height: 24px;"
                               data-clipboard="true" data-clipboard-target="#hesap_no"
                            >IBAN KOPYALA</a>
                        </div>
                    </div>

                </div>
                <div class="col-12 col-md-4">
                    <div class="basvuru-ozeti mt-n8 make-me-sticky d-none d-md-block">
                        <div class="card">
                            <div class="card-header">
                                <h4>Başvuru Özetiniz</h4>
                                <div class="ozet-paket-name d-flex justify-content-between flex-row">
                                    @foreach($packages as $package)
                                        <?php
                                        $arr = explode(' ',trim($package->name));
                                        ?>
                                        <div class="pkt{{ $package->id }} {!! $basket->packet_id == $package->id ? 'active' : '' !!}" data-id="{{ $package->id }}">
                                            @if($arr)
                                                {{$arr[0]}}
                                            @else
                                                {{$package->name}}
                                            @endif
                                        </div>
                                    @endforeach
                                    <div class="pkt0 {!! $basket->packet_id == 0 ? 'active' : '' !!}">Kapsamlı</div>
                                </div>
                            </div>
                            <div class="card-body d-flex flex-column">
                                <div class="d-flex flex-column">
                                    <span class="description">MARKA</span>
                                    <span class="service-name" id="ozet-marka">{{ $marka }}</span>
                                </div>

                                <div class="d-flex flex-column mt-4">
                                    <span class="description">SEKTÖR VEYA SINIF</span>
                                    <span class="service-name sector-class-name">
                                        @if($basket->packet_id == 0)
                                            <?php
                                            $siniflar = json_decode($basket->secili_siniflar,true);
                                            ?>
                                            @if(isset($siniflar))
                                                @foreach($siniflar as $value)
                                                    {{str_replace('.Sınıf', '', $value)}}
                                                    @if(!$loop->last) - @endif
                                                @endforeach
                                            @endif
                                        @else
                                            {{$basket->getSector($basket->secili_sektorler)->name}}
                                        @endif
                                    </span>
                                </div>

                                <?php
                                $hizmetler = json_decode($basket->ilave_hizmetler,true);

                                ?>
                                @if(is_array($hizmetler) && count($hizmetler) > 0)
                                    <div class="add-services-div @if(is_array($hizmetler) && count($hizmetler) == 0) d-none @endif">
                                        <div class="d-flex flex-column mt-4 mb-3">
                                            <span class="description">İLAVE HİZMETLER</span>
                                            @foreach($hizmetler as $key => $hizmet)
                                                <div class="d-flex flex-column" id="ilave_hizmetler">
                                                    {!! $hizmet['name'] !!}
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                @endif
                                <div class="pck-name">
                                    <div class="d-flex flex-row justify-content-between mt-5 mb-3">
                                        <span class="description">@if($basket->packet_id == 0)KAPSAMLI @else {{$basket->getPacket($basket->packet_id)->name_mini}} @endif</span>
                                        <span class="price flex-shrink-0">
                                            @if($basket->packet_id == 0)
                                                {{ number_format($basket->getBazPrice($basket->package_type_id)->price ,0,',','.') }}
                                            @else
                                                {{ number_format($basket->getPacket($basket->packet_id)->price ,0,',','.') }}
                                            @endif ₺</span>
                                    </div>
                                </div>
                                <div class="ilave-siniflar">
                                    <div class="d-flex flex-row justify-content-between mb-3">
                                        <span class="description">İLAVE SINIFLAR</span>
                                        <span class="price flex-shrink-0">
                                            {{ number_format($ucret ,0,',','.') }} ₺</span>
                                    </div>
                                </div>

                                <div class="ilave-hizmetler">
                                    <div class="d-flex flex-row justify-content-between mb-3">
                                        <span class="description">İLAVE HİZMETLER</span>
                                        <span class="price flex-shrink-0">
                                            {{ number_format($ilave_hizmet_ucreti ,0,',','.') }} ₺</span>
                                    </div>
                                </div>


                                <div class="d-flex flex-row justify-content-between mb-3">
                                    <span class="description">KDV (%18)</span>
                                    <span class="price flex-shrink-0 kdv-div" >{{ number_format($ana_kdv ,0,',','.') }} ₺</span>
                                </div>
                                <div class="d-flex flex-row justify-content-between">
                                    <span class="description">TOPLAM ÖDENECEK</span>
                                    <span class="total-price ml-auto flex-shrink-0 toplam-odenecek-div">{{ number_format($toplam ,0,',','.') }} ₺</span>
                                </div>
                            </div>
                        </div>

                        <div class="d-block d-flex white-shadow-box bg-white mt-n4">
                            <div class="d-flex flex-row">
                                <img class="mr-2" src="{{asset('images/lock-small.svg')}}" style="height: 19px;" />
                                <div class="d-flex flex-column">
                                    <span class="iade">İADE GARANTİSİ!</span>
                                    <span class="iade-info">Red Durumunda Ücretsiz 2. Başvuru</span>
                                </div>
                            </div>
                        </div>



                    </div>


                </div>
            </div>
    </section>
    {!! Form::close() !!}
@endsection

@section('css')
    <style>
    #fatura-bilgileri label{
        display:inherit;
        margin-bottom: 0;
    }
    .error .inputBasvuru {

        border:1px solid red;

    }
    </style>

@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.8/clipboard.min.js"></script>
    <script src="{{asset("assets/js/notify.min.js")}}"></script>
    <script>
        var pageScript = function() {
            var copyText = function () {
                new ClipboardJS('[data-clipboard=true]').on('success', function(e) {
                    e.clearSelection();
                    $("#iban_kopyala").notify(
                        "Kopyalandı",
                        { position:"top",
                            className:"success",
                            style: 'bootstrap',
                        }
                    );

                });
            }
            return {
                init: function() {
                    copyText();
                }
            };
        }();

        pageScript.init();
    </script>
@endsection
