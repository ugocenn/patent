@extends('frontend.app')
@section('title', 'Başvuru Adımı | '.config('app.name'))

@section('content')
    <?php
    $sinif_sayisi = array(
        0 => 'Tek',
        1 => 'Tek',
        2 => 'İki',
        3 => 'Üç',
        4 => 'Dört',
        5 => 'Beş',
        6 => 'Altı',
        7 => 'Yedi',
        8 => 'Sekiz',
        9 => 'Dokuz',
        10 => 'On'
    )

    ?>

    <div class="bg-img-hero-content basvuru-hero" style="background-image: url({{asset('images/bg.jpg')}});">
        <!-- Main Content -->
        <div class="d-lg-flex align-items-lg-center title-container">
            <div class="container position-relative space-2 space-0--lg mt-lg-8">
                <div class="row justify-content-lg-between ">
                    <h1 class="hero-h1 text-white text-center mb-0 w-100 ">BAŞVURU <br class="d-md-none"> ADIMI</h1>
                    <span class="hero-lead d-block text-center text-white w-100 mt-4">

                    </span>
                </div>

            </div>
        </div>
    </div>
    {!! Form::open(['url' => '/checkout/'.$type,'id'=>'admin-form','role'=>'form','autocomplete'=>'off', 'novalidate' => 'novalidate', 'files' => true])!!}
    <section class="container py-5 basvuruMainContainer">
            <div class="row">
                <div class="col-12 col-md-8">
                    <div id="opResult"></div>
                    <div id="basvuru-paketi" class="mt-n10 mt-md-auto">
                        <div class="basvuru-title mb-2 d-none d-md-block">Başvuru Paketinizi Seçin</div>
                        <div class="lead mt-3 mb-5 d-none d-md-block">Avantajları paketlerimizden birisini seçin, ya da Size Özel ile kendiniz oluşturun.</div>
                        <div class="row no-wrap overflow-auto" style="min-height: 356px;">
                            @php $i = 1; $a = ""; @endphp
                            @foreach($packages as $package)
                                @if($basket->packet_id == $package->id)
                                    @php $p_first = $package; $suggested_package = $package;  @endphp
                                @endif
                            <div class="col-md-4">
                                <div class="paket-dis pkt{{ $package->id }} {!! $basket->packet_id == $package->id ? 'active' : '' !!}" data-gorseladeti="{{ $package->gorsel_adeti }}" data-packageid="{{ $package->id }}" data-classlist="{{ $package->class_count > 0 ? $package->class_count : '0' }}" data-servicelist="{{ strlen($package->additional_services) > 0 ? $package->additional_services : '0' }}" data-servicelistprice="{{ strlen($package->additional_services) > 0 ? $package->getServices('price') : '0' }}" data-pckprice="{{ $package->price }}" data-pckname="{{ $package->name_mini }}">
                                    <div class="card paket">
                                        <div class="card-header">
                                            <div class="paket-icon text-center">{!! $package->is_star == 1 ? '<i class="bi bi-star-fill"></i>' : '' !!}</div>
                                            <div class="baslik-ust">{{$package->name_top}}</div>
                                            <h4 class="mt-2">{!! $package->name !!}</h4>
                                        </div>
                                        <div class="card-body position-relative">
                                            <?php
                                            // Sınıf sayısını yazdırmak için
                                            $sayi = $package->class_count;

                                            // Ek hizmetleri kartlara yazdırmak için
                                            if(isset($package->additional_services)){
                                                $ek_hizmetler = json_decode($package->additional_services,true);
                                            }else{
                                                $ek_hizmetler = null;
                                            }


                                            ?>
                                            <div class="ortaYazi">
                                                <div><span class="font-weight-bold">{{$sinif_sayisi[$sayi]}} Sınıflı</span> Başvuru</div>
                                                @if(isset($package->inactive_services))
                                                <div><del><em>{{$package->inactive_services}}</em></del></div>
                                                @endif

                                                @if(isset($ek_hizmetler))
                                                @foreach($ek_hizmetler as $hizmet)
                                                        <div>{{$hizmet['short_name']}}</div>
                                                    @endforeach
                                                @endif
                                            </div>

                                            <div class="w-100 paket-icon mt-4" style="height: 62px;">
                                                <img src="{!! $package->icon !!}" style="height: 62px;width: 62px;" />
                                            </div>
                                                @if($package->old_price)
                                                <div class="old_price fsize16 position-absolute absolute-center font-weight-bold bottom-0px">
                                                    <span class="strike">{!! str_replace('.00', '', $package->old_price) !!} ₺ + KDV</span> </div>
                                                    @endif
                                        </div>
                                        <div class="card-footer">
                                            <span class="d-block w-100 text-center" style="font-size: 24px;font-weight:700">{!! str_replace('.00', '', $package->price) !!} <span style="font-size: 18px">₺</span>
                                            <br><span style="font-size: 13px">+ KDV</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                @php $i++; @endphp
                            @endforeach
                            <div class="col-md-4">
                                <div class="paket-dis pkt0 {!! $basket->packet_id == 0 ? 'active' : '' !!}" data-packageid="0">
                                    <div class="card paket">
                                        <div class="card-header">
                                            <div class="paket-icon text-center"></div>
                                            <div class="baslik-ust"></div>
                                            <h4 class="mt-2">Kapsamlı</h4>
                                        </div>
                                        <div class="card-body">
                                            <div class="ortaYazi">
                                                İstediğiniz Kadar Sınıf Ekleyin
                                            </div>

                                            <div class="w-100 paket-icon mt-4" style="height: 60px;">
                                                <img src="{{asset('images/Shield-kapsamli.svg')}}" />
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <span class="d-block w-100 text-center" style="font-size: 24px;font-weight:700"> <span style="font-size: 15px">Fiyat Seçiminize Göre Belirlenir</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div id="markaniz" style="margin-top: 84px;">
                        <div class="basvuru-title mb-2">Markanız</div>
                        <div class="lead mt-3 mb-5">Markanız sorguladığınız biçimde iyi görünüyor. İsterseniz düzenleyebilirsiniz.</div>

                        <div class="form-row">
                            <div class="col-lg-6">
                                <input type="text" value="" id="marka" name="marka" class="form-control inputBasvuru" />
                                <img class="edit-pencil position-absolute" src="{{asset('images/pencil.svg')}}" />
                            </div>
                        </div>

                    </div>

                    <div id="fatura-bilgileri" style="margin-top: 84px;">
                        <div class="basvuru-title mb-2">Fatura ve Tescil Sahibi Bilgileriniz</div>
                        <div class="lead mt-3 mb-5">Fatura bilgilerinizi giriniz. Marka tescili şirket üzerine olmak zorunda değildir. Bireysel başvuru yapılabilir.</div>
                        <div class="form-row mb-3">
                            <div class="form-group col-md-6 ">
                                <label>
                                <input required type="text" class="form-control inputBasvuru"  id="inputName" name="inputName" value="{{ $visitor->isim }}" placeholder="Ad Soyad veya Ünvan"></label>
                            </div>
                            <div class="form-group col-md-6 ">
                                <label>
                                <input required type="text" class="form-control inputBasvuru"  name="inputTCNo" id="inputTCNo" value="{{ $visitor->tc }}" placeholder="TCKN veya Vergi No"></label>
                            </div>
                        </div>

                        <div class="form-row mb-3">
                            <div class="form-group col-md-6">
                                <label>
                                <input required type="email" class="form-control inputBasvuru"  name="inputEmail" id="inputEmail" value="{{ $visitor->eposta }}" placeholder="E-mail"></label>
                            </div>
                            <div class="form-group col-md-6">
                                <label>
                                <input required type="text" class="form-control inputBasvuru"  name="inputTelefon" id="inputTelefon" value="{{ $visitor->tel }}" placeholder="Telefon"></label>
                            </div>
                        </div>

                        <div class="form-row mb-3">
                            <div class="form-group col-md-6">
                                <label>
                                <textarea required name="address" class="form-control inputBasvuru"  id="inputAdres" value="{{ $visitor->adres }}" placeholder="Adres"></textarea></label>
                            </div>
                            <div class="form-group col-md-6">
                                <span class="font-weight-bolder ml-4">Fatura Türü</span>
                                <div class="ml-4 mt-4">
                                    <div class="default-checkbox custom-control-inline">
                                        <input type="radio" id="bireysel" name="fatura_turu" value="bireysel" class="custom-control-input"
                                               {!! ($visitor->fatura_turu != 'kurumsal') ? 'checked="checked"' : '' !!} >
                                        <label class="custom-control-label default-checkbox-label" for="bireysel">Bireysel</label>
                                    </div>
                                    <div class="default-checkbox custom-control-inline">
                                        <input type="radio" id="kurumsal" name="fatura_turu" value="kurumsal" {!! ($visitor->fatura_turu == 'kurumsal') ? 'checked="checked"' : '' !!} class="custom-control-input">
                                        <label class="custom-control-label default-checkbox-label" for="kurumsal">Kurumsal</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="sinif-alani" style="margin-top:80px;">
                        <div class="d-flex justify-content-between flex-lg-row flex-column">
                            <div class="basvuru-title mb-2">Sınıfınızı/Sektörünüzü Seçin</div>
                            <div class="d-flex align-items-center">
                                <div class="default-checkbox custom-control-inline border-radius">
                                    <input type="radio" id="sektorInput" name="sektorOrSinif" value="sektor" class="custom-control-input sektorOrSinif" @if($basket->packet_id > 0) checked="checked" @endif>
                                    <label class="custom-control-label default-checkbox-label" for="sektorInput">Sektör ile</label>
                                </div>
                                <div class="default-checkbox custom-control-inline border-radius">
                                    <input type="radio" id="sinifInput" name="sektorOrSinif" value="sinif" class="custom-control-input sektorOrSinif" @if($basket->packet_id == 0) checked="checked" @endif>
                                    <label class="custom-control-label default-checkbox-label" for="sinifInput">Sınıf ile</label>
                                </div>
                            </div>
                        </div>

                        <div class="d-flex align-items-center flex-lg-row flex-column mt-3 mb-5">
                            <div class="lead ">Sektörünüzü ya da sınıfınızı seçin. Farklı sektörlerde de başvuru yapmanız taklit edilebilirliğinizi zorlaştırır.</div>

                            <a href="https://patentsorgu.com/marka-siniflari" target="_blank" class="position-relative border-radius-20 px-3 py-2" style="width:167px; background: rgba(0, 0, 0, 0.04);font-size: 15px;">
                                <i class="bi bi-info-circle-fill position-absolute" style="font-size: 18px;left: -7px;top: 14px;"></i> Sınıflar ne anlama geliyor?
                            </a>
                        </div>


                        <div class="row @if($basket->packet_id == 0) d-none @endif" id="sektorler">
                            @foreach($packages as $package)

                                @if($package->is_suggested == 1)
                                    @php $sector_defaults = [];$class_defaults = [];$service_defaults = []; @endphp
                                    @php
                                        $sector_defaults = explode(',', $package->sector_lists);
                                        $class_defaults = explode(',', $package->class_lists);
                                        $service_defaults = explode(',', $package->additional_services);

                                    @endphp
                                @endif
                            @endforeach
                            @foreach($sectors as $sector)
                            <div class="form-group col-md-6 mb-4">
                                <div class="default-checkbox custom-control custom-checkbox basvuru-checkbox">
                                    <input type="radio" value="{{ $sector->id }}" name="sectors" id="s{{ $sector->id }}" class="custom-control-input sektor-kontrol-input" {!! $basket->secili_sektorler == $sector->id ? 'checked="checked"' : '' !!}>
                                    <label class="custom-control-label sektorCheckbox d-flex align-items-center" for="s{{ $sector->id }}" id="sl{{ $sector->id }}">{{ $sector->name }}</label>
                                </div>
                            </div>
                            @endforeach
                        </div>

                        <div class="row @if($basket->packet_id > 0) d-none @endif" id="siniflar">
                            <?php
                            $siniflar = array();
                            ?>
                            @if(isset($basket->secili_siniflar))
                                <?php
                                $siniflar = json_decode($basket->secili_siniflar, true);
                                //dd($siniflar);
                                ?>
                            @endif
                            @for($c = 1; $c <= 47; $c++)
                            <div class="form-group col-md-2 mb-4">
                                <div class="default-checkbox custom-control-inline sinif-checkbox">
                                    <input type="checkbox"   name="classes[]" id="c{{ $c }}" data-classid="{{ $c }}" value="{{ $c }}.Sınıf"  class="custom-control-input sinif-control-input" {!! isset($siniflar) ? (in_array(($c.'.Sınıf'), $siniflar) ? 'checked="checked"' : ''): '' !!}>
                                    <label class="custom-control-label default-checkbox-label" for="c{{ $c }}">{{ $c }}.Sınıf</label>
                                </div>
                            </div>
                            @endfor
                        </div>
                    </div>

                    <div id="logonuz" style="margin-top: 60px;">
                        <div class="basvuru-title mb-2">Logonuz <span class="font-weight-light" style="font-size: 17px;"><em>(İsteğe Bağlı)</em></span></div>
                        <div class="lead mt-3 mb-5">Logonuzu buradan yükleyebilirsiniz. Logo yüklemek isteğe bağlıdır. Logonuz hazır ise yükleyebilirsiniz.</div>
                        <div class="logo-upload d-flex w-100 flex-column">
                            <span>Logonuzu Yükleyin</span>
                            <i class="bi bi-file-earmark-arrow-up-fill"></i>
                            <span>PNG, JPEG - Max 1 MB</span>
                        </div>
                    </div>
                    <input type="file" id="inputDosya" name="dosyalar[]" maxlength="1" class="asd"  accept="gif|jpg|png|tiff|tif"/>
                    <div id="ilave-hizmetler" style="margin-top: 84px;">
                        <div class="basvuru-title mb-2">İlave Hizmetler <span class="font-weight-light" style="font-size: 17px;"><em>(İsteğe Bağlı)</em></span></div>
                        <div class="lead mt-3 mb-5">Süreçte işinizi kolaylaştırabilecek diğer hizmetleri buradan ekleyebilirsiniz.</div>

                        <div class="row">
                            @foreach($services as $service)
                            <div class="form-group col-md-6 mb-4">
                                <div class="ilave-checkbox">
                                    <input type="checkbox" data-serviceprice="{{ $service->price }}" data-servicename="{{ $service->name }}" data-serviceshortname="{{ $service->name }}" data-serviceid="{{ $service->id }}" id="serv{{ $service->id }}" class="ilave-control-input" {!! in_array($service->id, $service_defaults) ? 'checked="checked"' : '' !!} name="services[]" value="{{ $service->id }}">
                                    <label class="ilave-control-label d-flex flex-column position-relative" for="serv{{ $service->id }}">
                                        <div class="ilavePrice position-absolute text-right d-flex flex-column">
                                            <span class="ilaveNumber">{{ str_replace('.00', '', $service->price) }}<span class="ilaveTlIcon"> ₺</span></span>

                                            <span class="ilaveKdv">+KDV</span>
                                        </div>
                                        <span class="ilaveTitle">{!! $service->name !!}</span>
                                        <span class="ilaveDesc">{!! $service->description !!}</span>
                                    </label>
                                </div>
                            </div>
                            @endforeach

                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="basvuru-ozeti mt-n8 make-me-sticky d-none d-md-block">
                        <div class="card">
                            <div class="card-header">
                                <h4>Başvuru Özetiniz</h4>
                                <div class="ozet-paket-name d-flex justify-content-between flex-row">
                                    @foreach($packages as $package)
                                        <?php
                                        $arr = explode(' ',trim($package->name));
                                        ?>
                                    <div class="pkt{{ $package->id }} {!! $basket->packet_id == $package->id ? 'active' : '' !!}" data-id="{{ $package->id }}">
                                        @if($arr)
                                            {{$arr[0]}}
                                        @else
                                            {{$package->name}}
                                        @endif
                                    </div>
                                    @endforeach
                                    <div class="pkt0 {!! $basket->packet_id == 0 ? 'active' : '' !!}">Kapsamlı</div>
                                </div>
                            </div>
                            <div class="card-body d-flex flex-column">
                                <div class="d-flex flex-column">
                                    <span class="description">MARKA</span>
                                    <span class="service-name" id="ozet-marka"></span>
                                </div>

                                <div class="d-flex flex-column mt-4">
                                    <span class="description">SEKTÖR VEYA SINIF</span>
                                    <span class="service-name sector-class-name">
                                        @if($basket->packet_id == 0)
                                            <?php
                                            $siniflar = json_decode($basket->secili_siniflar,true);
                                            ?>
                                            @if(isset($siniflar))
                                                @foreach($siniflar as $value)
                                                    {{str_replace('.Sınıf', '', $value)}}
                                                    @if(!$loop->last) - @endif
                                                    @endforeach
                                                @endif
                                        @else
                                            @if(isset($basket->secili_sektorler))
                                                {{$basket->getSector($basket->secili_sektorler)->name}}
                                            @endif

                                        @endif
                                    </span>
                                </div>


                                    <?php $a = $suggested_package->getServices('text') ?>
                                <div class="add-services-div @if(!isset($a)) d-none @endif">
                                    <div class="d-flex flex-column mt-4 mb-3">
                                        <span class="description">İLAVE HİZMETLER</span>
                                        <div class="d-flex flex-column" id="ilave_hizmetler">
                                            {!! $suggested_package->getServices('text') !!}
                                        </div>
                                    </div>
                                </div>

                                <div class="pck-name">
                                    <div class="d-flex flex-row justify-content-between mt-5 mb-3">
                                        <span class="description">@if($basket->packet_id == 0)KAPSAMLI @else {{$basket->getPacket($basket->packet_id)->name_mini}} @endif</span>
                                        <span class="price flex-shrink-0">
                                            @if($basket->packet_id == 0)
                                                {{ number_format($basket->getBazPrice($basket->package_type_id)->price ,0,',','.') }}
                                            @else
                                                {{ number_format($basket->getPacket($basket->packet_id)->price ,0,',','.') }}
                                            @endif ₺</span>
                                    </div>
                                </div>
                                <div class="ilave-siniflar">
                                    <div class="d-flex flex-row justify-content-between mb-3">
                                        <span class="description">İLAVE SINIFLAR</span>
                                        <span class="price flex-shrink-0">

                                            0 ₺</span>
                                    </div>
                                </div>

                                <div class="ilave-hizmetler">
                                    <div class="d-flex flex-row justify-content-between mb-3">
                                        <span class="description">İLAVE HİZMETLER</span>
                                        <span class="price flex-shrink-0">
                                            @if($basket->packet_id == 0)

                                            @endif
                                            0 ₺</span>
                                    </div>
                                </div>


                                <div class="d-flex flex-row justify-content-between mb-3">
                                    <span class="description">KDV (%18)</span>
                                    <span class="price flex-shrink-0 kdv-div" >{{ number_format($suggested_package->getTotal('KDV', $suggested_package->price, 0, 0 ) ,0,',','.') }} ₺</span>
                                </div>
                                <div class="d-flex flex-row justify-content-between">
                                    <span class="description">TOPLAM ÖDENECEK</span>
                                    <span class="total-price ml-auto flex-shrink-0 toplam-odenecek-div">{{ number_format($suggested_package->getTotal('TOTAL', $suggested_package->price, 0, 0 ) ,0,',','.') }} ₺</span>
                                </div>
                            </div>
                        </div>

                        <div class="d-block d-flex white-shadow-box bg-white mt-n4">
                            <div class="d-flex flex-row">
                                <img class="mr-2" src="{{asset('images/lock-small.svg')}}" style="height: 19px;" />
                                <div class="d-flex flex-column">
                                    <span class="iade">İADE GARANTİSİ!</span>
                                    <span class="iade-info">Red Durumunda Ücretsiz 2. Başvuru</span>
                                </div>
                            </div>
                        </div>

                        <button type="button" class="btn-green-degrade mt-3 text-white font-weight-bolder d-block py-4 odeme-yap" >ÖDEME ADIMINA İLERLE</button>

                        <div class="white-shadow-box sozlesme-box mt-4 d-flex flex-row">

                            <div class="default-checkbox custom-control-inline">
                                <input type="checkbox" id="sozlesme_onay" name="sozlesme_onay" value="bireysel" class="custom-control-input" checked="checked">
                                <label class="custom-control-label default-checkbox-label" for="sozlesme_onay"></label>
                            </div>
                            <div>
                                Başvurum için gereken <a href="https://patentsorgu.com/mesafeli-satis-sozlesmesi" target="_blank"><span class="font-weight-bolder"><u>hizmet sözleşmesini</u></span></a>  okudum, onaylıyorum.
                            </div>

                        </div>

                    </div>

                    <div class="basvuru-ozeti-mobil d-sm-none position-fixed">
                        <div class="box-outer position-relative">
                            <img src="{{asset('images/up.svg')}}" class="basvuru-up-icon position-absolute" />
                            <div class="box-inside d-flex flex-column">
                                <div class="mobile-ozet-baslik text-white">Başvuru Özetiniz</div>
                                <div class="d-flex justify-content-between mt-2">
                                    <div class="d-flex flex-row">
                                        <img class="mr-2" src="{{asset('images/lock-small.svg')}}" />
                                        <div class="d-flex flex-column">
                                            <span class="iade">İADE GARANTİSİ!</span>
                                            <span class="iade-info">Red Durumunda Ücretsiz 2. Başvuru</span>
                                        </div>
                                    </div>
                                    <div class="mobil-price text-white mobil-total">{{ number_format($suggested_package->getTotal('TOTAL', $suggested_package->price, $suggested_package->getPrice('class'), $suggested_package->getPrice('service') ) ,0,',','.') }} ₺</div>
                                </div>

                                <button type="submit" class="btn-green-degrade mt-3 text-white font-weight-bolder d-block odeme-yap">ÖDEME YAP</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="visitor_id" id="visitor_id" value="{{ $visitor->id }}">
            <input type="hidden" name="order_id" id="order_id" value="">
            <input type="hidden" name="basket_id" id="basket_id" value="{{ $basket->id }}">
            <input type="hidden" name="current_packet" id="current_packet" value="{{ $suggested_package->id }}">
            <input type="hidden" name="gorsel_adeti" id="gorsel_adeti" value="{{-- $p_first->gorsel_adeti --}}">
            <input type="hidden" name="current_service_price" id="current_service_price" value="0">
            <input type="hidden" name="main_current" id="main_current" value="{{ $suggested_package->id }}">
            <input type="hidden" name="class_count" id="class_count" value="1">
            <input type="hidden" name="as_count" id="as_count" value="0">

            <input type="hidden" name="sinif_ucreti" id="sinif_ucreti" value="0">
            <input type="hidden" name="ilave_sinif_ucretleri" id="ilave_sinif_ucretleri" value="0">
            <input type="hidden" name="ilave_hizmet_ucretleri" id="ilave_hizmet_ucretleri" value="0">
            <input type="hidden" name="basvuru_ucreti" id="basvuru_ucreti" value="@if($basket->packet_id == 0) {{$basket->getBazPrice($basket->package_type_id)->price}} @else {{$basket->getPacket($basket->packet_id)->price}}@endif">
            <input type="hidden" name="kdv" id="kdv" value="{{-- number_format(($p_first->price * 0.18) ,1,'.',',') --}}">
            <input type="hidden" name="total" id="total" value="{{-- number_format(($p_first->price * 0.18) + $p_first->price ,1,'.',',') --}}">
            <input type="hidden" name="basvuru_turu" id="basvuru_turu" value="basvuru">
    </section>
    {!! Form::close() !!}
@endsection

@section('css')
    <style>
    #fatura-bilgileri label{
        display:inherit;
        margin-bottom: 0;
    }
    .error .inputBasvuru {

        border:1px solid red;

    }
    </style>

@endsection

@section('js')
    <script src="{{asset('assets/js/jquery.MultiFile.min.js')}}" type="text/javascript" language="javascript"></script>
    <script src="{{asset('js/sweetalert2.min.js')}}"></script>

    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        function sepet_guncelle(secim){
            let packet_id = $('.paket-dis.active').data('packageid');
            let secili_sektorler = $("input[name='sectors']:checked").val();
            let secili_siniflar = [];
            $('#siniflar :checkbox:checked').each(function(i){
                secili_siniflar[i] = $(this).val();

            });
            let ilave_hizmetler = [];
            $('#ilave-hizmetler :checkbox:checked').each(function(i){
                ilave_hizmetler[i] = {id: $(this).data('serviceid'), name: $(this).data('servicename'),short_name: $(this).data('serviceshortname'), price: $(this).data('serviceprice')};

            });
            let basket_id = $('#basket_id').val();
            let visitor_id = $('#visitor_id').val();
            let tc = $('#inputTCNo').val();
            let eposta = $('#inputEmail').val();
            let adres = $('#inputAdres').val();
            let fatura_turu = $('#fatura_turu').val();


            $.ajax({

                url: "{{route('update-basket')}}",
                type: "POST",
                xhrFields: {
                    withCredentials: true
                },
                data: {
                    packet_id: packet_id,
                    secili_sektorler: secili_sektorler,
                    secili_siniflar: secili_siniflar,
                    ilave_hizmetler: ilave_hizmetler,
                    basket_id: basket_id,
                    visitor_id: visitor_id,
                    secim: secim,
                    tc: tc,
                    eposta: eposta,
                    adres: adres,
                    fatura_turu: fatura_turu,

                },
                success: function( data )
                {
                    //hizmetler seçildi.
                    let hizmetler = data.hizmetler
                    $.each(JSON.parse(hizmetler),function (index,value) {
                       //console.log(value.name);
                       $('#serv'+value.id).prop('checked', true);
                       index == 0 ? $('#ilave_hizmetler').html('<div class="service-name">' + value.short_name+ '</div>') : $('#ilave_hizmetler .service-name').append('<div class="service-name">' + value.short_name+ '</div>')

                   });
                    if(JSON.parse(hizmetler) == null){
                        $('.add-services-div').addClass('d-none');
                    }else{
                        $('.add-services-div').removeClass('d-none');
                    }
                    $('.ozet-paket-name div').removeClass('active');
                    $('.pkt'+data.paket_id).addClass('active');

                    $('.kdv-div').html(data.kdv + ' ₺')
                    $('.toplam-odenecek-div').html(data.toplam + ' ₺')
                    $('.pck-name .description').html(data.paket_ismi)
                    $('.pck-name .price').html(data.paket_price+ ' ₺')
                    $('.ilave-siniflar .price').html(data.sinif_ucreti+ ' ₺');
                    $('.ilave-hizmetler .price').html(data.ilave_price+ ' ₺');

                    if(data.paket_id == 0){

                        let yeni_sinif = '';
                        if(JSON.parse(data.siniflar)){
                            $.each(JSON.parse(data.siniflar),function (index,value) {

                                yeni_sinif = yeni_sinif + value+' - '

                            });
                            yeni_sinif = yeni_sinif.replaceAll('.Sınıf', '')
                            $('.sector-class-name').html(yeni_sinif.slice(0,-2));
                            $( "#sinifInput" ).trigger( "click" )
                        }else{
                            $( "#sektorInput" ).trigger( "click" )
                            let sektor_name = $('#sl'+data.sektor).html();

                            $('.sector-class-name').html(sektor_name);
                        }


                    }else{

                        $( "#sektorInput" ).trigger( "click" )
                        let sektor_name = $('#sl'+data.sektor).html();

                        $('.sector-class-name').html(sektor_name);
                    }


                },

            });
        }

        $('.paket-dis').click(function (){
            $('.paket-dis').removeClass('active');
            $('.ozet-paket-name div').removeClass('active');
            $(this).addClass('active');
            sepet_guncelle('paket');
        });

        $('.sinif-control-input').click(function () {
           //sepet_guncelle('sinif');
            $( ".paket-dis.pkt0" ).trigger( "click" )
        });

        $('.ilave-control-input').click(function () {
            //sepet_guncelle('ilave')
            $( ".paket-dis.pkt0" ).trigger( "click" )
        });
        $('.sektor-kontrol-input').click(function () {
            sepet_guncelle('sektor')
        });
        $( "#marka" ).keyup(function( event ) {
            $('#ozet-marka').html($( "#marka" ).val());
        })


        $('#sektorInput').click(function (){
            $('#sektorler').removeClass('d-none');
            $('#siniflar').addClass('d-none');
        });

        $('#sinifInput').click(function (){
            $('#sektorler').addClass('d-none');
            $('#siniflar').removeClass('d-none');
        });

        $('input[name=marka]').val(JSON.parse(localStorage.getItem('markWord')));
        $('#ozet-marka').html(JSON.parse(localStorage.getItem('markWord')));
        $('#inputName').val(JSON.parse(localStorage.getItem('isim')));
        $('#inputTelefon').val(JSON.parse(localStorage.getItem('telefon')));
        $('#search_id').val(JSON.parse(localStorage.getItem('search_id')));

        //$('#arastirilan_sektor').html(JSON.parse(localStorage.getItem('sector_name')));

        $(document).on('click','.odeme-yap',function (e) {
            e.preventDefault();

            let inputName = $('#inputName').val();
            let inputTCNo = $('#inputTCNo').val();
            let inputEmail = $('#inputEmail').val();
            let inputTelefon = $('#inputTelefon').val();
            let inputAdres = $('#inputAdres').val();
            if(inputName == '' || inputTCNo == '' || inputEmail == '' || inputTelefon == '' || inputAdres == '' ){


                inputName == '' ? $('#inputName').parent().addClass('error') : '';
                inputTCNo == '' ? $('#inputTCNo').parent().addClass('error') : '';
                inputEmail == '' ? $('#inputEmail').parent().addClass('error') : '';
                inputTelefon == '' ? $('#inputTelefon').parent().addClass('error') : '';
                inputAdres == '' ? $('#inputAdres').parent().addClass('error') : '';

                $([document.documentElement, document.body]).animate({
                    scrollTop: $("#fatura-bilgileri").offset().top
                }, 1000);
            } else {
                $("form").submit();
            }
        });

        $( ".inputBasvuru" ).keyup(function( event ) {
            $(this).parent().removeClass('error');
        }).keydown(function( event ) {
            if ( event.which == 13 ) {
                event.preventDefault();
            }
        });


    </script>

@endsection
