<div class="row clearfix">
    <div class="col-xl-3">
        <div class="form-group form-group-default required">
            <label>Paket Adı</label>
            {!! Form::text("name",null,['class'=>'form-control','required'=>'required','id'=>'name']) !!}
        </div>
    </div>
    <div class="col-xl-3">
        <div class="form-group form-group-default">
            <label>Paket Üst Bilgi</label>
            {!! Form::text("name_top",null,['class'=>'form-control',null,'id'=>'name_top']) !!}
        </div>
    </div>
    <div class="col-xl-3">
        <div class="form-group form-group-default required">
            <label>Paket Mini Adı</label>
            {!! Form::text("name_mini",null,['class'=>'form-control','required'=>'required','id'=>'name_mini']) !!}
        </div>
    </div>
    <div class="col-xl-3">
        <div class="form-group form-group-default input-group">
            <div class="form-input-group">
                <label>Paket İkonu</label>
                {!!  Form::text('icon', null, ['required','class'=>'form-control','id'=>'icon'])!!}
            </div>
            <div class="input-group-append ">
              <span class="input-group-text"><a href="javascript:;" onclick="moxman.browse({extensions:'jpg,jpeg,png,gif,svg',fields: 'icon', no_host: true});" class="btn btn-info"><i class="glyphicon glyphicon-plus"></i> Ekle</a>
              </span>
            </div>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-xl-3">
        <div class="form-group form-group-default required">
            <label>Türü</label>
            {!! Form::select("package_type_id", $package_types, null, ['class'=>'form-control','id'=>'type']) !!}
        </div>
    </div>
    <div class="col-xl-3">
        <div class="form-group form-group-default required">
            <label>Paket Önerilen mi?</label>
            {!! Form::select("is_suggested", ['0' => 'HAYIR', '1' => 'EVET'], null, ['class'=>'form-control','id'=>'is_suggested']) !!}
        </div>
    </div>
    <div class="col-xl-3">
        <div class="form-group form-group-default required">
            <label>Yıldız mi?</label>
            {!! Form::select("is_star", ['0' => 'HAYIR', '1' => 'EVET'], null, ['class'=>'form-control','id'=>'is_star']) !!}
        </div>
    </div>
    <div class="col-xl-3">
        <div class="form-group form-group-default">
            <label>İnaktif hizmet</label>
            {!! Form::text("inactive_services",null,['class'=>'form-control',null,'id'=>'inactive_services']) !!}
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-xl-12">
        <div class="form-group form-group-default required">
            <label>Paket Açıklaması</label>
            <textarea rows="2" cols="2" class='form-control elastic editorezy'  id="description" name="description" >@if(isset($package)){!! $package->description !!}@elseif(!empty(old('description'))){!! old('description')  !!}@endif</textarea>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-xl-4">
        <div class="form-group form-group-default">
            <label>Varsayılan Sınıf Adeti</label>
            {!! Form::text("class_count",isset($package) ? $package->class_count : 0,['class'=>'form-control',null,'id'=>'class_count']) !!}
        </div>
    </div>
    <div class="col-xl-4">
        <div class="form-group form-group-default form-group-default-select2">
            <label>Varsayılan Sektörler</label>
            {!! Form::select("sector_lists[]", $sectors, isset($package) ? explode(',', $package->sector_lists) : null, ['multiple' => 'multiple','class'=>'full-width','id'=>'sector_lists', 'data-init-plugin' => 'select2']) !!}
        </div>
    </div>
    <div class="col-xl-4">
        <div class="form-group form-group-default form-group-default-select2">
            <label>Varsayılan Ek Hizmetler</label>
            {!! Form::select("additional_services[]", $services, isset($package) ? explode(',', $package->additional_services) : null, ['multiple' => 'multiple','class'=>'full-width','id'=>'additional_services', 'data-init-plugin' => 'select2']) !!}
        </div>
    </div>

</div>

<div class="row clearfix">
    <div class="col-xl-4">
        <div class="form-group form-group-default required">
            <label>Fiyatı</label>
            {!! Form::text("price",null,['class'=>'form-control','required'=>'required','id'=>'price']) !!}
        </div>
    </div>
    <div class="col-xl-4">
        <div class="form-group form-group-default">
            <label>Eski Fiyatı</label>
            {!! Form::text("old_price",null,['class'=>'form-control','id'=>'old_price']) !!}
        </div>
    </div>
    <div class="col-xl-4">
        <div class="form-group form-group-default">
            <label>Kampanya Kısa Yazı</label>
            {!! Form::text("kampanya_text",null,['class'=>'form-control','id'=>'kampanya_text']) !!}
        </div>
    </div>


    <div class="col-xl-4">
        <div class="form-group form-group-default required">
            <label>Durumu</label>
            {!! Form::select("gorsel_adeti", ['1' => '1 GÖRSEL', '2' => '2 GÖRSEL', '3' => '3 GÖRSEL', '4' => '4 GÖRSEL', '5' => '5 GÖRSEL', '6' => '6 GÖRSEL', '7' => '7 GÖRSEL', '8' => '8 GÖRSEL', '9' => '9 GÖRSEL'], null, ['class'=>'form-control','id'=>'gorsel_adeti']) !!}
        </div>
    </div>
    <div class="col-xl-4">
        <div class="form-group form-group-default required">
            <label>Durumu</label>
            {!! Form::select("active", ['1' => 'AKTİF', '0' => 'PASİF'], null, ['class'=>'form-control','id'=>'active']) !!}
        </div>
    </div>
</div>


@push('scripts')
    @include('macros.editors')
@endpush



