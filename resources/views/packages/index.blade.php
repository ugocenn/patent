@extends('layouts.app')
@section('title', 'Paket Listeleri')
@section('content')
    <div class="content ">
        <br/>
        <!-- START CONTAINER FLUID -->
        <div class=" container-fluid   container-fixed-lg">
            <!-- START card -->
            <div class="card card-default">
                <div class="card-header ">
                    <div class="card-title">Paket Listeleri
                    </div>
                    <div class="pull-right">
                        <a aria-label="" href="{{ route('packages.create') }}" class="btn btn-success btn-icon-left m-b-10" type="button"><i class="pg-icon">plus</i><span class="">Yeni Paket Oluştur</span></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                    @include('partials.alerts.error')
                    <div class="table-responsive">
                        <table id="paketler" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Paket Türü</th>
                                <th>Paket Adı</th>
                                <th>Fiyatı</th>
                                <th>Eski Fiyat</th>
                                <th>Varsayılan Sınıflar</th>
                                <th>Varsayılan Sektörler</th>
                                <th>Varsayılan Ek Servisler</th>
                                <th>Durumu</th>
                                <th>Önerilen</th>
                                <th>Yildiz</th>
                                <th>İşlemler</th>
                            </tr>
                            </thead>


                            <tbody>
                            @foreach($packages as $package)
                            <tr>
                                <td>{!! $package->package_type->name !!}</td>
                                <td>{!! $package->name !!}</td>

                                <td>{{ $package->price }}</td>
                                <td>{{ $package->old_price }}</td>
                                <td>{{ $package->class_count }}</td>
                                <td>{{ $package->getSectors() }}</td>
                                <td>{{ $package->getServices() }}</td>
                                <td>{{ $package->active == 1 ? 'EVET' : 'HAYIR' }}</td>
                                <td>{{ $package->is_suggested == 1 ? 'EVET' : 'HAYIR' }}</td>
                                <td>{{ $package->is_star == 1 ? 'EVET' : 'HAYIR' }}</td>
                                <td>
                                    <a href="{{ route('packages.edit', createHashId($package->id)) }}" aria-label="" type="button" class="btn btn-primary btn-cons btn-animated from-left">
                                        <span>Düzenle</span>
                                        <span class="hidden-block">
													<i class="pg-icon">edit</i>
												</span>
                                    </a>

                                </td>
                            </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END card -->
        </div>
        <!-- END CONTAINER FLUID -->
    </div>

@endsection
