<div class="row clearfix">
    <div class="col-xl-12">
        <div class="form-group form-group-default required">
            <label>Ek Hizmet Adı</label>
            {!! Form::text("name",null,['class'=>'form-control','required'=>'required','id'=>'name']) !!}
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-xl-12">
        <div class="form-group form-group-default required">
            <label>Ek Hizmet Açıklaması</label>
            {!! Form::text("description",null,['class'=>'form-control','required'=>'required','id'=>'description']) !!}
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-xl-12">
        <div class="form-group form-group-default required">
            <label>Fiyatı</label>
            {!! Form::text("price",null,['class'=>'form-control','required'=>'required','id'=>'price']) !!}
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-xl-12">
        <div class="form-group form-group-default required">
            <label>Türü</label>
            {!! Form::select("type", ['basvuru' => 'BAŞVURU', 'tasarim-tescili' => 'TASARIM TESCİLİ', 'faydali-model' => 'FAYDALI MODEL', 'patent-tescili' => 'PATENT TESCİLİ'], null, ['class'=>'form-control','id'=>'type']) !!}
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-xl-12">
        <div class="form-group form-group-default required">
            <label>Durumu</label>
            {!! Form::select("active", ['1' => 'AKTİF', '0' => 'PASİF'], null, ['class'=>'form-control','id'=>'active']) !!}
        </div>
    </div>
</div>


