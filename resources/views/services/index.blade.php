@extends('layouts.app')
@section('title', 'Ek Hizmet Listeleri')
@section('content')
    <div class="content ">
        <br/>
        <!-- START CONTAINER FLUID -->
        <div class=" container-fluid   container-fixed-lg">
            <!-- START card -->
            <div class="card card-default">
                <div class="card-header ">
                    <div class="card-title">Ek Hizmet Listeleri
                    </div>
                    <div class="pull-right">
                        <a aria-label="" href="{{ route('services.create') }}" class="btn btn-success btn-icon-left m-b-10" type="button"><i class="pg-icon">plus</i><span class="">Yeni Ek Hizmet Oluştur</span></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                    @include('partials.alerts.error')
                    <div class="table-responsive">
                        <table id="datatable" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Türü</th>
                                <th>Ek Hizmet</th>
                                <th>Fiyatı</th>
                                <th>Durumu</th>
                                <th>İşlemler</th>
                            </tr>
                            </thead>


                            <tbody>
                            @foreach($services as $service)
                            <tr>
                                <td>{{ $service->type }}</td>
                                <td>{{ $service->name }}</td>
                                <td>{{ $service->price }}</td>
                                <td>{{ $service->active == 1 ? 'EVET' : 'HAYIR' }}</td>
                                <td>
                                    <a href="{{ route('services.edit', createHashId($service->id)) }}" aria-label=""  class="text-success">
                                        <span>Düzenle</span>
                                    </a>
                                    <a href="javascript:void(0);" onclick="return confirmOpsDelete('{{ createHashId($service->id) }}')" role="menuitem" class="text-danger">
                                        <i class="pg-trash"></i> Sil
                                    </a>
                                    {{ Form::open([
                                        'method' => 'DELETE',
                                        'url' => route("services.destroy", createHashId($service->id)),
                                        'id' => 'ops-delete-form-' . createHashId($service->id)
                                        ]) }}
                                    {{ Form::close() }}


                                </td>
                            </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END card -->
        </div>
        <!-- END CONTAINER FLUID -->
    </div>

@endsection
