<div class="row clearfix">
    <div class="col-xl-12">
        <div class="form-group form-group-default required">
            <label>Sınıf Adı</label>
            {!! Form::text("name",null,['class'=>'form-control','required'=>'required','id'=>'name']) !!}
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-xl-12">
        <div class="form-group form-group-default required">
            <label>Sınıf Ücreti</label>
            {!! Form::text("price",null,['class'=>'form-control','required'=>'required','id'=>'price']) !!}
        </div>
    </div>
</div>


