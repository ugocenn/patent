@extends('layouts.app')
@section('title', 'Sınıf Düzenle')
@section('content')
    <div class="content ">
        <br/>
        <!-- START CONTAINER FLUID -->
        <div class=" container-fluid   container-fixed-lg">
            <!-- START card -->
            {!! Form::model($product_class,['method' => 'PATCH', 'route' => ['product-classes.update', createHashId($product_class->id)],'id'=>'admin-form','role'=>'form','autocomplete'=>'off', 'novalidate' => 'novalidate', 'files' => true])!!}
            <input type="hidden" name="q_id" value="{{ $product_class->id }}">
            <div class="card card-default">
                <div class="card-header ">
                    <div class="card-title">Sınıflar
                    </div>
                    <div class="pull-right">
                        <button aria-label="" class="btn btn-success btn-icon-left m-b-10" type="submit"><i class="pg-icon">tick</i><span class=""> GÜNCELLE</span></button>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                    @include('partials.alerts.error')
                    <p class="fs-16 mw-80 m-b-40">Platfordaki mevcut sınıfı güncellemek için aşağıdaki form üzerinde değişiklik yapabilirsiniz. </p>
                    @include('product_classes.form')
                </div>
            </div>
            {!! Form::close() !!}
            <!-- END card -->
        </div>
        <!-- END CONTAINER FLUID -->
    </div>

@endsection

@section('scripts')

@endsection
