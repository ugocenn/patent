@extends('layouts.app')
@section('title', 'Sınıf Listeleri')
@section('content')
    <div class="content ">
        <br/>
        <!-- START CONTAINER FLUID -->
        <div class=" container-fluid   container-fixed-lg">
            <!-- START card -->
            <div class="card card-default">
                <div class="card-header ">
                    <div class="card-title">Sınıf Listeleri
                    </div>
                    <div class="pull-right">
                        <a aria-label="" href="{{ route('product-classes.create') }}" class="btn btn-success btn-icon-left m-b-10" type="button"><i class="pg-icon">plus</i><span class="">Yeni Sınıf Oluştur</span></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                    @include('partials.alerts.error')
                    <div class="table-responsive">
                        <table id="datatable" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Sınıf Adı</th>
                                <th>Fiyatı</th>
                                <th>İşlemler</th>
                            </tr>
                            </thead>


                            <tbody>
                            @foreach($product_classes as $product_class)
                            <tr>
                                <td>{{ $product_class->name }}</td>
                                <td>{{ str_replace('.00', '', $product_class->price) }}</td>

                                <td>
                                    <a href="{{ route('product-classes.edit', createHashId($product_class->id)) }}" aria-label=""  class="text-success">
                                        <span>Düzenle</span>
                                    </a>
                                    <a href="javascript:void(0);" onclick="return confirmOpsDelete('{{ createHashId($product_class->id) }}')" role="menuitem" class="text-danger">
                                        <i class="pg-trash"></i> Sil
                                    </a>
                                    {{ Form::open([
                                        'method' => 'DELETE',
                                        'url' => route("product-classes.destroy", createHashId($product_class->id)),
                                        'id' => 'ops-delete-form-' . createHashId($product_class->id)
                                        ]) }}
                                    {{ Form::close() }}


                                </td>

                            </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END card -->
        </div>
        <!-- END CONTAINER FLUID -->
    </div>

@endsection
