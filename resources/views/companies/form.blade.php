<div class="row clearfix">
    <div class="col-xl-4">
        <div class="form-group form-group-default required">
            <label>Firma Adı (Kısa)</label>
            {!! Form::text("name",null,['class'=>'form-control','required'=>'required','id'=>'name']) !!}
        </div>
    </div>
    <div class="col-xl-4">
        <div class="form-group form-group-default required">
            <label>Firma Ünvanı</label>
            {!! Form::text("company_title",null,['class'=>'form-control','required'=>'required', 'id'=>'company_title']) !!}
        </div>
    </div>
    <div class="col-xl-4">
        <div class="form-group form-group-default required">
            <label>Vergi No</label>
            {!! Form::text("tax_number",null,['class'=>'form-control','required'=>'required', 'id'=>'tax_number']) !!}
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-xl-4">
        <div class="form-group form-group-default required">
            <label>Meslek Odası Sicil No</label>
            {!! Form::text("trade_number",null,['class'=>'form-control','required'=>'required','id'=>'trade_number']) !!}
        </div>
    </div>
    <div class="col-xl-4">
        <div class="form-group form-group-default required">
            <label>Firma İşkur No</label>
            {!! Form::text("iskur_no",null,['class'=>'form-control','required'=>'required', 'id'=>'iskur_no']) !!}
        </div>
    </div>
    <div class="col-xl-4">
        <div class="form-group form-group-default required">
            <label>Firma E-posta</label>
            {!! Form::text("email",null,['class'=>'form-control','required'=>'required', 'id'=>'email']) !!}
        </div>
    </div>
</div>


<div class="row clearfix">
    <div class="col-xl-4">
        <div class="form-group form-group-default required ">
            <label>Firma SGK Numaraları</label>
            {!! Form::text("sgk_numbers",null,['class'=>'tagsinput custom-tag-input','required'=>'required', 'id'=>'sgk_numbers']) !!}

        </div>
    </div>
    <div class="col-xl-4">
        <div class="form-group form-group-default required">
            <label>İletişim Adresi</label>
            {!! Form::text("address",null,['class'=>'form-control','required'=>'required', 'id'=>'address']) !!}
        </div>
    </div>
    <div class="col-xl-4">
        <div class="form-group form-group-default required">
            <label>Telefonu</label>
            {!! Form::text("phone",null,['class'=>'form-control','required'=>'required','id'=>'phone']) !!}
        </div>
    </div>
</div>


<div class="row clearfix">
    <div class="col-xl-6">
        <div class="form-group form-group-default required ">
            <label>Yetkili Adı Soyadı</label>
            {!! Form::text("official_name",null,['class'=>'form-control','required'=>'required', 'id'=>'official_name']) !!}

        </div>
    </div>
    <div class="col-xl-6">
        <div class="form-group form-group-default required">
            <label>Yetkili T.C.</label>
            {!! Form::text("official_tc",null,['class'=>'form-control','required'=>'required', 'id'=>'official_tc']) !!}
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-xl-6">
        <div class="form-group form-group-default required">
            <label>Yetkili Doğum Yeri</label>
            {!! Form::text("official_birth_place",null,['class'=>'form-control','required'=>'required','id'=>'official_birth_place']) !!}
        </div>
    </div>
    <div class="col-xl-6">
        <div class="form-group form-group-default required">
            <label>Yetkili Doğum Tarihi</label>
            {!! Form::text("official_birth_date",null,['class'=>'form-control','required'=>'required','id'=>'official_birth_date']) !!}
        </div>
    </div>
</div>


