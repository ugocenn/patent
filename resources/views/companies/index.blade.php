@extends('layouts.app')
@section('title', 'Firma Havuzu')
@section('content')
    <div class="content ">
        <br/>
        <!-- START CONTAINER FLUID -->
        <div class=" container-fluid   container-fixed-lg">
            <!-- START card -->
            <div class="card card-default">
                <div class="card-header ">
                    <div class="card-title">Firma Bilgileri
                    </div>
                    <div class="pull-right">

                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                    @include('partials.alerts.error')
                    <div class="table-responsive">
                        <table id="datatable" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Firma Adı</th>
                                <th>E-posta</th>
                                <th>Telefon</th>
                                <th>Adres</th>
                                <th>İşlemler</th>
                            </tr>
                            </thead>


                            <tbody>
                            @foreach($companies as $company)
                            <tr>
                                <td>{{ $company->name }}</td>
                                <td>{{ $company->email }}</td>
                                <td>{{ $company->phone }}</td>
                                <td>{{ $company->address }}</td>

                                <td>
                                    <a href="{{ route('companies.edit', createHashId($company->id)) }}" aria-label="" type="button" class="btn btn-primary btn-cons btn-animated from-left">
                                        <span>Düzenle</span>
                                        <span class="hidden-block">
													<i class="pg-icon">edit</i>
												</span>
                                    </a>

                                </td>
                            </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END card -->
        </div>
        <!-- END CONTAINER FLUID -->
    </div>

@endsection
