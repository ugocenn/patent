@extends('layouts.app')
@section('title', 'Firma')
@section('content')
    <div class="content ">
        <br/>
        <!-- START CONTAINER FLUID -->
        <div class=" container-fluid   container-fixed-lg">
            <!-- START card -->
            {!! Form::open(['route' => 'companies.store','id'=>'admin-form','role'=>'form','autocomplete'=>'off', 'novalidate' => 'novalidate'])!!}
            <div class="card card-default">
                <div class="card-header ">
                    <div class="card-title">Firma
                    </div>
                    <div class="pull-right">
                        <button aria-label="" class="btn btn-success btn-icon-left m-b-10" type="submit"><i class="pg-icon">tick</i><span class=""> KAYDET</span></button>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                    @include('partials.alerts.error')
                    <p class="fs-16 mw-80 m-b-40">Platforma yeni firma eklemek için aşağıdaki formu doldurabilirsiniz. </p>
                    @include('companies.form')
                </div>
            </div>
            {!! Form::close() !!}
            <!-- END card -->
        </div>
        <!-- END CONTAINER FLUID -->
    </div>

@endsection
@section('scripts')
<script>
    $('.custom-tag-input').tagsinput({
    });

</script>

@endsection
