<div class="row clearfix">
    <div class="col-xl-4">
        <div class="form-group form-group-default required">
            <label>Kategori Adı</label>
            {!! Form::text("name",null,['class'=>'form-control','required'=>'required','id'=>'name']) !!}
        </div>
    </div>
    <div class="col-xl-4">
        <div class="form-group form-group-default required">
            <label>Başlık</label>
            {!! Form::text("title",null,['class'=>'form-control','required'=>'required','id'=>'title']) !!}
        </div>
    </div>
    <div class="col-xl-4">
        <div class="form-group form-group-default">
            <label>Açıklama</label>
            {!! Form::text("description",null,['class'=>'form-control','required'=>'required','id'=>'description']) !!}
        </div>
    </div>
</div>



<div class="row clearfix">
    <div class="col-xl-4">
        <div class="form-group form-group-default">
            <label>Kategori Bağlantısı</label>
            {!! Form::text("slug",null,['class'=>'form-control','required'=>'required','id'=>'slug']) !!}
        </div>
    </div>
    <div class="col-xl-4">
        <div class="form-group form-group-default">
            <label>Meta Açıklama</label>
            {!! Form::text("metadescription",null,['class'=>'form-control','required'=>'required','id'=>'metadescription']) !!}
        </div>
    </div>
    <div class="col-xl-4">
        <div class="form-group form-group-default">
            <label>Kelimeler</label>
            {!! Form::text("keywords",null,['class'=>'form-control','required'=>'required','id'=>'keywords']) !!}
        </div>
    </div>
</div>

@push('scripts')
    <script>
        function slug(text) {
            var trMap = {
                'çÇ':'c',
                'ğĞ':'g',
                'şŞ':'s',
                'üÜ':'u',
                'ıİ':'i',
                'öÖ':'o'
            };
            for(var key in trMap) {
                text = text.replace(new RegExp('['+key+']','g'), trMap[key]);
            }
            return  text.replace(/[^-a-zA-Z0-9\s]+/ig, '') // remove non-alphanumeric chars
                .replace(/\s/gi, "-") // convert spaces to dashes
                .replace(/[-]+/gi, "-") // trim repeated dashes
                .toLowerCase();

        }

        $('#name').keyup(function () {
            var pageSlug = slug($('#name').val());
            $('#slug').val(pageSlug);
        });

    </script>
@endpush
