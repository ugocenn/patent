@extends('layouts.app')
@section('title', 'Kategori Düzenle')
@section('content')
    <div class="content ">
        <br/>
        <!-- START CONTAINER FLUID -->
        <div class=" container-fluid   container-fixed-lg">
            <!-- START card -->
            {!! Form::model($category,['method' => 'PATCH', 'route' => ['categories.update', createHashId($category->id)],'id'=>'admin-form','role'=>'form','autocomplete'=>'off', 'novalidate' => 'novalidate', 'files' => true])!!}
            <input type="hidden" name="q_id" value="{{ $category->id }}">
            <div class="card card-default">
                <div class="card-header ">
                    <div class="card-title">Kategoriler
                    </div>
                    <div class="pull-right">
                        <button aria-label="" class="btn btn-success btn-icon-left m-b-10" type="submit"><i class="pg-icon">tick</i><span class=""> GÜNCELLE</span></button>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                    @include('partials.alerts.error')
                    <p class="fs-16 mw-80 m-b-40">Platfordaki mevcut kategoriyi güncellemek için aşağıdaki form üzerinde değişiklik yapabilirsiniz. </p>
                    @include('categories.form')
                </div>
            </div>
            {!! Form::close() !!}
            <!-- END card -->
        </div>
        <!-- END CONTAINER FLUID -->
    </div>

@endsection

@section('scripts')

@endsection
