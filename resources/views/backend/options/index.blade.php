@extends('layouts.app')
@section('title', 'Site Ayarları')
@section('content')
    <div class="content ">
        <br/>

        <div class=" container-fluid   container-fixed-lg">
            {!! Form::open(['route' => 'option.save','id'=>'admin-form','role'=>'form','autocomplete'=>'off', 'novalidate' => 'novalidate'])!!}
            <div class="card card-default">
                <div class="card-header ">
                    <div class="card-title">Site Ayarları
                    </div>
                    <div class="pull-right">
                        <button aria-label="" class="btn btn-success btn-icon-left m-b-10" type="submit"><i class="pg-icon">tick</i><span class=""> KAYDET</span></button>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                    @include('partials.alerts.error')

                    <div class="row clearfix">
                        <div class="col-xl-12">
                            <div class="form-group form-group-default required">
                                <label>Site İsmi(max 60 karakter)</label>
                                {!! Form::text("site_name",$option->site_name,['class'=>'form-control','required'=>'required','id'=>'site_name']) !!}
                            </div>
                            <div class="form-group form-group-default required">
                                <label>Site Tanımı(max 160 karakter)</label>
                                {!! Form::text("site_desc",$option->site_desc,['class'=>'form-control','required'=>'required','id'=>'site_desc']) !!}
                            </div>
                            <div class="form-group form-group-default required">
                                <label>Site Keywords(max 160 karakter)</label>
                                {!! Form::text("site_keywords",$option->site_desc,['class'=>'form-control','required'=>'required','id'=>'site_keywords']) !!}
                            </div>
                            <div class="form-group form-group-default ">
                                <label>Google Tags Kod</label>
                                {!! Form::textarea("google_tags",$option->google_tags,['class'=>'form-control','id'=>'google_tags', 'style'=>'height:200px;']) !!}
                            </div>
                            <div class="form-group form-group-default ">
                                <label>Telefon</label>
                                {!! Form::text("telefon",$option->telefon,['class'=>'form-control','id'=>'telefon']) !!}
                            </div>
                            <div class="form-group form-group-default ">
                                <label>Whatsapp Numarası</label>
                                {!! Form::text("whatsapp",$option->whatsapp,['class'=>'form-control','id'=>'whatsapp']) !!}
                            </div>
                            <div class="form-group form-group-default ">
                                <label>Facebook</label>
                                {!! Form::text("facebook",$option->facebook,['class'=>'form-control','id'=>'facebook']) !!}
                            </div>
                            <div class="form-group form-group-default ">
                                <label>İnstagram</label>
                                {!! Form::text("instagram",$option->instagram,['class'=>'form-control','id'=>'instagram']) !!}
                            </div>
                            <div class="form-group form-group-default ">
                                <label>Linkedin</label>
                                {!! Form::text("linkedin",$option->linkedin,['class'=>'form-control','id'=>'linkedin']) !!}
                            </div>
                            <div class="form-group form-group-default ">
                                <label>Youtube</label>
                                {!! Form::text("youtube",$option->youtube,['class'=>'form-control','id'=>'youtube']) !!}
                            </div>
                            <div class="form-group form-group-default ">
                                <label>E-Posta</label>
                                {!! Form::text("eposta",$option->eposta,['class'=>'form-control','id'=>'eposta']) !!}
                            </div>
                            <div class="form-group form-group-default ">
                                <label>Google Sheet ID</label>
                                {!! Form::text("sheet",$option->sheet,['class'=>'form-control','id'=>'sheet']) !!}
                            </div>
                            <div class="form-group form-group-default ">
                                <label>İyzico Api Key</label>
                                {!! Form::text("iyzico_apikey",$option->iyzico_apikey,['class'=>'form-control','id'=>'iyzico_apikey']) !!}
                            </div>
                            <div class="form-group form-group-default ">
                                <label>İyzico Secret Key</label>
                                {!! Form::text("iyzico_secretkey",$option->iyzico_secretkey,['class'=>'form-control','id'=>'iyzico_secretkey']) !!}
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            {!! Form::close() !!}
            <!-- END card -->
        </div>
        <!-- END CONTAINER FLUID -->
    </div>

@endsection
@section('scripts')


@endsection
