@extends('frontend.app')
@section('title','Patent Sorgu')

@section('content')
    <div class="bg-img-hero-content" style="background-image: url({{asset('images/bg.jpg')}});">
        <div class="container" style="width: 960px;">
            <div class="row">
                <div class="position-absolute manset-image">
                    <img src="{{asset('images/demo/rehber_manset.jpg')}}" class="img-fluid" />
                </div>

            </div>
        </div>
    </div>

    <section class="container rehber-baslik mb-5" style="max-width: 960px">
        <div class="row">
            <div class="col-12 text-lg-center">
                <div class="">
                    <span class="d-block subtitle">REHBERLER</span>
                    <h1>2020’nin Önemli Buluşları</h1> 
                    <span class="d-block lead mb-4 mx-auto">2020’de meydana gelen önemli buluşları sizin için listeledik. Keyifli okumalar!</span>
                </div>
            </div>
        </div>
    </section>

    <section class="container rehber-icerik" style="max-width: 960px">
        <div class="row">
            <div class="col-12 text-center">
                <p>Adipiscing ipsum massa, tempor ullamcorper. Iaculis est bibendum tempus scelerisque augue vel. Sapien, consectetur lorem mus vel senectus. Massa ac dui eget arcu massa, tellus ut adipiscing. Nunc nulla pretium mi condimentum tellus malesuada sem tortor. Netus aliquet mi vulputate et nascetur sodales sit. Cum eu dolor facilisi sed congue imperdiet. Auctor eu vitae eu nunc aliquam phasellus scelerisque amet. Platea commodo ac mattis nisl. Sagittis feugiat non lectus etiam id eget sed sociis porttitor.</p>
                <img src="{{asset('images/demo/rehber_manset.jpg')}}" class="img-fluid" style=" margin-bottom:60px;" />

                <p><strong>Commodo egestas sed morbi velit velit, et amet, erat id.</strong></p>
                <p>Leo ultricies nunc ultrices quis tortor ipsum. Sed tempus senectus mauris neque, vivamus euismod viverra. Fermentum sed lacus, enim eu. A ac non vestibulum neque. Iaculis vitae, ultrices ut ut feugiat arcu. Blandit aliquet ultrices est non donec tristique porttitor at. Aenean sed magna ut arcu enim, eu ipsum, faucibus. Nulla pretium etiam quis dictum in est egestas platea. Aliquet etiam iaculis est placerat cras tempor, elit purus.</p>
                <img src="{{asset('images/demo/rehber_manset.jpg')}}" class="img-fluid" style=" margin-bottom:60px;" />
                <p><strong>Commodo egestas sed morbi velit velit, et amet, erat id.</strong></p>
                <p>Leo ultricies nunc ultrices quis tortor ipsum. Sed tempus senectus mauris neque, vivamus euismod viverra. Fermentum sed lacus, enim eu. A ac non vestibulum neque. Iaculis vitae, ultrices ut ut feugiat arcu. Blandit aliquet ultrices est non donec tristique porttitor at. Aenean sed magna ut arcu enim, eu ipsum, faucibus. Nulla pretium etiam quis dictum in est egestas platea. Aliquet etiam iaculis est placerat cras tempor, elit purus.</p>
                <p>Leo ultricies nunc ultrices quis tortor ipsum. Sed tempus senectus mauris neque, vivamus euismod viverra. Fermentum sed lacus, enim eu. A ac non vestibulum neque. Iaculis vitae, ultrices ut ut feugiat arcu. Blandit aliquet ultrices est non donec tristique porttitor at. Aenean sed magna ut arcu enim, eu ipsum, faucibus. Nulla pretium etiam quis dictum in est egestas platea. Aliquet etiam iaculis est placerat cras tempor, elit purus.</p>

            </div>
        </div>
    </section>

    <section class="yeni-sorgu" style="background-image: url({{asset('images/bg.jpg')}});">
        <div class="d-lg-flex align-items-lg-center">
            <div class="container position-relative space-2 space-0--lg mt-lg-8">
                <div class="row justify-content-lg-between ">
                    <div class="order-2 order-md-1 col-md-5 mt-md-5">
                        <div class="marka-form mt-3 mt-lg-0">
                            <form class="js-validate" novalidate="novalidate">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="marka" id="marka"
                                           placeholder="MARKA ADI"/>
                                </div>

                                <div class="form-group">
                                    <select name="sektor" id="sektor">
                                        <option>TÜM SEKTÖRLER</option>
                                        <option>Makina</option>
                                        <option>Mobilya</option>
                                        <option>Giyim</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <input type="text" class="form-control" name="isim" id="isim" placeholder="İSİM"/>
                                </div>

                                <div class="form-group">
                                    <input type="text" class="form-control" name="telefon" id="telefon"
                                           placeholder="0 5__ ___ __ __"/>
                                </div>

                                <div class="form-group form-check">
                                    <input type="checkbox" class="form-check-input" id="iletisimIzni" checked>
                                    <label class="form-check-label" for="iletisimIzni">Sorgulanan Marka için <span
                                            class="boldUndeline">İletişim İzni</span> vermeyi kabul ediyorum.</label>
                                </div>


                                <button type="submit" class="btn btn-block btn-primary">HEMEN SORGULA</button>
                            </form>
                        </div>
                    </div>

                    <div class="order-1 order-md-2 col-md-6 mb-7 mb-lg-0 mt-md-5">
                        <h1 class="hero-h1 text-white text-right mb-0 mt-4 font-weight-bolder">MARKANI HEMEN <br class="d-block"/>ÜCRETSİZ SORGULA</h1>
                        <span
                            class="hero-span d-block text-white  text-right w-100">20 Dakikada Başvuru için <br
                                class="d-lg-block">Arayalım!</span>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="container pt-100">
        <div class="row">
            <div class="col-12 text-lg-center">
                <div class="mb-120">
                    <span class="d-block subtitle">DİĞER SERVİSLER</span>
                    <h2>Tüm Servisler</h2>
                    <span class="d-block lead mb-4 mx-auto">Her türlü, tasarım tescil, yenileme, adres değişikliği, ünvan değişikliği gibi işlemlerde yanınızdayız.</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="service-box d-flex ">
                    <div class="service-icon align-self-center text-left order-2 order-md-1"><i
                            class="bi bi-paperclip"></i></div>
                    <div class="service-body order-1 order-md-2">
                        <span class="d-block subtitle">DİĞER SERVİSLER</span>
                        <h3>Yurt Dışı Tescil İşlemleri</h3>
                        <span class="d-block bottom-lead mb-4">Yurt Dışı için ürün ve servislerinizi tescil edebilirsiniz. Hemen başvurun!</span>
                    </div>
                    <div class="service-button align-self-center d-none d-md-block order-3">
                        <a href="#" class="btn btn-third my-auto">BAŞVUR</a>
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-6">
                <div class="service-box d-flex ">
                    <div class="service-icon align-self-center text-left order-2 order-md-1"><i
                            class="bi bi-paperclip"></i></div>
                    <div class="service-body  order-1 order-md-2">
                        <span class="d-block subtitle">DİĞER SERVİSLER</span>
                        <h3>Yurt Dışı Tescil İşlemleri</h3>
                        <span class="d-block bottom-lead mb-4">Yurt Dışı için ürün ve servislerinizi tescil edebilirsiniz. Hemen başvurun!</span>
                    </div>
                    <div class="service-button align-self-center d-none d-md-block order-3">
                        <a href="#" class="btn btn-third my-auto">BAŞVUR</a>
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-6">
                <div class="service-box d-flex ">
                    <div class="service-icon align-self-center text-left order-2 order-md-1"><i
                            class="bi bi-paperclip"></i></div>
                    <div class="service-body order-1 order-md-2">
                        <span class="d-block subtitle">DİĞER SERVİSLER</span>
                        <h3>Yurt Dışı Tescil İşlemleri</h3>
                        <span class="d-block bottom-lead mb-4">Yurt Dışı için ürün ve servislerinizi tescil edebilirsiniz. Hemen başvurun!</span>
                    </div>
                    <div class="service-button align-self-center d-none d-md-block order-3">
                        <a href="#" class="btn btn-third my-auto">BAŞVUR</a>
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-6">
                <div class="service-box d-flex ">
                    <div class="service-icon align-self-center text-left order-2 order-md-1"><i
                            class="bi bi-paperclip"></i></div>
                    <div class="service-body order-1 order-md-2">
                        <span class="d-block subtitle">DİĞER SERVİSLER</span>
                        <h3>Yurt Dışı Tescil İşlemleri</h3>
                        <span class="d-block bottom-lead mb-4">Yurt Dışı için ürün ve servislerinizi tescil edebilirsiniz. Hemen başvurun!</span>
                    </div>
                    <div class="service-button align-self-center d-none d-md-block order-3">
                        <a href="#" class="btn btn-third my-auto">BAŞVUR</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="container-fluid bg-light services">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="ml-100 pb-230">
                        <span class="d-block subtitle">REHBERLER</span>
                        <h2>İlgili Rehberler</h2>
                        <span class="d-block lead mb-4">Yeni İş Süreçlerinizde Size Destek Olacak Tüm Rehberlerimizi Görüntüleyin</span>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="container position-relative pt-3 pt-lg-0 pb-5 mt-lg-n10">
        <div class="row">
            @foreach(getRehbers(2) as $rehber)
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card content-card ml-auto" style="background-image: url({{ $rehber->image }});">
                        <span>REHBERLER</span>
                        <a href="#"><h3 class="mt-3 mb-4">{{ $rehber->title }}</h3></a>
                        <p class="mb-0">{!! $rehber->short_text !!}</p>
                        <a href="{{ route('show-page', ['rehberler', $rehber->slug]) }}"><i
                                class="bi bi-arrow-right"></i></a>
                    </div>
                </div>
            @endforeach

            <div class="col-12 col-md-6 col-lg-4">
                <div class="card content-card ml-auto gradient-purple"
                     style="background-image: url({{asset('images/tum-rehberler-bg.svg')}});">
                    <span>REHBERLER</span>
                    <a href="#"><h3 class="mt-3 mb-4">Tüm Rehberler!</h3></a>
                    <p class="mb-0">&nbsp;</p>
                    <a href="#" class="text-right"><i class="bi bi-arrow-right"></i></a>
                </div>
            </div>

        </div>
    </section>

@endsection

@section('css')
@endsection

@section('js')
@endsection
