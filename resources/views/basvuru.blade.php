@extends('frontend.app')
@section('title','Patent Sorgu')

@section('content')
    <?php
    $sinif_sayisi = array(
        0 => 'Tek',
        1 => 'Tek',
        2 => 'İki',
        3 => 'Üç',
        4 => 'Dört',
        5 => 'Beş',
        6 => 'Altı',
        7 => 'Yedi',
        8 => 'Sekiz',
        9 => 'Dokuz',
        10 => 'On'
    )

    ?>
    <div class="bg-img-hero-content basvuru-hero" style="background-image: url({{asset('images/bg.jpg')}});">
        <!-- Main Content -->
        <div class="d-lg-flex align-items-lg-center title-container">
            <div class="container position-relative space-2 space-0--lg mt-lg-8">
                <div class="row justify-content-lg-between ">
                    <h1 class="hero-h1 text-white text-center mb-0 w-100 ">BAŞVURU <br class="d-md-none"> ADIMI</h1>
                    <span class="hero-lead d-block text-center text-white w-100 mt-4">

                    </span>
                </div>

            </div>
        </div>
    </div>
    {!! Form::open(['url' => '/basvuru/basvuru','id'=>'admin-form','role'=>'form','autocomplete'=>'off', 'novalidate' => 'novalidate', 'files' => true])!!}
    <section class="container py-5 basvuruMainContainer">
            <div class="row">
                <div class="col-12 col-md-8">
                    <div id="opResult"></div>
                    <div id="basvuru-paketi" class="mt-n10 mt-md-auto">
                        <div class="basvuru-title mb-2 d-none d-md-block">Başvuru Paketinizi Seçin</div>
                        <div class="lead mt-3 mb-5 d-none d-md-block">Avantajları paketlerimizden birisini seçin, ya da Size Özel ile kendiniz oluşturun.</div>
                        <div class="row no-wrap overflow-auto" style="min-height: 356px;">
                            @php $i = 1; $a = ""; @endphp
                            @foreach($packages as $package)
                                @if($i == 1)
                                    @php $p_first = $package;$suggested_package = $package;  @endphp
                                @endif
                            <div class="col-md-4">
                                <div class="paket-dis pkt{{ $package->id }} {!! $package->is_suggested == 1 ? 'active' : '' !!}" data-gorseladeti="{{ $package->gorsel_adeti }}" data-packageid="{{ $package->id }}" data-classlist="{{ $package->class_count > 0 ? $package->class_count : '0' }}" data-servicelist="{{ strlen($package->additional_services) > 0 ? $package->additional_services : '0' }}" data-servicelistprice="{{ strlen($package->additional_services) > 0 ? $package->getServices('price') : '0' }}" data-pckprice="{{ $package->price }}" data-pckname="{{ $package->name_mini }}">
                                    <div class="card paket">
                                        <div class="card-header">
                                            <div class="paket-icon text-center">{!! $package->is_star == 1 ? '<i class="bi bi-star-fill"></i>' : '' !!}</div>
                                            <div class="baslik-ust">{{$package->name_top}}</div>
                                            <h4 class="mt-2">{!! $package->name !!}</h4>
                                        </div>
                                        <div class="card-body">
                                            <?php
                                            // Sınıf sayısını yazdırmak için
                                            $sayi = $package->class_count;

                                            // Ek hizmetleri kartlara yazdırmak için
                                            if(isset($package->additional_services)){
                                                $hizmet_ids = explode(',',$package->additional_services);
                                            }



                                            ?>
                                            <div class="ortaYazi">
                                                <div><span class="font-weight-bold">{{$sinif_sayisi[$sayi]}} Sınıflı</span> Başvuru</div>
                                                @if(isset($package->inactive_services))
                                                <div><del><em>{{$package->inactive_services}}</em></del></div>
                                                @endif

                                                @if(isset($hizmet_ids))
                                                @foreach($hizmet_ids as $hizmet)
                                                        <div>{{$additional_services[$hizmet]}}</div>
                                                    @endforeach
                                                @endif
                                            </div>

                                            <div class="w-100 paket-icon mt-4" style="height: 62px;">
                                                <img src="{!! $package->icon !!}" style="height: 62px;width: 62px;" />
                                            </div>
                                            
                                        </div>
                                        <div class="card-footer">
                                            <span class="d-block w-100 text-center" style="font-size: 24px;font-weight:700">{!! str_replace('.00', '', $package->price) !!} <span style="font-size: 18px">₺</span>
                                            <br><span style="font-size: 13px">+ KDV</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                @php $i++; @endphp
                            @endforeach
                            <div class="col-md-4">
                                <div class="paket-dis pkt0" data-packageid="0">
                                    <div class="card paket">
                                        <div class="card-header">
                                            <div class="paket-icon text-center"></div>
                                            <div class="baslik-ust"></div>
                                            <h4 class="mt-2">Kapsamlı</h4>
                                        </div>
                                        <div class="card-body">
                                            <div class="ortaYazi">
                                                İstediğiniz Kadar Sınıf Ekleyin
                                            </div>

                                            <div class="w-100 paket-icon mt-4" style="height: 60px;">
                                                <img src="{{asset('images/Shield-kapsamli.svg')}}" />
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <span class="d-block w-100 text-center" style="font-size: 24px;font-weight:700"> <span style="font-size: 15px">Fiyat Seçiminize Göre Belirlenir</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div id="markaniz" style="margin-top: 84px;">
                        <div class="basvuru-title mb-2">Markanız</div>
                        <div class="lead mt-3 mb-5">Markanız sorguladığınız biçimde iyi görünüyor. İsterseniz düzenleyebilirsiniz.</div>

                        <div class="form-row">
                            <div class="col-lg-6">
                                <input type="text" value="Deha Patent" id="marka" name="marka" class="form-control inputBasvuru" />
                                <img class="edit-pencil position-absolute" src="{{asset('images/pencil.svg')}}" />
                            </div>
                        </div>

                    </div>

                    <div id="fatura-bilgileri" style="margin-top: 84px;">
                        <div class="basvuru-title mb-2">Fatura ve Tescil Sahibi Bilgileriniz</div>
                        <div class="lead mt-3 mb-5">Fatura bilgilerinizi giriniz. Marka tescili şirket üzerine olmak zorunda değildir. Bireysel başvuru yapılabilir.</div>
                        <div class="form-row mb-3">
                            <div class="form-group col-md-6 ">
                                <input type="text" class="form-control inputBasvuru" id="inputName" name="inputName" placeholder="Ad Soyad veya Ünvan">
                            </div>
                            <div class="form-group col-md-6 ">
                                <input type="text" class="form-control inputBasvuru" name="inputTCNo" id="inputTCNo" placeholder="TCKN veya Vergi No">
                            </div>
                        </div>

                        <div class="form-row mb-3">
                            <div class="form-group col-md-6">
                                <input type="email" class="form-control inputBasvuru" name="inputEmail" id="inputEmail" placeholder="E-mail">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control inputBasvuru" name="inputTelefon" id="inputTelefon" placeholder="Telefon">
                            </div>
                        </div>

                        <div class="form-row mb-3">
                            <div class="form-group col-md-6">
                                <textarea name="address" class="form-control inputBasvuru" id="inputAdres" placeholder="Adres"></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <span class="font-weight-bolder ml-4">Fatura Türü</span>
                                <div class="ml-4 mt-4">
                                    <div class="default-checkbox custom-control-inline">
                                        <input type="radio" id="bireysel" name="fatura_turu" value="bireysel" class="custom-control-input" checked="checked">
                                        <label class="custom-control-label default-checkbox-label" for="bireysel">Bireysel</label>
                                    </div>
                                    <div class="default-checkbox custom-control-inline">
                                        <input type="radio" id="kurumsal" name="fatura_turu" value="kurumsal" class="custom-control-input">
                                        <label class="custom-control-label default-checkbox-label" for="kurumsal">Kurumsal</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="sinif-alani" style="margin-top:80px;">
                        <div class="d-flex justify-content-between flex-lg-row flex-column">
                            <div class="basvuru-title mb-2">Sınıfınızı/Sektörünüzü Seçin</div>
                            <div class="d-flex align-items-center">
                                <div class="default-checkbox custom-control-inline border-radius">
                                    <input type="radio" id="sektorInput" name="sektorOrSinif" value="sektor" class="custom-control-input" checked="checked">
                                    <label class="custom-control-label default-checkbox-label" for="sektorInput">Sektör ile</label>
                                </div>
                                <div class="default-checkbox custom-control-inline border-radius">
                                    <input type="radio" id="sinifInput" name="sektorOrSinif" value="sinif" class="custom-control-input">
                                    <label class="custom-control-label default-checkbox-label" for="sinifInput">Sınıf ile</label>
                                </div>
                            </div>
                        </div>

                        <div class="d-flex align-items-center flex-lg-row flex-column mt-3 mb-5">
                            <div class="lead ">Sektörünüzü ya da sınıfınızı seçin. Farklı sektörlerde de başvuru yapmanız taklit edilebilirliğinizi zorlaştırır.</div>

                            <span class="position-relative border-radius-20 px-3 py-2" style="width:167px; background: rgba(0, 0, 0, 0.04);font-size: 15px;">
                                <i class="bi bi-info-circle-fill position-absolute" style="font-size: 18px;left: -7px;top: 14px;"></i> Sınıflar ne anlama geliyor?
                            </span>
                        </div>


                        <div class="row " id="sektorler">
                            @foreach($packages as $package)

                                @if($package->is_suggested == 1)
                                    @php $sector_defaults = [];$class_defaults = [];$service_defaults = []; @endphp
                                    @php
                                        $sector_defaults = explode(',', $package->sector_lists);
                                        $class_defaults = explode(',', $package->class_lists);
                                        $service_defaults = explode(',', $package->additional_services);

                                    @endphp
                                @endif
                            @endforeach
                            @foreach($sectors as $sector)
                            <div class="form-group col-md-6 mb-4">
                                <div class="custom-control custom-checkbox basvuru-checkbox">
                                    <input type="checkbox" value="{{ $sector->id }}" name="sectors[]" id="s{{ $sector->id }}" class="custom-control-input sektor-kontrol-input" {!! in_array($sector->id, $sector_defaults) ? 'checked="checked"' : '' !!}>
                                    <label class="custom-control-label sektorCheckbox d-flex align-items-center" for="s{{ $sector->id }}">{{ $sector->name }}</label>
                                </div>
                            </div>
                            @endforeach
                        </div>

                        <div class="row d-none" id="siniflar">
                            @for($c = 1; $c <= 47; $c++)
                            <div class="form-group col-md-2 mb-4">
                                <div class="default-checkbox custom-control-inline sinif-checkbox">
                                    <input type="checkbox"   name="classes[]" id="c{{ $c }}" data-classid="{{ $c }}" value="{{ $c }}.Sınıf" {!! $c == $p_first->class_count ? 'checked="checked"' : '' !!} class="custom-control-input sinif-control-input">
                                    <label class="custom-control-label default-checkbox-label" for="c{{ $c }}">{{ $c }}.Sınıf</label>
                                </div>
                            </div>
                            @endfor
                        </div>
                    </div>

                    <div id="logonuz" style="margin-top: 60px;">
                        <div class="basvuru-title mb-2">Logonuz <span class="font-weight-light" style="font-size: 17px;"><em>(İsteğe Bağlı)</em></span></div>
                        <div class="lead mt-3 mb-5">Logonuzu buradan yükleyebilirsiniz. Logo yüklemek isteğe bağlıdır. Logonuz hazır ise yükleyebilirsiniz.</div>
                        <div class="logo-upload d-flex w-100 flex-column">
                            <span>Logonuzu Yükleyin</span>
                            <i class="bi bi-file-earmark-arrow-up-fill"></i>
                            <span>PNG, JPEG - Max 1 MB</span>
                        </div>
                    </div>
                    <input type="file" id="inputDosya" name="dosyalar[]" maxlength="1" class="asd"  accept="gif|jpg|png|tiff|tif"/>
                    <div id="ilave-hizmetler" style="margin-top: 84px;">
                        <div class="basvuru-title mb-2">İlave Hizmetler (İsteğe Bağlı)</div>
                        <div class="lead mt-3 mb-5">Süreçte işinizi kolaylaştırabilecek diğer hizmetleri buradan ekleyebilirsiniz.</div>

                        <div class="row">
                            @foreach($services as $service)
                            <div class="form-group col-md-6 mb-4">
                                <div class="ilave-checkbox">
                                    <input type="checkbox" data-serviceprice="{{ $service->price }}" data-servicename="{{ $service->name }}" data-serviceid="{{ $service->id }}" id="serv{{ $service->id }}" class="ilave-control-input" {!! in_array($service->id, $service_defaults) ? 'checked="checked"' : '' !!} name="services[]" value="{{ $service->id }}">
                                    <label class="ilave-control-label d-flex flex-column position-relative" for="serv{{ $service->id }}">
                                        <div class="ilavePrice position-absolute text-right d-flex flex-column">
                                            <span class="ilaveNumber">{{ str_replace('.00', '', $service->price) }}<span class="ilaveTlIcon"> ₺</span></span>

                                            <span class="ilaveKdv">+KDV</span>
                                        </div>
                                        <span class="ilaveTitle">{!! $service->name !!}</span>
                                        <span class="ilaveDesc">{!! $service->description !!}</span>
                                    </label>
                                </div>
                            </div>
                            @endforeach

                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="basvuru-ozeti mt-n8 make-me-sticky d-none d-md-block">
                        <div class="card">
                            <div class="card-header">
                                <h4>Başvuru Özetiniz</h4>
                                <div class="ozet-paket-name d-flex justify-content-between flex-row">
                                    @foreach($packages as $package)
                                        <?php
                                        $arr = explode(' ',trim($package->name));
                                        ?>
                                    <div class="pkt{{ $package->id }} {!! $package->is_suggested == 1 ? 'active' : '' !!}" data-id="{{ $package->id }}">
                                        @if($arr)
                                            {{$arr[0]}}
                                        @else
                                            {{$package->name}}
                                        @endif
                                    </div>
                                    @endforeach
                                    <div class="pkt0">Kapsamlı</div>
                                </div>
                            </div>
                            <div class="card-body d-flex flex-column">
                                <div class="d-flex flex-column">
                                    <span class="description">MARKA</span>
                                    <span class="service-name" id="ozet-marka">Deha Patent</span>
                                </div>

                                <div class="d-flex flex-column mt-4">
                                    <span class="description">SEKTÖR VEYA SINIF</span>
                                    <span class="service-name sector-class-name">
                                        @php $class_sag_dizi = [];@endphp
                                        @for($class_sag = 1; $class_sag <= $p_first->class_count; $class_sag++)
                                            @php $class_sag_dizi[] = $class_sag; @endphp
                                        @endfor
                                        {{ implode(',', $class_sag_dizi) }}
                                    </span>
                                </div>


                                <div class="add-services-div">
                                    {!! $suggested_package->getServices('text') !!}
                                </div>

                                <div class="pck-name">
                                    <div class="d-flex flex-row justify-content-between mt-5 mb-3">
                                        <span class="description">{{ $suggested_package->name_mini }}</span>
                                        <span class="price flex-shrink-0">{{ number_format($suggested_package->price ,0,',','.') }} ₺</span>
                                    </div>
                                </div>
                                <div class="ilave-siniflar">
                                    <div class="d-flex flex-row justify-content-between mb-3">
                                        <span class="description">İLAVE SINIFLAR</span>
                                        <span class="price flex-shrink-0">0 ₺</span>
                                    </div>
                                </div>

                                <div class="ilave-hizmetler">
                                    <div class="d-flex flex-row justify-content-between mb-3">
                                        <span class="description">İLAVE HİZMETLER</span>
                                        <span class="price flex-shrink-0">0 ₺</span>
                                    </div>
                                </div>


                                <div class="d-flex flex-row justify-content-between mb-3">
                                    <span class="description">KDV (%18)</span>
                                    <span class="price flex-shrink-0 kdv-div" >{{ number_format($suggested_package->getTotal('KDV', $suggested_package->price, 0, 0 ) ,0,',','.') }} ₺</span>
                                </div>
                                <div class="d-flex flex-row justify-content-between">
                                    <span class="description">TOPLAM ÖDENECEK</span>
                                    <span class="total-price ml-auto flex-shrink-0 toplam-odenecek-div">{{ number_format($suggested_package->getTotal('TOTAL', $suggested_package->price, 0, 0 ) ,0,',','.') }} ₺</span>
                                </div>
                            </div>
                        </div>

                        <div class="d-block d-flex white-shadow-box bg-white mt-n4">
                            <div class="d-flex flex-row">
                                <img class="mr-2" src="{{asset('images/lock-small.svg')}}" style="height: 19px;" />
                                <div class="d-flex flex-column">
                                    <span class="iade">İADE GARANTİSİ!</span>
                                    <span class="iade-info">Red Durumunda Ücretsiz 2. Başvuru</span>
                                </div>
                            </div>
                        </div>

                        <button type="button" class="btn-green-degrade mt-3 text-white font-weight-bolder d-block py-4 odeme-yap" >ÖDEME YAP</button>

                        <div class="white-shadow-box sozlesme-box mt-4 d-flex flex-row">

                            <div class="default-checkbox custom-control-inline">
                                <input type="checkbox" id="sozlesme_onay" name="sozlesme_onay" value="bireysel" class="custom-control-input" checked="checked">
                                <label class="custom-control-label default-checkbox-label" for="sozlesme_onay"></label>
                            </div>
                            <div>
                                Başvurum için gereken <a href="javascript:;"><span class="font-weight-bolder"><u>hizmet sözleşmesini</u></span></a>  okudum, onaylıyorum.
                            </div>

                        </div>

                    </div>

                    <div class="basvuru-ozeti-mobil d-sm-none position-fixed">
                        <div class="box-outer position-relative">
                            <img src="{{asset('images/up.svg')}}" class="basvuru-up-icon position-absolute" />
                            <div class="box-inside d-flex flex-column">
                                <div class="mobile-ozet-baslik text-white">Başvuru Özetiniz</div>
                                <div class="d-flex justify-content-between mt-2">
                                    <div class="d-flex flex-row">
                                        <img class="mr-2" src="{{asset('images/lock-small.svg')}}" />
                                        <div class="d-flex flex-column">
                                            <span class="iade">İADE GARANTİSİ!</span>
                                            <span class="iade-info">Red Durumunda Ücretsiz 2. Başvuru</span>
                                        </div>
                                    </div>
                                    <div class="mobil-price text-white mobil-total">{{ number_format($suggested_package->getTotal('TOTAL', $suggested_package->price, $suggested_package->getPrice('class'), $suggested_package->getPrice('service') ) ,0,',','.') }} ₺</div>
                                </div>

                                <button type="button" class="btn-green-degrade mt-3 text-white font-weight-bolder d-block odeme-yap">ÖDEME YAP</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="current_packet" id="current_packet" value="{{ $suggested_package->id }}">
        <input type="hidden" name="gorsel_adeti" id="gorsel_adeti" value="{{ $p_first->gorsel_adeti }}">
            <input type="hidden" name="current_service_price" id="current_service_price" value="0">
            <input type="hidden" name="main_current" id="main_current" value="{{ $suggested_package->id }}">
            <input type="hidden" name="class_count" id="class_count" value="1">
            <input type="hidden" name="as_count" id="as_count" value="0">
            <input type="hidden" name="sinif_ucreti" id="sinif_ucreti" value="200">
            <input type="hidden" name="ilave_sinif_ucretleri" id="ilave_sinif_ucretleri" value="0">
            <input type="hidden" name="ilave_hizmet_ucretleri" id="ilave_hizmet_ucretleri" value="0">
        <input type="hidden" name="basvuru_ucreti" id="basvuru_ucreti" value="{{ $p_first->price }}">
        <input type="hidden" name="kdv" id="kdv" value="{{ number_format(($p_first->price * 0.18) ,1,'.',',') }}">
        <input type="hidden" name="total" id="total" value="{{ number_format(($p_first->price * 0.18) + $p_first->price ,1,'.',',') }}">
            <input type="hidden" name="basvuru_turu" id="basvuru_turu" value="basvuru">
    </section>
    {!! Form::close() !!}
@endsection

@section('css')


@endsection

@section('js')
    <script src="{{asset('assets/js/jquery.MultiFile.min.js')}}" type="text/javascript" language="javascript"></script>
    <script src="{{asset('js/sweetalert2.min.js')}}"></script>

    <script>
        $('.odeme-yap').on('click', function() {
            if (ValidateSearchParameters() === true) {
                $('#admin-form').submit();
            }
        });
        function ValidateSearchParameters() {


            let searchWord = $("#marka").val();

            if(searchWord.length == 3){
                searchWord = searchWord + ' ';
            }
            if (searchWord.length <= 3 || searchWord.length > 50) {
                swal({
                    title: '',
                    text: "Markanız en az 4 en çok 50 karakter uzunluğunda olmalıdır.",
                    type: 'warning',
                    confirmButtFonText: 'Tamam'
                });
                return false;
            }

            searchWord = $("#inputName").val();

            if(searchWord.length == 1){
                searchWord = searchWord + ' ';
            }
            if (searchWord.length <= 2) {
                swal({
                    title: '',
                    text: "Ad soyad veya ünvanınızın girilmesi gereklidir.",
                    type: 'warning',
                    confirmButtFonText: 'Tamam'
                });
                return false;
            }

            searchWord = $("#inputAdres").val();

            if(searchWord.length == 1){
                searchWord = searchWord + ' ';
            }
            if (searchWord.length <= 2) {
                swal({
                    title: '',
                    text: "Adres alanı girilmesi gereklidir.",
                    type: 'warning',
                    confirmButtFonText: 'Tamam'
                });
                return false;
            }

            let tel = $('#inputTCNo').val();

            if (tel.length != 11) {
                swal({
                    title: '',
                    text: "Eksik veya hatalı T.C. numarası girildi.",
                    type: 'warning',
                    confirmButtonText: 'Tamam'
                });
                return false;
            }
            searchWord = $("#inputEmail").val();
            if(searchWord.length == 1){
                searchWord = searchWord + ' ';
            }
            if (searchWord.length <= 2) {
                swal({
                    title: '',
                    text: "E-posta bilgisi girilmesi gereklidir.",
                    type: 'warning',
                    confirmButtFonText: 'Tamam'
                });
                return false;
            }

            searchWord = $("#inputTelefon").val();

            if (searchWord.length === 0) {
                swal({
                    title: '',
                    text: "Telefon numarası bilgisi girilmesi gereklidir.",
                    type: 'warning',
                    confirmButtFonText: 'Tamam'
                });
                return false;
            }

            return true;
        }
        $('.marka-text').keyup(function () {

            $('#ozet-tecil-marka').html($('.marka-text').val());
        });

        function rightSide(){

            let selected_class_count = $('#class_count').val();
            let mini_name = '';
            let ilave_sinif_text = '';
            let ilave_hizmet_text = '';
            let selected_packet = parseInt($('#current_packet').val());
            console.log('sdsd'+ selected_packet);
            if (selected_packet > 0) {
                $('#basvuru_ucreti').val($('.pkt' + selected_packet).data('pckprice'));
                $('#ilave_hizmet_ucretleri').val(0);
                $('#ilave_sinif_ucretleri').val(0);
                mini_name = '<div class="pck-name"><div class="d-flex flex-row justify-content-between mt-5 mb-3"><span class="description pck-mini-name">'+$('.pkt' + selected_packet).data('pckname')+'</span><span class="price flex-shrink-0">'+$('.pkt' + selected_packet).data('pckprice')+' ₺</span></div></div>';

            } else {
                mini_name = '';
                $('#basvuru_ucreti').val(0);
            }

            if (selected_packet == 0) {
                let ilave_s_ucreti = parseFloat(selected_class_count) * parseFloat($('#sinif_ucreti').val());
                $('#ilave_sinif_ucretleri').val(ilave_s_ucreti);
                ilave_sinif_text = '<div class="d-flex flex-row justify-content-between mb-3"><span class="description">İLAVE SINIFLAR</span><span class="price flex-shrink-0">'+ ilave_s_ucreti +' ₺</span></div>';
                let p_ilave = 0;
                $('#ilave-hizmetler input:checked').each(function() {
                    p_ilave += parseFloat($(this).data('serviceprice'));

                });
                $('#ilave_hizmet_ucretleri').val(p_ilave);
                ilave_hizmet_text = '<div class="d-flex flex-row justify-content-between mb-3"><span class="description">İLAVE HİZMETLER</span><span class="price flex-shrink-0">'+ p_ilave +' ₺</span></div>';


            }
            //pck-mini-name
            $('.pck-name').html(mini_name);
            $('.ilave-siniflar').html(ilave_sinif_text);
            $('.ilave-hizmetler').html(ilave_hizmet_text);

            //ilave-siniflar



            if (selected_class_count > 0) {
                //sector-class-name
                let class_text = '';
                $('.sinif-checkbox input:checked').each(function() {
                    //2.Sınıf
                    let v = $(this).val();
                    let a = v.split('.');
                    class_text += a[0] + ' - ';
                });
                class_text = class_text.substring(0,class_text.length - 1);
                class_text = class_text.substring(0,class_text.length - 1);
                $('.sector-class-name').html(class_text);

            }



            let as_count = $('#as_count').val();

            if (as_count > 0 && selected_packet == 0) {
                //add-services-div in içine
                $('.add-services-div').html('');
                let service_text = '';
                let bottom = '</div>';
                let top = '<div class="d-flex flex-column mt-4 mb-3"><span class="description">İLAVE HİZMETLER</span>';
                $('#ilave-hizmetler input:checked').each(function() {
                    let servicename = $(this).data('servicename');
                    service_text += '<span class="service-name">'+servicename+'</span>';
                });
                $('.add-services-div').append(top + service_text + bottom);

            } else {
                $('.add-services-div').html('');
            }
            let s = parseFloat($('#ilave_sinif_ucretleri').val());
            let h = parseFloat($('#ilave_hizmet_ucretleri').val());
            let b = parseFloat($('#basvuru_ucreti').val());

            let k = (s + h + b) * 0.18;
            $('#kdv').val(k);
            $('#basvuru_ucreti').val(b);
            $('#total').val(s + h + b + k );
            $('.kdv-div').html(k + ' ₺');
            let to = s + h + b + k;
            $('.toplam-odenecek-div').html(to + ' ₺' );
            $('.mobil-total').html(to + ' ₺' );

        }

        function checkValue(value,arr){
            var status = false;

            for(var i=0; i<arr.length; i++){
                var name = arr[i];
                if(name == value){
                    status = true;
                    break;
                }
            }
            return status;
        }

        let as = 0;
        $('#ilave-hizmetler').click(function (){
            as = $('#ilave-hizmetler input:checked').length;
            let serviceprice = 0;
            $('#ilave-hizmetler input:checked').each(function() {
                let servicename = $(this).data('servicename');
                serviceprice += parseFloat($(this).data('serviceprice'));
            });

            $('#as_count').val(as);
            $('#current_service_price').val(serviceprice);
            activeControl();
        });

        let s = 0;
        $('#siniflar').click(function (){
            s = $('#siniflar input:checked').length;
            $('#class_count').val(s);
            activeControl();
        });

        function activeControl() {
            let current_packet = $('#current_packet').val();
            let class_count = parseInt($('#class_count').val());
            let as_count = parseInt($('#as_count').val());
            let current_service_price = parseFloat($('#current_service_price').val());
            let result = 0;
            let classlist = $('.pkt1').data('classlist').toString();
            let servicelist = $('.pkt1').data('servicelist').toString();
            let servicelistprice = $('.pkt1').data('servicelistprice').toString();
            let sinif_adeti = parseInt(classlist);
            let hizmet_adeti = servicelist.split(',').length;
            if (class_count == sinif_adeti && as_count == 0 && current_service_price == 0) {
                $('.paket-dis').removeClass('active');
                $('.pkt1').addClass("active");
                $('#current_packet').val(1);
                result = 1;
                rightSide();
            }

            if (result == 0) {
                servicelistprice = $('.pkt2').data('servicelistprice').toString();
                if (class_count == sinif_adeti && as_count == 1 && current_service_price == servicelistprice) {
                    $('.paket-dis').removeClass('active');
                    $('.pkt2').addClass("active");
                    $('#current_packet').val(2);
                    result = 1;
                    rightSide();
                }
            }

            if (result == 0) {
                console.log('0sinif_adeti:'+sinif_adeti+'|'+class_count+'  hizmet_adeti:'+hizmet_adeti+ '|' + as_count +'  fiyat:'+servicelistprice+'--'+current_service_price);

                $('.paket-dis').removeClass('active');
                $('.pkt0').addClass("active");
                $('#current_packet').val(0)
                rightSide();
            }


        }

        $('.paket-dis').click(function (){

            $('.paket-dis').removeClass('active');
            $(this).addClass('active');



            let packageid = $(this).data('packageid');
            let gorselAdeti = $(this).data('gorseladeti');
            $('#gorsel_adeti').val(gorselAdeti);
            $('#inputDosya').removeClass('multi');
            $('#inputDosya').addClass('multi');

            $('.basvuru-ozeti .card-header .ozet-paket-name div').removeClass('active');
            $('.basvuru-ozeti .card-header .ozet-paket-name .pkt'+packageid).addClass('active');

            $('.multi').MultiFile(gorselAdeti);
            //console.log(packageid + '--' + classlist+ '--'+ servicelist + '');
            if (packageid != 0) {

                let classlist = parseInt($(this).data('classlist').toString());
                let servicelist = $(this).data('servicelist').toString();

                $('#current_packet').val(packageid);

                if (classlist != 0) {
                    $('.sinif-control-input').prop("checked", false);
                    for (i = 1; i <= classlist; i++) {
                        $("#c" + i).prop("checked", true);
                    }

                } else {
                    $('.sinif-control-input').prop("checked", false);
                }

                if (servicelist != '0' || servicelist != 0) {
                    $('.ilave-control-input').prop("checked", false);
                    let services = servicelist.split(',');

                    let si = 0;
                    $.each(services, function (index, value) {
                        if (value > 0) {
                            si++;
                            $("#serv" + value).prop("checked", true);
                        }
                    });
                    $('#as_count').val(si);
                } else {
                    $('#as_count').val(0);
                    $('.ilave-control-input').prop("checked", false);
                }

            } else {
                $('#current_packet').val(0);
            }
            rightSide();
        });

        $('#sektorInput').click(function (){
            $('#sektorler').removeClass('d-none');
            $('#siniflar').addClass('d-none');
        });

        $('#sinifInput').click(function (){
            $('#sektorler').addClass('d-none');
            $('#siniflar').removeClass('d-none');
        });

        $('input[name=marka]').val(JSON.parse(localStorage.getItem('markWord')));
        $('#ozet-marka').html(JSON.parse(localStorage.getItem('markWord')));
        $('#inputName').val(JSON.parse(localStorage.getItem('isim')));
        $('#inputTelefon').val(JSON.parse(localStorage.getItem('telefon')));
        //$('#arastirilan_sektor').html(JSON.parse(localStorage.getItem('sector_name')));


    </script>
@endsection
