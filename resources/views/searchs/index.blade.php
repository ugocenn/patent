@extends('layouts.app')
@section('title', 'Başvuru Aramaları')
@section('content')
    <div class="content ">
        <br/>
        <!-- START CONTAINER FLUID -->
        <div class=" container-fluid   container-fixed-lg">
            <!-- START card -->
            <div class="card card-default">
                <div class="card-header ">
                    <div class="card-title">Başvuru Aramaları
                    </div>
                    <div class="pull-right">

                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                    @include('partials.alerts.error')
                    <div class="table-responsive">
                        <table id="datatable" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Müşteri</th>
                                <th>Telefon</th>
                                <th>Paket Türü</th>
                                <th>Marka</th>
                                <th>Sektör</th>
                                <th>İşlemler</th>
                            </tr>
                            </thead>


                            <tbody>
                            @foreach($searchs as $search)
                            <tr>
                                <td>{{ $search->customer->isim }}</td>
                                <td>{{ $search->customer->tel }}</td>
                                <td>{{ $search->package_type->name }}</td>
                                <td>{{ $search->mark }}</td>
                                <td>{{ $search->sektor_ismi }}</td>
                                <td>
                                    <a href="javascript:void(0);" onclick="return confirmOpsDelete('{{ createHashId($search->id) }}')" role="menuitem" class="text-danger">
                                        <i class="pg-trash"></i> Sil
                                    </a>
                                    {{ Form::open([
                                        'method' => 'DELETE',
                                        'url' => route("searchs.destroy", createHashId($search->id)),
                                        'id' => 'ops-delete-form-' . createHashId($search->id)
                                        ]) }}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END card -->
        </div>
        <!-- END CONTAINER FLUID -->
    </div>

@endsection
