$(document).ready(function(){
    jQuery.extend( jQuery.fn.dataTableExt.oSort, {
        "date-uk-pre": function ( a ) {
            if (a == null || a == "") {
                return 0;
            }
            var ukDatea = a.split('.');
            return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
        },

        "date-uk-asc": function ( a, b ) {
            return ((a < b) ? -1 : ((a > b) ? 1 : 0));
        },

        "date-uk-desc": function ( a, b ) {
            return ((a < b) ? 1 : ((a > b) ? -1 : 0));
        }
    } );
    $("#datatable").DataTable();
    var a=$("#datatable-buttons").DataTable({lengthChange:!1,buttons:["copy","excel","pdf"]});
    $("#key-table").DataTable({keys:!0}),
        $("#responsive-datatable").DataTable(),
        $("#selection-datatable").DataTable({select:{style:"multi"}}),
        a.buttons().container().appendTo("#datatable-buttons_wrapper .col-md-6:eq(0)"),
    $("#datatable-page-index").DataTable({
        columnDefs: [
            { type: 'date-uk', targets: 5 }
        ],
        "order": [[ 5, "desc" ]],
        "pageLength": 50,
    });

    $("#paketler").DataTable({
        "order": [[ 0, "asc" ]],
        "pageLength": 50,
        "columnDefs": [
            { width: 200, targets: 0 }
        ],
    });

});
