<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
    <title>Document</title>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <link href="https://www.patentsorgu.com/js/select2.min.css" rel="stylesheet"/>
    <script src="https://www.patentsorgu.com/js/select2.min.js"></script>
    <script src="https://www.patentsorgu.com/js/sweetalert2.min.js"></script>
    <script src="https://www.google.com/recaptcha/api.js?render=6Leu63oaAAAAALsJsH54fHf8Pr2kGrkbJgxNgIqS"></script>
</head>
<body>
<div class="modal"></div>
    <div id="loader" class="displayNone"></div>
    <div id="reCaptchaDiv" class="displayNone">
        <div id="captcha_container0" class="g-recaptcha-with-ajax"></div>
    </div>
<form id="markaFormOne">
    <div class="marka-form mt-3 mt-lg-0">
        <div class="form-group">
            <input type="text" class="form-control" name="marka" id="txtMarkWord"
                    placeholder="MARKA ADI" value="adidas"/>
        </div>
        <div class="form-group">
            <select class="" name="sektor" id="sektor">
                
                    <option value="1-2-3-4-5-6-7-8-9-10-11-12-13-14-15-16-17-18-19-20-21-22-23-24-25-26-27-28-29-30-31-32-33-34-35-36-37-38-39-40">DENEME</option>
                
            </select>
        </div>
        <div id="example1"></div>
        <button type="submit"
                class="btn btn-block btn-primary g-recaptcha"
                id="btnSearch"
                data-sitekey="6Leu63oaAAAAALsJsH54fHf8Pr2kGrkbJgxNgIqS" 
                data-callback='onSubmit' 
                data-action='submit'
                data-grecaptcha='example1'
                
        >HEMEN SORGULA</button>
    </div>
    </form>

    <script>
        var captchaWidgetId;
        var recaptchaCalls = 0;
        function onSubmit(token) {
            //document.getElementById("markaFormOne").submit();
            //Search(token)
            console.log('Token: '+token)
           
            
            
        }
        $('#btnSearch').on('click', function(e) {
            e.preventDefault();

            grecaptcha.ready(function() {
                grecaptcha.execute(
                    '6Leu63oaAAAAALsJsH54fHf8Pr2kGrkbJgxNgIqS', 
                    {action: 'submit'}).then(function(token) {
                       Search() 
                });
            });

           

           /* captchaWidgetId =    */
            
           /* captchaWidgetId =    grecaptcha.render('grecaptcha-badge', {
                    'sitekey' : '6Leu63oaAAAAALsJsH54fHf8Pr2kGrkbJgxNgIqS',
                    'callback' : Search,
                });*/

               /* $("#captcha_container" + recaptchaCalls).remove();
                recaptchaCalls++;
                $('#reCaptchaDiv').append("<div id=captcha_container" + recaptchaCalls + " class='g-recaptcha-with-ajax'></div>");
                AddModalModeAndShowReCaptchaDiv();
                captchaWidgetId = grecaptcha.render('captcha_container' + recaptchaCalls, {
                    'sitekey': '6LeMER0UAAAAAAwDolCzAx3p8r4gstwibqw8ummv', //don't change this key
                    'callback': Search
                });*/
            
        });
//6Leu63oaAAAAALsJsH54fHf8Pr2kGrkbJgxNgIqS
        function AddModalModeAndShowReCaptchaDiv() {
            $("body").addClass("modalmode");
            $("#reCaptchaDiv").show();
        }

        function RemoveModalModeAndHideReCaptchaDiv() {
            $("body").addClass("modalmode");
            $("#reCaptchaDiv").hide();
        }

        function AddModalModeAndShowLoader() {
            $("body").addClass("modalmode");
            $("#loader").show();
        }

        function RemoveModalModeAndHideLoader() {
            $("body").removeClass("modalmode");
            $("#loader").hide();
        }
        function GetCheckedSectors() {
            var checkedSectors = $("#sektor").val();
            return checkedSectors;
        }

       

        function Search() {
            RemoveModalModeAndHideReCaptchaDiv();
            let markWord = $("#txtMarkWord").val();
            let sectors = GetCheckedSectors();
            let sector_name = $("#sektor option:selected").text();
            let grecaptchaResponse = grecaptcha.getResponse(0);
            console.log('Response: '+grecaptchaResponse)
            $.ajax({

                url: "https://www.markanabak.com.tr/MarkanaBak/api/v1/TrademarkSearch.ashx",
                type: "POST",
                xhrFields: {
                    withCredentials: true
                },
                data: {
                    markWord: markWord,
                    draw: 1,
                    start: 0,
                    length: 30,
                    sectors: sectors,
                    grecaptchaResponse: grecaptchaResponse
                },


                beforeSend: function () {
                    AddModalModeAndShowLoader();
                },
                complete: function () {
                    RemoveModalModeAndHideLoader();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    swal({
                        title: '',
                        text: jqXHR.responseText,
                        type: 'warning',
                        confirmButtonText: 'Tamam'
                    });
                },
                success: function( data )
                {
                    
                    console.log(data)

                },

            });
        }
    </script>
</body>
</html>