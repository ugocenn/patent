<?php

namespace App\Helpers;

use A6digital\Image\DefaultProfileImage;
use App\Models\Employee;
use App\Models\User;
use Illuminate\Http\Request;

class ImageHelper
{
    /**
     * File upload
     *
     * @param \Illuminate\Foundation\Http\FormRequest|Request $request
     * @param string $inputName
     * @param string $subFolder
     *
     * @return null|string
     */
    public function upload($request, $inputName, $subFolder = null)
    {
        if ($request->hasFile($inputName)) {
            $path = $request->file('pic_path')->store(
                'users/'.$request->id, 'uploads'
            );



            return $path;
        }

        return null;
    }

    /**
     * image upload and model update
     *
     * @param Request $request
     * @param string $inputName
     * @param string $subFolder
     * @param $object
     * @param string $oldImage
     *
     * @return string
     */
    public function updatePicture($request, $inputName, $subFolder, $object, $oldImage)
    {
        $file = $request->file($inputName);
        if ($file) {
            $picture = $this->upload($request, $inputName, $subFolder);

            if ($picture) {
                $object->update(['pic_path' => $picture]);
                if ($oldImage) {
                    \File::delete($oldImage);
                }
            }

            return 'has updated';
        } else {
            $object->update(['pic_path' => $oldImage]);

            return 'no updates';
        }
    }

    /**
     * upload and add model pictures
     *
     * @param Request $request
     * @param string $inputName
     * @param string $subFolder
     * @param $object
     *
     * @return string
     */
    public function storePicture($request, $inputName, $subFolder, $object)
    {
        $file = $request->file($inputName);
        if ($file) {
            $picture = $this->upload($request, $inputName, $subFolder);
            if ($picture) {
                $object->update([
                    'pic_path' => $picture,
                ]);

                return 'added picture';
            }
        }

        return null;
    }

    /**
     * Create user logos with initials
     *
     * @param User $user
     *
     * @return bool
     */
    public function createUserImage(User $user)
    {
        $colors = [
            '#324D5C',
            '#46B29D',
            '#F0CA4D',
            '#E37B40',
            '#DE5B49'
        ];

        $slugName = str_slug($user->name);
        $imageFullPath = $user->company_id . '/user/' . $slugName . '.png';

        $randKeys = array_rand($colors, 1);

        $img = DefaultProfileImage::create($user->name, 512, $colors[$randKeys], '#FFF');

        \Storage::disk('uploads')->put($imageFullPath, $img->encode());
        $fullPath = 'uploads/' . $imageFullPath;

        $user->pic_path = $fullPath;
        $user->save();

        return true;
    }

    /**
     * Create employees logos with initials
     *
     * @param Employee $employee
     *
     * @return bool
     */
    public function createEmployeeImage(Employee $employee)
    {
        $colors = [
            '#324D5C',
            '#46B29D',
            '#F0CA4D',
            '#E37B40',
            '#DE5B49'
        ];
        if(strlen($employee->name) < 2) {
            $name = 'Tr';
        } else {
            $name = $employee->name;
        }
        $slugName = str_slug($name);
        $imageFullPath = $employee->company_id . '/user/' . $slugName . '.png';

        $randKeys = array_rand($colors, 1);

        $img = DefaultProfileImage::create($name, 512, $colors[$randKeys], '#FFF');

        \Storage::disk('uploads')->put($imageFullPath, $img->encode());
        $fullPath = 'uploads/' . $imageFullPath;

        $employee->pic_path = $fullPath;
        $employee->save();

        return true;
    }


    public function createCompanyImage($company)
    {
        $colors = [
            '#324D5C',
            '#46B29D',
            '#F0CA4D',
            '#E37B40',
            '#DE5B49'
        ];

        $slugName = str_slug($company->name);
        $imageFullPath = $company->id . '/company/' . $slugName . '.png';

        $randKeys = array_rand($colors, 1);

        $img = DefaultProfileImage::create($company->name, 512, $colors[$randKeys], '#FFF');

        \Storage::disk('uploads')->put($imageFullPath, $img->encode());
        $fullPath = 'uploads/' . $imageFullPath;

        $company->pic_path = $fullPath;
        $company->save();

        return true;
    }
}
