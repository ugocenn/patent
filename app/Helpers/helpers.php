<?php

use App\Models\Department;
use Carbon\Carbon;
use Illuminate\Support\Str;

function egt_turk_Ucword($str){
    $str=egt_turk_kucuk($str);
    $x_arr=explode(" ",$str);$ret="";
    foreach ($x_arr as $k=>$v){
        $ret.=egt_turk_buyuk(mb_substr($v,0,1,'utf-8') ).mb_substr($v,1,strlen($v)-1,'utf-8')." ";
    }
    return trim($ret);
}

function egt_turk_buyuk($str) {
    return mb_strtoupper(str_replace(array('ı','ğ','ü','ş','i','ö','ç'), array('I','Ğ','Ü','Ş','İ','Ö','Ç'), $str), 'utf-8');
}


function egt_turk_kucuk($str) {
    return mb_strtolower(str_replace(array('I','Ğ','Ü','Ş','İ','Ö','Ç'), array('ı','ğ','ü','ş','i','ö','ç'), $str), 'utf-8');
}

function turkce_duzelt($veri){
    $bul = array('ç', 'ğ', 'ı', 'i', 'ö', 'ş', 'ü');
    $deg = array('Ç', 'Ğ', 'I', 'İ', 'Ö', 'Ş', 'Ü');
    return str_replace($bul, $deg, $veri);
}

function tr_converter($uri) {
    $uri = str_replace ("ç","c",$uri);
    $uri = str_replace ("ğ","g",$uri);
    $uri = str_replace ("İ","I",$uri);
    $uri = str_replace ("ı","i",$uri);
    $uri = str_replace ("ş","s",$uri);
    $uri = str_replace ("ö","o",$uri);
    $uri = str_replace ("ü","u",$uri);
    $uri = str_replace ("Ü","U",$uri);
    $uri = str_replace ("Ç","c",$uri);
    $uri = str_replace ("!","",$uri);
    $uri = str_replace ("-","",$uri);
    $uri = str_replace (":)","",$uri);
    $uri = str_replace (")","",$uri);
    $uri = str_replace ("(","",$uri);
    $uri = str_replace (",","_",$uri);
    $uri = str_replace (".","",$uri);
    $uri = str_replace ("Ğ","g",$uri);
    $uri = str_replace ("Ş","S",$uri);
    $uri = str_replace ("Ö","O",$uri);
    $uri = str_replace (" ","_",$uri);
    $uri = str_replace ("'","",$uri);
    $uri = str_replace ("/","",$uri);
    $uri = str_replace ("__","_",$uri);
    $uri = str_replace("`","",$uri);
    $uri = str_replace ("ç","c",$uri);
    $uri = str_replace("&","",$uri);
    $uri = str_replace("%","",$uri);
    $uri = str_replace("'","",$uri);
    $uri = strtolower($uri);
    return $uri;
}
function ezey_get_dateformat($str,$type,$only='tarih_saat'){
    if(is_null($str))
        return null;
    if(empty($str))
        return null;
    if($type=="toThem"){
        //14/01/2006 gelecek
        //2006-01-14 dönecek
        $str=trim($str);
        $str_dizi = explode("/", $str);
        $str_gun=$str_dizi[0];//0-gun;1-ay;2-yil
        $str_ay= $str_dizi[1];
        $str_yil=$str_dizi[2];
        $str=$str_yil."-".$str_ay."-".$str_gun;
        return $str;
    }
    if($type=="toOur"){
        //2006-01-14 gelecek
        //14-01-2006 dönecek

        if(strstr($str,' ')){
            $str=explode(' ',$str);
            $str=trim($str[0]);
        }
        else{
            $str=trim($str);
        }

        $str_dizi = explode("-", $str);
        $str_yil=$str_dizi[0];//0-yil;1-ay;2-gun
        $str_ay= $str_dizi[1];
        $str_gun=$str_dizi[2];
        $str=$str_gun."/".$str_ay."/".$str_yil;
        return $str;
    }
    if($type=="LoginTime"){
        //2006-01-14 12:45:48 gelecek
        //14-01-2006 12:45:48 dönecek


        $str=trim($str);
        $str_dizi = explode(" ", $str);
        $str_tarih=ezey_get_dateformat($str_dizi[0],"toOur");//0-tarihl;1-saat;
        $str_saat= $str_dizi[1];
        if($only=="tarih_saat")
            $str=$str_tarih." ".$str_saat;
        else
            $str=$str_tarih;
        return $str;
    }
}


if (! function_exists('snake_case')) {
    /**
     * Convert a string to snake case.
     *
     * @param  string  $value
     * @param  string  $delimiter
     * @return string
     */
    function snake_case($value, $delimiter = '_')
    {
        return Str::snake($value, $delimiter);
    }
}


if (! function_exists('str_random')) {
    /**
     * Generate a more truly "random" alpha-numeric string.
     *
     * @param  int  $length
     * @return string
     *
     * @throws \RuntimeException
     */
    function str_random($length = 16)
    {
        return Str::random($length);
    }
}

if (! function_exists('str_slug')) {
    /**
     * Generate a URL friendly "slug" from a given string.
     *
     * @param  string  $title
     * @param  string  $separator
     * @param  string  $language
     * @return string
     */
    function str_slug($title, $separator = '-', $language = 'en')
    {
        return Str::slug($title, $separator, $language);
    }
}
if (! function_exists('camel_case')) {
    /**
     * Convert a value to camel case.
     *
     * @param  string  $value
     * @return string
     */
    function camel_case($value)
    {
        return Str::camel($value);
    }
}
if (! function_exists('str_plural')) {
    /**
     * Get the plural form of an English word.
     *
     * @param  string  $value
     * @param  int     $count
     * @return string
     */
    function str_plural($value, $count = 2)
    {
        return Str::plural($value, $count);
    }
}


if (! function_exists('str_singular')) {
    /**
     * Get the singular form of an English word.
     *
     * @param  string  $value
     * @return string
     */
    function str_singular($value)
    {
        return Str::singular($value);
    }
}

if (!function_exists('decimalFormatMysql')) {
    function decimalFormatMysql($string)
    {
        if (strlen($string) == 0)
            return 0;
        if ($string === '')
            return 0;
        $string = trim(str_replace("TL", "", $string));
        $delimiters = ['.', ','];
        $format = ".";
        $return = "";
        $ready = str_replace($delimiters, $delimiters[0], $string);
        $launch = explode($delimiters[0], $ready);
        for ($i = 0; $i <= (count($launch) - 2); $i++) {
            $return .= trim($launch[$i]);
        }
        $return .= $format . trim($launch[count($launch) - 1]);
        return round($return, 2);
    }
}


if (!function_exists('getFullMonthName')) {
    function getFullMonthName($month_id)
    {

        //'Oca', 'Şub', 'Mar', 'Nis', 'May', 'Haz', 'Tem', 'Ağs', 'Eyl', 'Ekm', 'Kas', 'Ara'
        if ($month_id == 1 || $month_id == '01' || $month_id == 'January' || $month_id == 'Jan') {
            return 'Ocak';
        } else if ($month_id == '2' || $month_id == '02' || $month_id == 'February' || $month_id == 'Feb') {
            return 'Şubat';
        } else if ($month_id == '3' || $month_id == '03' || $month_id == 'March' || $month_id == 'Mar') {
            return 'Mart';
        } else if ($month_id == '4' || $month_id == '04' || $month_id == 'April' || $month_id == 'Apr') {
            return 'Nisan';
        } else if ($month_id == '5' || $month_id == '05' || $month_id == 'May' || $month_id == 'May') {
            return 'Mayıs';
        } else if ($month_id == '6' || $month_id == '06' || $month_id == 'June' || $month_id == 'Jun') {
            return 'Haziran';
        } else if ($month_id == '7' || $month_id == '07' || $month_id == 'July' || $month_id == 'Jul') {
            return 'Temmuz';
        } else if ($month_id == '8' || $month_id == '08' || $month_id == 'August' || $month_id == 'Aug') {
            return 'Ağustos';
        } else if ($month_id == '9' || $month_id == '09' || $month_id == 'September' || $month_id == 'Sep') {
            return 'Eylül';
        } else if ($month_id == '10' || $month_id == '10' || $month_id == 'October' || $month_id == 'Oct') {
            return 'Ekim';
        } else if ($month_id == '11' || $month_id == '11' || $month_id == 'November' || $month_id == 'Nov') {
            return 'Kasım';
        } else if ($month_id == '12' || $month_id == '12' || $month_id == 'December' || $month_id == 'Dec') {
            return 'Aralık';
        } else {
            return '';
        }
    }
}

function pdf_sayfa_no ($pdf)
{
    if ( isset($pdf) ) {
        $size = 9;
        $y = $pdf->get_height() - 17;
        $x = $pdf->get_width() - 70;
        $pdf->page_text($x, $y, 'Sayfa:{PAGE_NUM}/{PAGE_COUNT}', '', $size);
    }
}

function remove_last_string($text,$string){
    if (substr($text, -1) == $string) {
        $text = substr($text, 0, strlen($text)-1);
    }
    return $text;
}


function clearText($str)
{
    $table = MB_CONVERT_ENCODING($str, "UTF-8", "ISO-8859-9");
    $table = str_replace("ý", "ı", $table);
    $table = str_replace("\r", "", $table);
    $table = str_replace("\n", "", $table);
    $table = str_replace("    ", "", $table);
    return $table;
}

if (!function_exists('createHashId')) {
    /**
     * Create hash id
     *
     * @param $id
     *
     * @return string
     */
    function createHashId($id)
    {
        return Vinkla\Hashids\Facades\Hashids::encode($id);
    }
}


if (!function_exists('getEmployeeName')) {
    function getEmployeeName($id)
    {
        $result =  \App\Models\Employee::find($id);
       if ($result) {
           return $result->name;
       }
       return '';
    }
}



if (!function_exists('getAllDepartmentByName')) {
    function getAllDepartmentByName($ids)
    {
        $result =  \App\Models\Department::where('company_id', auth()->user()->company_id)->whereIn('id', $ids)->pluck('name')->toArray();
        return implode(', ', $result);
    }
}

if (!function_exists('getAllAplicant')) {
    function getAllAplicant($ids)
    {
        $ids_array = explode(',', $ids);
        return  \App\Models\Employee::where('company_id', auth()->user()->company_id)->whereIn('id', $ids_array)->get();

    }
}


if (!function_exists('getRemainingTime')) {
    function getRemainingTime($end_date)
    {
        Carbon::setLocale('tr');
        $bitis = new Carbon($end_date);
        $suan = Carbon::now(config('app.tz'));
        $fark = date_diff($suan, $bitis);
        $text = "";
        if ($fark->y > 0)
            $text = $fark->y.' yıl ';
        if ($fark->m > 0)
            $text .= $fark->m.' ay ';
        if ($fark->d > 0)
            $text .= $fark->d.' gün ';
        return $text;

    }


}
if (!function_exists('getRehbers')) {
    function getRehbers($location = 2, $cat = 1, $limit = 0)
    {
        $category = \App\Models\Category::where('slug', $cat)->first();
        if (!$category) {
            return null;
        }
        $limit = $limit == 0 ? 10000 : $limit;
        if ($location == 2) {
            return \App\Models\Page::where('category_id', $category->id)->where('active',1)->orderBy('id', 'ASC')->take($limit)->get();
        }
        return \App\Models\Page::where('category_id', $category->id)->where('active',1)->orderBy('id', 'ASC')->where('location', $location)->take($limit)->get();
    }
}

if (!function_exists('getPackageTypes')) {
    function getPackageTypes($package_type = null)
    {
        if (!$package_type) {
            $query = \App\Models\PackageType::where('active',1)->get();
            return $query;
        } else {
            $query = \App\Models\PackageType::find($package_type);
            $package = \App\Models\Package::where('package_type_id', $query->id)->first();
            return $package->price;
        }
    }
}

if (!function_exists('getPackageTypesOldPrice')) {
    function getPackageTypesOldPrice($package_type = null)
    {
        if (!$package_type) {
            $query = \App\Models\PackageType::where('active',1)->get();
            return $query;
        } else {
            $query = \App\Models\PackageType::find($package_type);
            $package = \App\Models\Package::where('package_type_id', $query->id)->first();
            return $package->old_price;
        }
    }
}
if (!function_exists('getPackageKampanyaText')) {
    function getPackageKampanyaText($package_type = null)
    {
        if (!$package_type) {
            $query = \App\Models\PackageType::where('active',1)->get();
            return $query;
        } else {
            $query = \App\Models\PackageType::find($package_type);
            $package = \App\Models\Package::where('package_type_id', $query->id)->first();
            return $package->kampanya_text;
        }
    }
}





