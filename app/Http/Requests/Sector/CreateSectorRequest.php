<?php

namespace App\Http\Requests\Sector;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateCategoryRequest
 * @package App\Http\Requests\Category
 */
class CreateSectorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',

        ];
    }

    public function messages()
    {
        $messages['name.required'] = 'Sektör adı alanı zorunludur.';

        return $messages;
    }


}
