<?php

namespace App\Http\Requests\Company;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateUserRequest
 * @package App\Http\Requests\User
 */
class CreateCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'company_title' => 'required',
            'tax_number' => 'required',
            'trade_number' => 'required',
            'iskur_no' => 'required',
            'email' => 'required|email',
            'sgk_numbers' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'official_name' => 'required',
            'official_tc' => 'required',
            'official_birth_place' => 'required',
            'official_birth_date' => 'required',
        ];
    }

    public function messages()
    {
        $messages['name.required'] = 'Firma kısa adı alanı zorunludur.';
        $messages['company_title.required'] = 'Firma yasal ünvanı alanı zorunludur.';
        $messages['tax_number.required'] = 'Firma vergi no alanı zorunludur.';
        $messages['trade_number.required'] = 'Firma meslek odası sicil no alanı zorunludur.';
        $messages['iskur_no.required'] = 'Firma İŞKUR numarası alanı zorunludur.';
        $messages['email.required'] = 'Firma e-posta alanı zorunludur.';
        $messages['email.email'] = 'Firma e-posta alanı hatalı girildi.';
        $messages['sgk_numbers.required'] = 'Firma SGK numaraları alanı zorunludur.';
        $messages['address.required'] = 'Firma iletişim adresi alanı zorunludur.';
        $messages['phone.required'] = 'Firma iletişim telefonu alanı zorunludur.';
        $messages['official_name.required'] = 'Firma yetkili adı soyadı alanı zorunludur.';
        $messages['official_tc.required'] = 'Firma yetkili T.C. alanı zorunludur.';
        $messages['official_birth_place.required'] = 'Firma yetkili doğum yeri alanı zorunludur.';
        $messages['official_name.required'] = 'Firma yetkili doğum tarihi alanı zorunludur.';

        return $messages;
    }


}
