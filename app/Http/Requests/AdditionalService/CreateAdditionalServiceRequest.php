<?php

namespace App\Http\Requests\AdditionalService;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateCategoryRequest
 * @package App\Http\Requests\Category
 */
class CreateAdditionalServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'price' => 'required',

        ];
    }

    public function messages()
    {
        $messages['name.required'] = 'Ek Hizmet adı alanı zorunludur.';
        $messages['price.required'] = 'Ücret bilgisi alanı zorunludur.';

        return $messages;
    }


}
