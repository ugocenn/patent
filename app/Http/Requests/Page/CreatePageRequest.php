<?php

namespace App\Http\Requests\Page;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateUserRequest
 * @package App\Http\Requests\User
 */
class CreatePageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'slug' => 'required',
        ];
    }

    public function messages()
    {
        $messages['title.required'] = 'Sayfa başlık alanı zorunludur.';
        $messages['slug.required'] = 'Sayfa bağlantı alanı zorunludur.';
        $messages['category_id.required'] = 'Kategori seçimi zorunludur.';

        return $messages;
    }


}
