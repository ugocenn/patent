<?php

namespace App\Http\Requests\PackageType;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateCategoryRequest
 * @package App\Http\Requests\Category
 */
class CreatePackageTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'slug' => 'required',

        ];
    }

    public function messages()
    {
        $messages['name.required'] = 'Pajet türü adı alanı zorunludur.';
        $messages['slıg.required'] = 'Paket türü bağlantı adı alanı zorunludur.';

        return $messages;
    }


}
