<?php

namespace App\Http\Requests\Category;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateCategoryRequest
 * @package App\Http\Requests\Category
 */
class CreateCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'title' => 'required',

        ];
    }

    public function messages()
    {
        $messages['name.required'] = 'Kategori adı alanı zorunludur.';
        $messages['title.required'] = 'Kategori başlık alanı zorunludur.';

        return $messages;
    }


}
