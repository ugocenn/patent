<?php

namespace App\Http\Requests\Customer;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateCustomerRequest
 * @package App\Http\Requests\User
 */
class CreateCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'isim' => 'required',
        ];
    }

    public function messages()
    {
        $messages['isim.required'] = 'Müşteri isim alanı zorunludur.';

        return $messages;
    }


}
