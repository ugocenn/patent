<?php

namespace App\Http\Requests\Package;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateCategoryRequest
 * @package App\Http\Requests\Category
 */
class CreatePackageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'price' => 'required',
            'package_type_id' => 'required',

        ];
    }

    public function messages()
    {
        $messages['name.required'] = 'Paket adı alanı zorunludur.';
        $messages['price.required'] = 'Paket ücreti alanı zorunludur.';
        $messages['package_type_id.required'] = 'Paket türü alanı zorunludur.';

        return $messages;
    }


}
