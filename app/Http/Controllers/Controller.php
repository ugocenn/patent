<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Models\Option;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function __construct()
    {
        view()->share('option',$option = Option::where('id',1)->first());
        //view()->share('categories',$categories = Category::where('ust_id', '!=' , 0)->get());
        //view()->share('aktifPeriod',$aktifPeriod = Period::where('status', 1)->first());
    }
}
