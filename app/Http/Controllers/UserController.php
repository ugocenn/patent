<?php

namespace App\Http\Controllers;

use App\Events\UserRegistered;
use App\Helpers\HashingSlug;
use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Models\User;
use App\Services\User\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    protected $userService;
    protected $user;

    public function __construct(UserService $userService, User $user){
        $this->userService = $userService;
        $this->user = $user;
    }

    public function index()
    {
        return $this->userService->indexView('users.index', $this->userService->getUsers());
    }

    public function create()
    {
        if (auth()->user()->role == 'superadmin') {
            $roles = ['superadmin' => 'Süper Admin', 'admin' => 'Yönetici', 'teamleader' => 'Takım Lideri'];
        } else {
            $roles = ['admin' => 'Yönetici', 'teamleader' => 'Takım Lideri'];
        }

        return view('users.create', [
            'roles' => $roles
        ]);
    }

    public function store(CreateUserRequest $request)
    {
        $password = $request->password;
        $tb = $this->userService->storeUser($request);
        try {
            // Fire event to send welcome email
            event(new UserRegistered($tb, $password));
        } catch(\Exception $ex) {
            Log::info('Bu adrese hoşgeldin e-postası gönderilirken hata oluştu: '.$tb->email);
        }

        return redirect(route('users.index'))->with('success', 'Sisteme kullanıcı başarılı bir şekilde eklenerek giriş bilgileri e-posta adresine gönderilmiştir...');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = HashingSlug::decodeHash($id);
        $user = $this->user->find($id);

        if (!$user) {
            return back()->with('warning', 'İlgili kayıt bulunamamıştır. Lütfen kontrol ederek tekrar deneyiniz.');
        }

        if (auth()->user()->role == 'superadmin') {
            $roles = ['superadmin' => 'Süper Admin', 'admin' => 'Yönetici', 'teamleader' => 'Takım Lideri'];
        } else {
            $roles = ['admin' => 'Yönetici', 'teamleader' => 'Takım Lideri'];
        }

        return view('users.edit', [
            'user' => $user,
            'roles' => $roles
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateUserRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update($id, UpdateUserRequest $request)
    {
        $id = HashingSlug::decodeHash($id);

        $tb = $this->user->find($id);
        $tb->name = $request->name;
        $tb->email = (!empty($request->email)) ? $request->email : '';
        $tb->role = (!empty($request->role)) ? $request->role : '';
        $tb->active = (!empty($request->active)) ? $request->active : '';
        if (!empty($request->pic_path)) {
            $tb->pic_path = $request->pic_path;
        }

        if ($tb->save()) {
            try{

            } catch(\Exception $ex) {
                Log::info('Failed to update User information, Id: '.$tb->id. 'err:'.$ex->getMessage());
            }
        }

        return redirect(route('users.index'))->with('success', 'Kullanıcı başarılı bir şekilde güncelleştirilmiştir.');
    }
}
