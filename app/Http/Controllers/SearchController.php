<?php

namespace App\Http\Controllers;


use App\Helpers\HashingSlug;
use App\Http\Requests\Search\CreateSearchRequest;
use App\Models\Basket;
use App\Models\Customer;
use App\Models\Search;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Revolution\Google\Sheets\Facades\Sheets;

class SearchController extends Controller
{




    public function index()
    {
        $searchs = Search::orderBy('mark', 'ASC')->get();
        return view('searchs.index', [
            'searchs' => $searchs
        ]);
    }

//https://docs.google.com/spreadsheets/d/1rF53Ynfp80-NthXv94kkcgAfyFqt94kvlZUwBtyQqt4/edit?usp=sharing
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateSearchRequest $request)
    {


    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateSearchRequest $request, $id)
    {


    }

    public function destroy($id) {

        $id = HashingSlug::decodeHash($id);
        $search = Search::find($id);
        if (!$search) {
            return back()->with('warning', 'İlgili kayıt bulunamamıştır. Lütfen kontrol ederek tekrar deneyiniz.');
        }

        $basket = Basket::where('search_id', $search->id)->update(['search_id' => null]);

        $search->forceDelete();

        return redirect(route('searchs.index'))->with('success', 'Kayıt başarılı bir şekilde silinmiştir.');
    }
}
