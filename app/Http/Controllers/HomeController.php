<?php

namespace App\Http\Controllers;

use App\Helpers\HashingSlug;
use App\Models\Category;
use App\Models\Employee;
use App\Models\PackageType;
use App\Models\Search;
use App\Models\Sector;
use App\Models\Video;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use Revolution\Google\Sheets\Facades\Sheets;
use Vinkla\Hashids\Facades\Hashids;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $sectors = Sector::orderby('sort', 'ASC')->get();
        return view('index', compact('sectors'));
    }
    public function marka_sonuc(Request $request)
    {
        $sectors = Sector::orderby('sort', 'ASC')->get();
        $package = PackageType::find(1);
        $marka = $request->marka;
        $searchID = Hashids::decode($request->search_id)[0];
        $visitorID = Hashids::decode($request->visitor)[0];
        $search_id = $request->search_id;
        $get_search = Search::findorFail($searchID);
        $visitor = $request->visitor;
        $video = Video::find(1);

        $view = $get_search->sonuc_sayisi > 0 ? 'marka_sonuc_coklu' : 'marka_sonuc_uygun';

        return view($view, compact('sectors','package','search_id','visitor','marka','video'));
    }
    /*public function marka_sonuc_uygun(Request $request){
        $sectors = Sector::orderby('sort', 'ASC')->get();
        $package = PackageType::find(1);
        return view('marka_sonuc_uygun', compact('sectors','package'));

    }*/



    public function welcome()
    {

        return view('welcome');
    }

    public function rehberler(){
        $rehberler_cat = Category::where('id',2)->first();
        return view('rehberler',compact('rehberler_cat'));
    }

    public function __invoke(Request $request)
    {
        /**
         * Service Account demo.
         */

        $sheets = Sheets::spreadsheet(env('POST_SPREADSHEET_ID'))

            ->sheetById(env('POST_SPREADSHEET_ID'))

            ->all();

        dd($sheets);


        //$header = $sheets->pull(0);


        $posts = array();

        foreach ($sheets AS $data) {

            $posts[] = array(

                'mark' => $data[0],

                'sector' => $data[1],

                'name' => $data[2],

                'phone' => $data[3],

            );

        }

        return view('welcome')->with(compact('posts'));
    }


}
