<?php

namespace App\Http\Controllers;

use App\Helpers\HashingSlug;
use App\Http\Requests\ProductClass\CreateProductClassRequest;
use App\Models\ProductClass;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductClassController extends Controller
{


    public function index()
    {
        $product_classes = ProductClass::orderBy('name', 'ASC')->get();

        return view('product_classes.index', [
            'product_classes' => $product_classes
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('product_classes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProductClassRequest $request)
    {
        try {
            $product_class = new ProductClass();
            $product_class->name = $request->name;
            $product_class->price = str_replace(',', '', $request->price);
            $product_class->save();
            return redirect(route('product-classes.index'))->with('success', 'Sınıf bilgileri başarılı bir şekilde sisteme eklenmiştir.');
        } catch(\Exception $ex) {
            return redirect(route('product-classes.index'))->with('danger', ' Güncelleme işlemi esnasında bir hata oluştu. Lütfen tekrar deneyiniz. <br/><pre> Hata:'.$ex->getMessage(). '</pre>');
        }


    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd('ss');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = HashingSlug::decodeHash($id);
        $product_class = ProductClass::find($id);
        if (!$product_class) {
            return back()->with('warning', 'İlgili kayıt bulunamamıştır. Lütfen kontrol ederek tekrar deneyiniz.');
        }

        return view('product_classes.edit', compact('product_class'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateProductClassRequest $request, $id)
    {

        $id = HashingSlug::decodeHash($id);
        $product_class = ProductClass::find($id);

        if (!$product_class) {
            return back()->with('warning', 'İlgili kayıt bulunamamıştır. Lütfen kontrol ederek tekrar deneyiniz.');
        }

        try {
            $product_class->name = $request->name;
            $product_class->price = str_replace(',', '', $request->price);
            $product_class->save();
        } catch(\Exception $ex) {
            return redirect(route('product-classes.index'))->with('danger', ' Güncelleme işlemi esnasında bir hata oluştu. Lütfen tekrar deneyiniz. <br/><pre> Hata:'.$ex->getMessage(). '</pre>');
        }
        return redirect(route('product-classes.index'))->with('success', 'Sınıf bilgileri başarılı bir şekilde sisteme güncellenmiştir.');


    }

    public function destroy($id) {

        $id = HashingSlug::decodeHash($id);
        $product_class = ProductClass::find($id);
        if (!$product_class) {
            return back()->with('warning', 'İlgili kayıt bulunamamıştır. Lütfen kontrol ederek tekrar deneyiniz.');
        }

        $product_class->forceDelete();


        return redirect(route('product-classes.index'))->with('success', 'Kayıt başarılı bir şekilde silinmiştir.');
    }
}
