<?php

namespace App\Http\Controllers;

use App\Helpers\HashingSlug;
use App\Http\Requests\Company\CreateApplicationRequest;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CompanyController extends Controller
{


    public function index()
    {
        $companies = Company::where('id', auth()->user()->company_id)->orderBy('name', 'ASC')->get();

        return view('companies.index', [
            'companies' => $companies
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateApplicationRequest $request)
    {
        dd($request);


    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new FaqResource(Faq::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = HashingSlug::decodeHash($id);
        $company = Company::find($id);
        if (!$company) {
            return back()->with('warning', 'İlgili kayıt bulunamamıştır. Lütfen kontrol ederek tekrar deneyiniz.');
        }

        return view('companies.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateApplicationRequest $request, $id)
    {

        $id = HashingSlug::decodeHash($id);
        $company = Company::find($id);

        if (!$company) {
            return back()->with('warning', 'İlgili kayıt bulunamamıştır. Lütfen kontrol ederek tekrar deneyiniz.');
        }

        try {
            $company->name = $request->name;
            $company->company_title = $request->company_title;
            $company->tax_number = $request->tax_number;
            $company->trade_number = $request->trade_number;
            $company->iskur_no = $request->iskur_no;
            $company->email = $request->email;
            $company->sgk_numbers = $request->sgk_numbers;
            $company->address = $request->address;
            $company->phone = $request->phone;

            $company->official_name = $request->official_name;
            $company->official_tc = $request->official_tc;
            $company->official_birth_place = $request->official_birth_place;
            $company->official_birth_date = ezey_get_dateformat($request->official_birth_date, 'toThem');
            $company->save();
        } catch(\Exception $ex) {
            return redirect(route('companies.index'))->with('danger', ' Güncelleme işlemi esnasında bir hata oluştu. Lütfen tekrar deneyiniz. <br/><pre> Hata:'.$ex->getMessage(). '</pre>');
        }
        return redirect(route('companies.index'))->with('success', 'Firma bilgileri başarılı bir şekilde sisteme güncellenmiştir.');


    }
}
