<?php

namespace App\Http\Controllers;

use App\Helpers\HashingSlug;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DeleteController extends Controller
{


    public function destroyModal($type, $id)
    {
        $orj = $id;
        $id = HashingSlug::decodeHash($id);

        if ($type == 'lesson') {
            $education_lesson = EducationLesson::find($id);
            $education = Education::find($education_lesson->education_id);
            if (!$education_lesson) {
                return back()->with('danger','İşlem gerçekleştirilemedi. Hatalı kayıt seçimi yapıldı!!!');
            }

            $education_lesson->delete();
            return redirect(route('educations.lesson.index', createHashId($education->id)));
        }
    }


}
