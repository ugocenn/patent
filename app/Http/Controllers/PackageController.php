<?php

namespace App\Http\Controllers;

use App\Helpers\HashingSlug;
use App\Http\Requests\Package\CreatePackageRequest;
use App\Models\AdditionalService;
use App\Models\Package;
use App\Models\PackageType;
use App\Models\ProductClass;
use App\Models\Sector;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PackageController extends Controller
{


    public function index()
    {
        $packages = Package::orderBy('name', 'ASC')->get();

        return view('packages.index', [
            'packages' => $packages
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $services = AdditionalService::orderBy('name', 'ASC')->pluck('name', 'id');
        $package_types = PackageType::orderBy('name', 'ASC')->pluck('name', 'id');
        $sectors = Sector::orderBy('name', 'ASC')->pluck('name', 'id');
        return view('packages.create', compact('services', 'sectors', 'package_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePackageRequest $request)
    {

        try {

            $package = new Package();
            $package->name = $request->name;
            $package->gorsel_adeti = $request->gorsel_adeti;
            $package->class_count = $request->class_count;
            $package->package_type_id = $request->package_type_id;
            $package->name_mini = $request->name_mini;
            $package->name_top = $request->name_top;
            $package->inactive_services = $request->inactive_services;
            $package->icon = $request->icon;
            $package->description = $request->description;
            $package->price = str_replace(',', '', $request->price);
            $package->old_price = $request->old_price;
            $package->kampanya_text = $request->kampanya_text;
            $package->active = $request->active;
            $package->is_star = $request->is_star;
            $package->is_suggested = $request->is_suggested;

            $package->sector_lists = isset($request->sector_lists) ? implode(',', $request->sector_lists) : null;

            if(isset($request->additional_services)){
                $servis_dizi = array();
                $services = AdditionalService::get();
                foreach ($services as $service){
                    $servis_dizi[$service->id]=['id'=> $service->id, 'name' =>$service->name, 'short_name' =>$service->short_name, 'price' => $service->price];
                }
                $ek_servisler = array();
                $x = 0;
                foreach ($request->additional_services as $ek){
                    $ek_servisler[$x] = $servis_dizi[$ek];
                    $x++;
                }
            }else{
                $ek_servisler = null;
            }


            $package->additional_services = json_encode($ek_servisler);
            $package->save();
            return redirect(route('packages.index'))->with('success', ' Paket bilgileri başarılı bir şekilde sisteme eklenmiştir.');
        } catch(\Exception $ex) {
            return redirect(route('packages.index'))->with('danger', ' Güncelleme işlemi esnasında bir hata oluştu. Lütfen tekrar deneyiniz. <br/><pre> Hata:'.$ex->getMessage(). '</pre>');
        }


    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd('ss');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = HashingSlug::decodeHash($id);
        $package = Package::find($id);
        if (!$package) {
            return back()->with('warning', 'İlgili kayıt bulunamamıştır. Lütfen kontrol ederek tekrar deneyiniz.');
        }
        $package_types = PackageType::orderBy('name', 'ASC')->pluck('name', 'id');
        $services = AdditionalService::orderBy('name', 'ASC')->pluck('name', 'id');
        $sectors = Sector::orderBy('name', 'ASC')->pluck('name', 'id');

        return view('packages.edit', compact('package', 'services', 'sectors', 'package_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreatePackageRequest $request, $id)
    {

        $id = HashingSlug::decodeHash($id);
        $package = Package::find($id);

        if (!$package) {
            return back()->with('warning', 'İlgili kayıt bulunamamıştır. Lütfen kontrol ederek tekrar deneyiniz.');
        }

        try {
            $package->package_type_id = $request->package_type_id;
            $package->name = $request->name;
            $package->gorsel_adeti = $request->gorsel_adeti;
            $package->class_count = $request->class_count;
            $package->name_mini = $request->name_mini;
            $package->name_top = $request->name_top;
            $package->inactive_services = $request->inactive_services;
            $package->icon = $request->icon;
            $package->description = $request->description;
            $package->price = str_replace(',', '', $request->price);
            $package->old_price = $request->old_price;
            $package->kampanya_text = $request->kampanya_text;
            $package->active = $request->active;
            $package->is_star = $request->is_star;
            $package->is_suggested = $request->is_suggested;

            $package->sector_lists = isset($request->sector_lists) ? implode(',', $request->sector_lists) : null;

            if(isset($request->additional_services)){
                $servis_dizi = array();
                $services = AdditionalService::get();
                foreach ($services as $service){
                    $servis_dizi[$service->id]=['id'=> $service->id, 'name' =>$service->name, 'short_name' =>$service->short_name, 'price' => $service->price];
                }
                $ek_servisler = array();
                $x = 0;
                foreach ($request->additional_services as $ek){
                    $ek_servisler[$x] = $servis_dizi[$ek];
                    $x++;
                }
            }else{
                $ek_servisler = null;
            }


            $package->additional_services = json_encode($ek_servisler);
            $package->save();
        } catch(\Exception $ex) {
            return redirect(route('packages.index'))->with('danger', ' Güncelleme işlemi esnasında bir hata oluştu. Lütfen tekrar deneyiniz. <br/><pre> Hata:'.$ex->getMessage(). '</pre>');
        }
        return redirect(route('packages.index'))->with('success', 'Paket bilgileri başarılı bir şekilde sisteme güncellenmiştir.');


    }
}
