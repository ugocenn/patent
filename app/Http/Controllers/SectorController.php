<?php

namespace App\Http\Controllers;

use App\Helpers\HashingSlug;
use App\Http\Requests\Sector\CreateSectorRequest;
use App\Models\Basket;
use App\Models\Search;
use App\Models\Sector;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SectorController extends Controller
{


    public function index()
    {
        $sectors = Sector::orderBy('sort', 'ASC')->get();

        return view('sectors.index', [
            'sectors' => $sectors
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sectors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateSectorRequest $request)
    {
        try {
            $sector = new Sector();
            $sector->name = $request->name;
            $sector->sinif_degeri = $request->sinif_degeri;
            $sector->sort = $request->sort;
            $sector->save();
            return redirect(route('sectors.index'))->with('success', ' Sektör bilgileri başarılı bir şekilde sisteme eklenmiştir.');
        } catch(\Exception $ex) {
            return redirect(route('sectors.index'))->with('danger', ' Güncelleme işlemi esnasında bir hata oluştu. Lütfen tekrar deneyiniz. <br/><pre> Hata:'.$ex->getMessage(). '</pre>');
        }


    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd('ss');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = HashingSlug::decodeHash($id);
        $sector = Sector::find($id);
        if (!$sector) {
            return back()->with('warning', 'İlgili kayıt bulunamamıştır. Lütfen kontrol ederek tekrar deneyiniz.');
        }

        return view('sectors.edit', compact('sector'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateSectorRequest $request, $id)
    {

        $id = HashingSlug::decodeHash($id);
        $sector = Sector::find($id);

        if (!$sector) {
            return back()->with('warning', 'İlgili kayıt bulunamamıştır. Lütfen kontrol ederek tekrar deneyiniz.');
        }

        try {
            $sector->name = $request->name;
            $sector->sinif_degeri = $request->sinif_degeri;
            $sector->sort = $request->sort;
            $sector->save();
        } catch(\Exception $ex) {
            return redirect(route('sectors.index'))->with('danger', ' Güncelleme işlemi esnasında bir hata oluştu. Lütfen tekrar deneyiniz. <br/><pre> Hata:'.$ex->getMessage(). '</pre>');
        }
        return redirect(route('sectors.index'))->with('success', 'Sektör bilgileri başarılı bir şekilde sisteme güncellenmiştir.');


    }

    public function destroy($id) {

        $id = HashingSlug::decodeHash($id);
        $sector = Sector::find($id);
        if (!$sector) {
            return back()->with('warning', 'İlgili kayıt bulunamamıştır. Lütfen kontrol ederek tekrar deneyiniz.');
        }
        $search = Search::where('sector_id', $sector->id)->first();
        if ($search) {
            return back()->with('warning', 'Arama kayıtlarında ilgili sektöre ait kayıt bulunmaktadır. Arama kayıtlarını sildikten sonra tekrar deneyiniz.');
        }
        $sector->forceDelete();


        return redirect(route('sectors.index'))->with('success', 'Kayıt başarılı bir şekilde silinmiştir.');
    }
}
