<?php

namespace App\Http\Controllers;

use App\Helpers\HashingSlug;
use App\Http\Requests\AdditionalService\CreateAdditionalServiceRequest;
use App\Models\AdditionalService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ServiceController extends Controller
{


    public function index()
    {
        $services = AdditionalService::orderBy('name', 'ASC')->get();

        return view('services.index', [
            'services' => $services
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateAdditionalServiceRequest $request)
    {
        try {
            $service = new AdditionalService();
            $service->name = $request->name;
            $service->type = $request->type;
            $service->description = $request->description;
            $service->active = $request->active;
            $service->price = str_replace(',', '', $request->price);
            $service->save();
            return redirect(route('services.index'))->with('success', ' Ek Hizmet bilgileri başarılı bir şekilde sisteme eklenmiştir.');
        } catch(\Exception $ex) {
            return redirect(route('services.index'))->with('danger', ' Güncelleme işlemi esnasında bir hata oluştu. Lütfen tekrar deneyiniz. <br/><pre> Hata:'.$ex->getMessage(). '</pre>');
        }


    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd('ss');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = HashingSlug::decodeHash($id);
        $service = AdditionalService::find($id);
        if (!$service) {
            return back()->with('warning', 'İlgili kayıt bulunamamıştır. Lütfen kontrol ederek tekrar deneyiniz.');
        }

        return view('services.edit', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateAdditionalServiceRequest $request, $id)
    {

        $id = HashingSlug::decodeHash($id);
        $service = AdditionalService::find($id);

        if (!$service) {
            return back()->with('warning', 'İlgili kayıt bulunamamıştır. Lütfen kontrol ederek tekrar deneyiniz.');
        }

        try {

            $service->name = $request->name;
            $service->type = ''.$request->type.'';
            $service->description = $request->description;
            $service->active = $request->active;
            $service->price = str_replace(',', '', $request->price);
            $service->save();
        } catch(\Exception $ex) {
            return redirect(route('services.index'))->with('danger', ' Güncelleme işlemi esnasında bir hata oluştu. Lütfen tekrar deneyiniz. <br/><pre> Hata:'.$ex->getMessage(). '</pre>');
        }
        return redirect(route('services.index'))->with('success', 'Ek hizmet bilgileri başarılı bir şekilde sisteme güncellenmiştir.');
    }

    public function destroy($id) {

        $id = HashingSlug::decodeHash($id);
        $service = AdditionalService::find($id);
        if (!$service) {
            return back()->with('warning', 'İlgili kayıt bulunamamıştır. Lütfen kontrol ederek tekrar deneyiniz.');
        }
        $service->forceDelete();


        return redirect(route('services.index'))->with('success', 'Kayıt başarılı bir şekilde silinmiştir.');
    }
}
