<?php 

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Page;
use Illuminate\Http\Request;


class SitemapController extends Controller
{
    
    public function index(Request $r)
    {
       
        $pages = Page::orderBy('id','desc')->where('active', 1)->get();

        return response()->view('sitemap', compact('pages'))
          ->header('Content-Type', 'text/xml');

    }
}