<?php

namespace App\Http\Controllers;

use App\Helpers\HashingSlug;
use App\Models\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MenuController extends Controller
{

    public function index()
    {
        $menus = Menu::where('parent_id', '=', 0)->get();

        $allMenus = Menu::pluck('name', 'id');
        $defaultSelection = ['' => 'Seçiniz'];
        $allMenus = $defaultSelection + $allMenus->toArray();

        return view('menus.index',compact('menus','allMenus'));
    }

    public function create()
    {
        $allMenus = Menu::pluck('name', 'id');
        $defaultSelection = ['' => 'Seçiniz'];
        $allMenus = $defaultSelection + $allMenus->toArray();

        return view('menus.create',compact('allMenus'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $input = $request->all();
        $input['parent_id'] = empty($input['parent_id']) ? 0 : $input['parent_id'];
        Menu::create($input);
        return redirect(route('menus.index'))->with('success', 'Menü kaydı başarılı bir şekilde sisteme eklenmiştir.');
    }

    public function show()
    {
        $menus = Menu::where('parent_id', '=', 0)->get();
        return view('menus.index',compact('menus'));
    }

}
