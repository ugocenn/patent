<?php

namespace App\Http\Controllers;

use App\Models\Video;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    public function index(){
        $video  = Video::find(1);
        return view('videos.index',compact('video'));
    }

    public function save(Request $request){
        $video  = Video::find(1);
        $video->update($request->post());
        return view('videos.index',compact('video'));
    }
}
