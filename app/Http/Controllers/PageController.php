<?php

namespace App\Http\Controllers;

use App\Helpers\HashingSlug;
use App\Http\Requests\Page\CreatePageRequest;
use App\Models\Category;
use App\Models\Page;
use App\Models\Sector;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class PageController extends Controller
{


    public function index()
    {
        $pages = Page::orderBy('created_at', 'DESC')->get();

        return view('pages.index', [
            'pages' => $pages
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::pluck('name', 'id');
        return view('pages.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePageRequest $request)
    {
        //dd($request->post());

        try {
            $page = new Page();
            $page->title = $request->title;
            $page->slug = $request->slug;
            $page->active = $request->active;
            $page->image = $request->image;
            /**
             * Gelen posttan küçük image i küçültüyoruz...
             * /depo/dosyalar/resimismi.jpg" / ı silmeliyiz.
             */

            if(isset($request->image)){
                $image = ltrim($request->image,"/public/");
                //$image = public_path().'/'.$image;
                $img = Image::make(url('/public/'.$image));
                $filename = basename($image);

                if($img->width() > $img->height()){
                    $img->resize(null, 255, function ($constraint) {
                        $constraint->aspectRatio();
                    });

                }else{
                    $img->resize(188, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $img->fit(188, 255);
                $path = public_path().'/depo/dosyalar/188x255';
                if(!File::exists($path)) {
                    File::makeDirectory($path);
                }

                $img->save($path.'/'.$filename);

            }

            if(isset($request->page_header_image)){
                $image = ltrim($request->page_header_image,"/public/");

                $img = Image::make(url('/public/'.$image));
                $filename = basename($image);

                $width = $img->width();
                $height = $img->height();
                if(($width == $height) or ($width > $height)){
                    if($width >= 960 ){
                        $img->fit(960,380);
                    }
                }else{
                    if($height >= 380){
                        $img->resize(null, 380);
                    }
                }

                $path = public_path().'/depo/dosyalar/960x380';
                if(!File::exists($path)) {
                    File::makeDirectory($path);
                }
                $img->save($path.'/'.$filename);
            }


            $page->page_header_image = $request->page_header_image;


            $page->short_text = $request->short_text;
            $page->location = $request->location;
            $page->content = $request->page_content;
            $page->metadescription = $request->metadescription;
            $page->keywords = $request->keywords;
            $page->category_id = $request->category_id;


            $page->save();
            return redirect(route('web-pages.index'))->with('success', 'Sayfa bilgileri başarılı bir şekilde sisteme eklenmiştir.');
        } catch(\Exception $ex) {
            return redirect(route('web-pages.index'))->with('danger', ' Güncelleme işlemi esnasında bir hata oluştu. Lütfen tekrar deneyiniz. <br/><pre> Hata:'.$ex->getMessage(). '</pre>');
        }


    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd('ss');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = HashingSlug::decodeHash($id);
        $page = Page::find($id);
        if (!$page) {
            return back()->with('warning', 'İlgili kayıt bulunamamıştır. Lütfen kontrol ederek tekrar deneyiniz.');
        }
        $categories = Category::pluck('name', 'id');
        return view('pages.edit', compact('page', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreatePageRequest $request, $id)
    {

        $id = HashingSlug::decodeHash($id);
        $page = Page::find($id);

        if (!$page) {
            return back()->with('warning', 'İlgili kayıt bulunamamıştır. Lütfen kontrol ederek tekrar deneyiniz.');
        }

        try {
            $page->title = $request->title;
            $page->short_text = $request->short_text;
            $page->content = $request->page_content;
            $page->slug = $request->slug;
            /**
             * Gelen posttan küçük image i küçültüyoruz...
             * /public/depo/dosyalar/resimismi.jpg" public i silmeliyiz.
             */
            if(isset($request->image)){
                $image = ltrim($request->image,"/public/");
                //$image = public_path().'/'.$image;
                $img = Image::make(url('/public/'.$image));
                $filename = basename($image);

                if($img->width() > $img->height()){
                    $img->resize(null, 255, function ($constraint) {
                        $constraint->aspectRatio();
                    });

                }else{
                    $img->resize(188, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $img->fit(188, 255);
                $path = public_path().'/depo/dosyalar/188x255';
                if(!File::exists($path)) {
                    File::makeDirectory($path);
                }

                $img->save($path.'/'.$filename);

            }

            if(isset($request->page_header_image)){
                $image = ltrim($request->page_header_image,"/public/");

                $img = Image::make(url('/public/'.$image));
                $filename = basename($image);

                $width = $img->width();
                $height = $img->height();
                if(($width == $height) or ($width > $height)){
                    if($width >= 960 ){
                        $img->fit(960,380);
                    }
                }else{
                    if($height >= 380){
                        $img->resize(null, 380);
                    }
                }

                $path = public_path().'/depo/dosyalar/960x380';
                if(!File::exists($path)) {
                    File::makeDirectory($path);
                }
                $img->save($path.'/'.$filename);
            }
            $page->image = $request->image;
            $page->page_header_image = $request->page_header_image;
            $page->active = $request->active;
            $page->location = $request->location;
            $page->metadescription = $request->metadescription;
            $page->keywords = $request->keywords;
            $page->category_id = $request->category_id;
            $page->save();
        } catch(\Exception $ex) {
            return redirect(route('web-pages.index'))->with('danger', ' Güncelleme işlemi esnasında bir hata oluştu. Lütfen tekrar deneyiniz. <br/><pre> Hata:'.$ex->getMessage(). '</pre>');
        }
        return redirect(route('web-pages.index'))->with('success', 'Sayfa bilgileri başarılı bir şekilde sisteme güncellenmiştir.');


    }

    public function showPage($slug)
    {
        $sectors = Sector::orderby('sort', 'ASC')->get();

        if (view()->exists('detay')) {
            $view = 'detay';
            // $category = Category::where('slug', $section)->first();
            // if (!$category) {
            //     $view = 'errors.404';
            //     return view($view);
            // }
            $page = Page::where('slug', $slug)->first();
            if (!$page) {
                $view = 'errors.404';
                return view($view);
            }
            if($page->page_header_image == null){
                $view = 'detay_noimage';
            }
            return view($view, compact('page','sectors'));
        } else {
            $view = 'errors.frontend.404';
            return view($view);
        }
    }


    public function destroy($id) {

        $id = HashingSlug::decodeHash($id);
        $page = Page::find($id);
        if (!$page) {
            return back()->with('warning', 'İlgili kayıt bulunamamıştır. Lütfen kontrol ederek tekrar deneyiniz.');
        }
        $page->forceDelete();

        return redirect(route('web-pages.index'))->with('success', 'Kayıt başarılı bir şekilde silinmiştir.');
    }
}
