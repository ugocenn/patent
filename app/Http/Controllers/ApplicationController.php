<?php

namespace App\Http\Controllers;


use App\Helpers\HashingSlug;
use App\Http\Requests\Application\CreateApplicationRequest;
use App\Mail\NewApplicationMail;
use Mail;
use App\Models\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Revolution\Google\Sheets\Facades\Sheets;

class ApplicationController extends Controller
{




    public function index()
    {
        $applications = Application::orderBy('mark', 'ASC')->get();
        return view('applications.index', [
            'applications' => $applications
        ]);
    }

//https://docs.google.com/spreadsheets/d/1rF53Ynfp80-NthXv94kkcgAfyFqt94kvlZUwBtyQqt4/edit?usp=sharing
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('applications.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateApplicationRequest $request)
    {


    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new FaqResource(Faq::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = HashingSlug::decodeHash($id);
        $application = Application::find($id);
        if (!$application) {
            return back()->with('warning', 'İlgili kayıt bulunamamıştır. Lütfen kontrol ederek tekrar deneyiniz.');
        }

        return view('applications.edit', compact('application'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateApplicationRequest $request, $id)
    {

        $id = HashingSlug::decodeHash($id);
        $application = Application::find($id);

        if (!$application) {
            return back()->with('warning', 'İlgili kayıt bulunamamıştır. Lütfen kontrol ederek tekrar deneyiniz.');
        }

        try {
            $application->mark = $request->mark;
            $application->sector = $request->sector;
            $application->name = $request->name;
            $application->phone = $request->phone;
            $application->save();
        } catch(\Exception $ex) {
            return redirect(route('applications.index'))->with('danger', ' Güncelleme işlemi esnasında bir hata oluştu. Lütfen tekrar deneyiniz. <br/><pre> Hata:'.$ex->getMessage(). '</pre>');
        }
        return redirect(route('applications.index'))->with('success', 'Başvuru bilgileri başarılı bir şekilde sisteme güncellenmiştir.');
    }
}
