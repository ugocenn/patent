<?php

namespace App\Http\Controllers;

use App\Mail\NewApplicationMail;
use App\Models\Basket;
use App\Models\Company;
use App\Models\Customer;
use App\Models\Order;
use App\Models\Package;
use App\Models\PackageType;
use App\Models\ProductClass;
use App\Models\ProfessionCode;
use App\Models\Search;
use App\Models\Sector;
use Carbon\Carbon;
use DB;
use FontLib\Table\Type\name;
use Mail;
use Revolution\Google\Sheets\Facades\Sheets;
use Illuminate\Http\Request;
use Vinkla\Hashids\Facades\Hashids;


class AjaxController extends Controller
{

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */

    public function getJobCodes()
    {
        header('Content-Type: application/json');

        $codes = ProfessionCode::select(DB::raw("CONCAT(name, ' - ', isco) AS code"),'id')->pluck('code');
        echo json_encode($codes);
    }

    public function setData(Request $request)
    {
        try {

            $package_type = PackageType::findOrFail($request->type);
            $is_yeni = 1;
            if (isset($request->visitor)) {
                $visitorDecode = Hashids::decode($request->visitor);
                //echo $visitorDecode;
                $customer = @Customer::find($visitorDecode[0]);
                if (!$customer) {
                    $customer = new Customer();
                } else {
                    $is_yeni = 0;
                }
            } else {
                $customer = new Customer();
            }

            $customer->isim = str_replace('"', '', $request->name);
            $customer->iletisim_izni = $request->iletisimizni;
            $tel = str_replace('"', '', $request->phone);
            $tel  = ltrim($tel,"+90");
            $tel  = ltrim($tel,"90");
            $tel  = ltrim($tel,"0");
            $tel = "+90" . $tel;

            $customer->tel = $tel;
            $customer->save();

            $package = Package::where('package_type_id', $package_type->id)->where('active', 1)->first();



            $search = new Search();
            $search->customer_id = $customer->id;
            $mark_gelen = str_replace('"', '', $request->mark);
            $sector_gelen = str_replace('"', '', $request->sector);
            $sector = Sector::where('name', $sector_gelen)->first();
            if ($sector) {
                $search->sector_id = $sector->id;
            } else {
                $search->sector_id = 0;
            }
            $search->package_type_id = $package->package_type_id;
            $search->mark = $mark_gelen;
            $search->sonuc_sayisi = $request->sonuc_sayisi;
            $search->sektor_ismi = $sector_gelen;
            $search->save();

            /*
             * marka
                sektör ismi
                sektör değeri
                isim
                telefon
                sorgu saati
                kaç sonuç gördü
                kullanıcı ip
             * */
            $append = [
                $mark_gelen,
                $sector_gelen,
                $sector->sinif_degeri,
                str_replace('"', '', $request->name),
                str_replace('"', '', $request->phone),
                Carbon::parse($search->created_at)->format('Y-m-d H:i:s'),
                $request->sonuc_sayisi,
                $request->iletisimizni ? 'EVET' : 'HAYIR',
                $request->ip(),
                $customer->id,
                $is_yeni ? 'EVET' : 'HAYIR'
            ];

            /*
            Sheets::spreadsheet(env('POST_SPREADSHEET_ID'))

                ->sheetById(config('sheets.post_sheet_id'))

                ->append([$append]);
            */
$token = [
      'access_token'  => 'hC7YjB0hFKseMcIxzEREEgKs',
      'refresh_token' => 'hC7YjB0hFKseMcIxzEREEgKsdfdfdf',
      'expires_in'    => 100,
      'created'       => $search->created_at,
];
/*
            Sheets::spreadsheet(env('POST_SPREADSHEET_ID'))->sheetById(env('POST_SPREADSHEET_ID'))->range('A:K')->append([$append]);
*/
/*
            $company = Company::first();
           /* Mail::to($company->email)->send(new NewApplicationMail(
                $search));
*/
            /*if ($request->type != 1) {
                $basket = new Basket();
                $basket->customer_id = $customer->id;
                $basket->sepet_turu = $package->type;
                $basket->packet_id = $package->id;
                $secili_sektorler = explode(',', $package->sector_lists);

                $ilave_hizmetler = explode(',', $package->additional_services);
                $basket->secili_sektorler = json_encode($secili_sektorler);
                $basket->ilave_hizmetler = json_encode($ilave_hizmetler);
                for($i = 1; $i <= $package->class_count; $i++) {
                    $secili_siniflar[] = $i;
                }
                if ($package->class_count == 0) {
                    $secili_siniflar = [];
                }
                $basket->secili_siniflar = json_encode($secili_siniflar);
                $basket->sepet_toplami = $order->price;
                $basket->save();
                return ['customer_id' => $customer->id, 'search_id' => $search->id, 'order_id' => $order->id];
            }*/
            $visitor = Hashids::encode($customer->id);
            $search = Hashids::encode($search->id);
            return ['visitor' => $visitor, 'search_id' => $search];
        } catch(\Exception $ex) {
            return $ex;
        }




    }

    public function createBasket(Request $request){
        $basket = new Basket();
    }

    public function updateBasket(Request $request){
        //dd($request);
        $fazla_class_sayisi = 0;
        $paket_class_sayisi = 0;
        $paket_name = '';
        $ilave_toplam = 0;
        $customer = Customer::find($request->visitor_id);
        $customer->tc = $request->tc;
        $customer->eposta = $request->eposta;
        $customer->adres = $request->adres;
        $customer->fatura_turu = $request->fatura_turu;
        $customer->save();

        if(isset($request->packet_id) AND $request->packet_id != 0){
            $package = Package::findOrFail($request->packet_id);
            $paket_class_sayisi = $package->class_count;
            $paket_name = $package->name_mini;
            $paket_price = $package->price;
        }else{
            $paket_name = 'KAPSAMLI';

        }

        $secilen_class_sayisi = (isset($request->secili_siniflar)) ? count($request->secili_siniflar) : 0;
        $fazla_class_sayisi = $secilen_class_sayisi - $paket_class_sayisi;
        if($fazla_class_sayisi > 0){
            $paket_id = 0;
        }else{
            $paket_id = $request->packet_id;
        }

        $basket = Basket::findOrFail($request->basket_id);
        if(isset($request->packet_id) AND $request->packet_id == 0){
            $ara = Package::where('package_type_id',$basket->package_type_id)->orderby('price','ASC')->first();
            $paket_price = $ara->price;
        }

        if($request->secim == 'sektor'){
            $basket->secili_sektorler = $request->secili_sektorler;
            $basket->save();
            $sinif_fiyati = 0;
        }
        if($request->secim == 'paket' AND $request->packet_id != 0){
            $basket->packet_id = $request->packet_id;
            $basket->secili_sektorler = $request->secili_sektorler;
            $basket->secili_siniflar = null;
            $basket->ilave_hizmetler = $package->additional_services;
            $basket->ilave_sinif_toplam = 0;
            $basket->ilave_hizmet_toplam = 0;
            $basket->sepet_toplami = $package->price;
            $basket->save();
            $sinif_fiyati = 0;
        }elseif($request->secim == 'paket'){

            //sepet türünü buradan anlıyoruz..
            $ara = Package::where('package_type_id',$basket->package_type_id)->orderby('price','ASC')->first();
            $paket_price = $ara->price;
            $basket->packet_id = 0;
            $basket->secili_sektorler = isset($request->secili_sektorler) ? $request->secili_sektorler : null;

            $basket->secili_siniflar = json_encode($request->secili_siniflar);
            $basket->ilave_hizmetler = json_encode($request->ilave_hizmetler);;

            $baz_fiyat = $ara->price;
            $sinif_fiyati = ProductClass::first()->price;

            if(isset($request->secili_siniflar)){
                $kac_tane = count($request->secili_siniflar);

                $sinif_fiyati = $kac_tane > 1 ? ($kac_tane-1) * $sinif_fiyati : 0;
            }else{
                $sinif_fiyati = 0;
            }

            if(isset($request->ilave_hizmetler)){
                $ilave_toplam = 0;
                foreach ($request->ilave_hizmetler as $ilave){
                    $ilave_toplam += $ilave['price'];
                }
            }
            $total = number_format(($sinif_fiyati + $baz_fiyat + $ilave_toplam), 2, '.', '');


            $basket->sepet_toplami = $total;
            $basket->save();
        }elseif ($request->secim == 'sinif' or $request->secim == 'ilave'){
            $basket->secili_siniflar = json_encode($request->secili_siniflar);
            $basket->ilave_hizmetler = json_encode($request->ilave_hizmetler);

            if($request->secim == 'paket' AND $paket_id != 0){
                $baz_fiyat = $package->price;
            }else{
                $ara = Package::where('package_type_id',$basket->package_type_id)->orderby('price','ASC')->first();
                $baz_fiyat = $ara->price;
            }

            $sinif_fiyati = ProductClass::first()->price;
            if(isset($request->secili_siniflar)){
                $kac_tane = count($request->secili_siniflar);

                $sinif_fiyati = $kac_tane > 1 ? ($kac_tane-1) * $sinif_fiyati : 0;
            }else{
                $sinif_fiyati = 0;
            }
            if(isset($request->ilave_hizmetler)){
                $ilave_toplam = 0;
                foreach ($request->ilave_hizmetler as $ilave){
                    $ilave_toplam += $ilave['price'];
                }
            }
            $total = number_format(($sinif_fiyati + $baz_fiyat + $ilave_toplam), 2, '.', '');
            $basket->sepet_toplami  = $total;
            $basket->ilave_hizmet_toplam = $ilave_toplam;
            $basket->save();

        }
        //dd($request->post());


        $KDV = number_format((($basket->sepet_toplami * 18)/100), 2, '.', '');
        $toplam = number_format(($basket->sepet_toplami + $KDV), 2, '.', '');
        $ilave_toplam = number_format($ilave_toplam, 2, '.', '');
        if (isset($request->search_id)) {
            $search = Search::find($request->search_id);
            $search->mark = $request->marka;
            $search->basvuru_aciklamasi = isset($request->basvuru_aciklamasi) ? $request->basvuru_aciklamasi : '';
            $search->save();
        }

        return response()->json(array(
           'paket_id' => $basket->packet_id,
           'paket_ismi' => $paket_name,
           'paket_price' => $paket_price,
           'sektor' => $basket->secili_sektorler,
           'siniflar' => $basket->secili_siniflar,
           'hizmetler' => $basket->ilave_hizmetler,
           'sinif_ucreti' => $sinif_fiyati,
           'ilave_price' => $ilave_toplam,
           'toplam' => $toplam,
            'kdv' => $KDV
        ));


    }
}
