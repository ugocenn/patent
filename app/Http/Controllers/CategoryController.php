<?php

namespace App\Http\Controllers;

use App\Helpers\HashingSlug;
use App\Http\Requests\Category\CreateCategoryRequest;
use App\Models\Basket;
use App\Models\Category;
use App\Models\Customer;
use App\Models\Page;
use App\Models\Search;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{


    public function index()
    {
        $categories = Category::orderBy('name', 'ASC')->get();

        return view('categories.index', [
            'categories' => $categories
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCategoryRequest $request)
    {
        try {
            $category = new Category();
            $category->name = $request->name;
            $category->metadescription = $request->metadescription;
            $category->description = $request->description;
            $category->keywords = $request->keywords;
            $category->title = $request->title;
            $category->slug = $request->slug;
            //$category->text1 = $request->text1;
            //$category->text2 = $request->text2;


            $category->save();
            return redirect(route('categories.index'))->with('success', 'Kategori bilgileri başarılı bir şekilde sisteme eklenmiştir.');
        } catch(\Exception $ex) {
            return redirect(route('categories.index'))->with('danger', ' Güncelleme işlemi esnasında bir hata oluştu. Lütfen tekrar deneyiniz. <br/><pre> Hata:'.$ex->getMessage(). '</pre>');
        }


    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd('ss');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = HashingSlug::decodeHash($id);
        $category = Category::find($id);
        if (!$category) {
            return back()->with('warning', 'İlgili kayıt bulunamamıştır. Lütfen kontrol ederek tekrar deneyiniz.');
        }

        return view('categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateCategoryRequest $request, $id)
    {

        $id = HashingSlug::decodeHash($id);
        $category = Category::find($id);

        if (!$category) {
            return back()->with('warning', 'İlgili kayıt bulunamamıştır. Lütfen kontrol ederek tekrar deneyiniz.');
        }

        try {
            $category->name = $request->name;
            $category->description = $request->description;
            $category->metadescription = $request->metadescription;
            $category->keywords = $request->keywords;
            $category->title = $request->title;
            $category->slug = $request->slug;
            //$category->text1 = $request->text1;
            //$category->text2 = $request->text2;
            $category->save();
        } catch(\Exception $ex) {
            return redirect(route('categories.index'))->with('danger', ' Güncelleme işlemi esnasında bir hata oluştu. Lütfen tekrar deneyiniz. <br/><pre> Hata:'.$ex->getMessage(). '</pre>');
        }
        return redirect(route('categories.index'))->with('success', 'Kategori bilgileri başarılı bir şekilde sisteme güncellenmiştir.');


    }
    public function destroy($id) {

        $id = HashingSlug::decodeHash($id);
        $category = Category::find($id);
        if (!$category) {
            return back()->with('warning', 'İlgili kayıt bulunamamıştır. Lütfen kontrol ederek tekrar deneyiniz.');
        }

        $page = Page::where('category_id', $category->id)->first();

        if ($page) {
            return back()->with('warning', 'İlgili kategoriye ait sayfa bulunmuştur. Lütfen öncelikle sayfayı silerek tekrar deneyiniz.');
        }
        $category->forceDelete();


        return redirect(route('categories.index'))->with('success', 'Kayıt başarılı bir şekilde silinmiştir.');
    }
}
