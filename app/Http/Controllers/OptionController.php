<?php

namespace App\Http\Controllers;

use App\Models\Option;
use Illuminate\Http\Request;

class OptionController extends Controller
{
    public function index(){
        $option = Option::where('id',1)->first();
        return view('backend.options.index' , compact('option'));
    }

    public function save(Request $request){
        try{
            $option = Option::where('id',1)->update($request->except('_token'));
            return redirect(route('option.index'))->with('success', 'Ayarlar başarılı bir şekilde güncellenmiştir.');
        }catch(\Exception $e){
            return redirect(route('option.index'))->with('error', 'Bir hata oluştu.');
        }

    }
}
