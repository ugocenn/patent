<?php

namespace App\Http\Controllers;

use App\Helpers\HashingSlug;
use App\Http\Requests\Sector\CreateSectorRequest;
use App\Models\AdditionalService;
use App\Models\Basket;
use App\Models\Customer;
use App\Models\Order;
use App\Models\Package;
use App\Models\Search;
use App\Models\PackageType;
use App\Models\ProductClass;
use App\Models\Sector;
use App\Models\Sell;
use App\Models\Upload;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Vinkla\Hashids\Facades\Hashids;

use Iyzipay\Model\Address;
use Iyzipay\Model\BasketItem;
use Iyzipay\Model\BasketItemType;
use Iyzipay\Model\Buyer;
use Iyzipay\Model\CheckoutForm;
use Iyzipay\Model\CheckoutFormInitialize;
use Iyzipay\Model\Currency;
use Iyzipay\Model\Locale;
use Iyzipay\Model\PaymentGroup;
use Iyzipay\Options;
use Iyzipay\Request\CreateCheckoutFormInitializeRequest;
use Storage;

class BasvuruController extends Controller
{


    public function index($type,$visitor = null, $search_id = null,  Request $request)
    {
        $package_type = PackageType::where('slug', $type)->first();
        try {
            if(isset($visitor)){
                $customer_id = Hashids::decode($visitor)[0];
                $customer = Customer::find($customer_id);
            }else{
                $customer = new Customer();
                $customer->save();
            }



            if (isset($search_id)) {
                $searchID = Hashids::decode($search_id)[0];
                $search = Search::find($searchID);
            }else{
                $search = new Search();
                $search->customer_id = $customer->id;
                $search->package_type_id = $package_type->id;
                $search->save();
                $searchID = $search->id;
            }
        }catch(\Exception $e){
            return $e;
        }




        $packages = Package::where('package_type_id', $package_type->id)->where('active', 1)->get();
        $suggested_package = Package::where('active', 1)->where('package_type_id', $package_type->id)->first();
        $sectors = Sector::orderby('sort','ASC')->get();
        $services = AdditionalService::where('active', 1)->get();

        //kartlara servis isimlerini yazdırmak için
        $additional_services = AdditionalService::pluck('name','id');




        if ($package_type->id == 1) {
            if(isset($customer) and isset($search)){
                $basket_search = Basket::where('customer_id' , $customer->id)->where('search_id', $search->id)->first();
            }


            if(!isset($basket_search)){
                $basket = new Basket();
                $basket->customer_id = isset($customer) ? $customer->id : 0;
                $basket->search_id = $searchID;
                $basket->package_type_id = $suggested_package->package_type_id;
                $basket->packet_id = $suggested_package->id;
                //$secili_sektorler = explode(',', $suggested_package->sector_lists);
                $secili_sektor = isset($search) ? $search->sector_id : '';
                //$ilave_hizmetler = explode(',', $suggested_package->additional_services);
                $basket->secili_sektorler = $secili_sektor;
                $basket->ilave_hizmetler = json_encode($suggested_package->additional_services);
                for($i = 1; $i <= $suggested_package->class_count; $i++) {
                    $secili_siniflar[] = $i;
                }
                if ($suggested_package->class_count == 0) {
                    $secili_siniflar = [];
                }
                $basket->secili_siniflar = json_encode($secili_siniflar);
                $basket->sepet_toplami = $suggested_package->price;
                $basket->save();
            }else{
                $basket = $basket_search;
            }



        } else {
           // $order_id = $request->order_id;
            //$order = Order::find($order_id);
            //$search = Search::find($request->search_id);
            if (isset($customer)) {
                $basket = Basket::where('customer_id' , $customer->id)->first();
            }

        }


        $sayfa = $package_type->id==1 ? 'online-marka-tescili' : $type;


        return view($sayfa, [
            'packages' => $packages,
            'type' => $type,
            'sectors' => $sectors,
            'basket' => isset($basket) ? $basket : null,
            'visitor' => isset($customer) ? $customer : null,
            'search' => $search,
            'suggested_package' => $suggested_package,
            'services' => $services,
            'additional_services' => $additional_services,
            'paket_turu' => $package_type
        ]);
    }

    public function basvuruStore($type, Request $request)
    {
        if (!isset($request->basket_id) && !isset($request->visitor_id)) {
            return back();
        }
        //dd($request->post());
        $package_type = PackageType::where('slug', $type)->first();
        $packages = Package::where('package_type_id', $package_type->id)->where('active', 1)->get();
        $basket = Basket::find($request->basket_id);
        $customer = Customer::find($request->visitor_id);
        $marka = $request->marka;

        $ana_para = $basket->sepet_toplami;
        $ana_kdv = $basket->sepet_toplami * 0.18;
        $toplam = $ana_para + $ana_kdv;

        $customer->tc = $request->inputTCNo;
        $customer->eposta = $request->inputEmail;
        $customer->adres = $request->address;
        $customer->fatura_turu = $request->fatura_turu;
        $customer->save();




        $options = new Options();
        $options->setApiKey('ez8OKMA935Rg6KgMz7D5eAR1836n5TvI');
        $options->setSecretKey('w38KDNWdkFWy768Ny4skazB6iHe5uyto');
        $options->setBaseUrl('https://api.iyzipay.com');

        $request_iy = new CreateCheckoutFormInitializeRequest();
        $request_iy->setLocale(Locale::TR);
        $request_iy->setConversationId($basket->id);
        $request_iy->setPrice(number_format($toplam ,1,'.',''));
        $request_iy->setPaidPrice(number_format($toplam ,1,'.',''));
        $request_iy->setCurrency(Currency::TL);
        $request_iy->setBasketId($basket->id);
        $request_iy->setPaymentGroup(PaymentGroup::PRODUCT);
        $request_iy->setCallbackUrl(route('iyzicoSonuc'));
        $request_iy->setEnabledInstallments(array(2, 3, 6, 9));

        $buyer = new Buyer();
        $buyer->setId("BY".$basket->id);
        $buyer->setName($customer->isim);
        $phone = str_replace('(', '', $customer->tel);
        $phone = str_replace(')', '', $phone);
        $phone = str_replace(' ', '', $phone);
        $phone = str_replace('-', '', $phone);
        $buyer->setSurname($customer->isim);

        $buyer->setGsmNumber("+90".$phone);
        $buyer->setEmail($customer->eposta);
        $buyer->setIdentityNumber($customer->tc);
        $buyer->setLastLoginDate($basket->created_at->format('Y-m-d H:i:s'));
        $buyer->setRegistrationDate($basket->created_at->format('Y-m-d H:i:s'));
        $buyer->setRegistrationAddress($customer->adres);
        $buyer->setIp( \Request::ip());
        $buyer->setCity("Bursa");
        $buyer->setCountry("Turkey");
        $buyer->setZipCode("16000");
        $request_iy->setBuyer($buyer);

        $shippingAddress = new Address();
        $shippingAddress->setContactName($customer->isim);
        $shippingAddress->setCity("Bursa");
        $shippingAddress->setCountry("Turkey");
        $shippingAddress->setAddress($customer->adres);
        $shippingAddress->setZipCode("16000");
        $request_iy->setShippingAddress($shippingAddress);

        $billingAddress = new Address();
        $billingAddress->setContactName($customer->isim);
        $billingAddress->setCity("Bursa");
        $billingAddress->setCountry("Turkey");
        $billingAddress->setAddress($customer->adres);
        $billingAddress->setZipCode("16000");
        $request_iy->setBillingAddress($billingAddress);

        $basketItems = array();

        $i = 0;

        $ucret = 0;
        $ilave_hizmet_ucreti = 0;
        //dd(json_decode($basket->secili_siniflar));
        //dd(json_decode($basket->ilave_hizmetler));
        //$logolar = json_decode($basket->logolar);

        $secili_siniflar = json_decode($basket->secili_siniflar);
        $ilave_hizmetler = json_decode($basket->ilave_hizmetler);
        if (is_array($ilave_hizmetler)) {
            if(isset($ilave_hizmet)){
                foreach ($ilave_hizmetler as $ilave_hizmet) {
                    $ilave_hizmet_ucreti += $ilave_hizmet->price;
                    $firstBasketItem = new BasketItem();
                    $firstBasketItem->setId($basket->id.''.$ilave_hizmet->id);
                    $firstBasketItem->setName($ilave_hizmet->name);
                    $firstBasketItem->setCategory1("İlave Hizmetler");
                    $firstBasketItem->setItemType(BasketItemType::PHYSICAL);
                    $firstBasketItem->setPrice(number_format((($ilave_hizmet->price * 0.18 ) + $ilave_hizmet->price) ,1,'.',''));
                    $basketItems[$i] = $firstBasketItem;
                    $i++;
                }
            }

        }

        if (is_array($secili_siniflar)) {
            $adet = count($secili_siniflar);
            $ucret = $adet * 200;
            $firstBasketItem = new BasketItem();
            $firstBasketItem->setId($basket->id.'C02');
            $firstBasketItem->setName('Sınıf Ücreti');
            $firstBasketItem->setCategory1("İlave Sınıflar");
            $firstBasketItem->setItemType(BasketItemType::PHYSICAL);
            $firstBasketItem->setPrice(number_format((($ucret * 0.18 ) + $ucret) ,1,'.',''));
            $basketItems[$i] = $firstBasketItem;
            $i++;

        }
        $basvuru_ucreti = $ana_para - ($ilave_hizmet_ucreti + $ucret);
        if ($basvuru_ucreti > 0) {
            $thirdBasketItem = new BasketItem();
            $thirdBasketItem->setId('BK'.$basket->id.'02');
            $thirdBasketItem->setName('Başvuru Ücreti');
            $thirdBasketItem->setCategory1('Başvuru Ücreti');
            $thirdBasketItem->setItemType(BasketItemType::PHYSICAL);
            $thirdBasketItem->setPrice(number_format((($basvuru_ucreti * 0.18 ) + $basvuru_ucreti) ,1,'.',''));
            $basketItems[$i] = $thirdBasketItem;
        }



        $request_iy->setBasketItems($basketItems);
        //dd($request_iy);
        $checkoutFormInitialize = CheckoutFormInitialize::create($request_iy, $options);
        $paymentinput = $checkoutFormInitialize->getCheckoutFormContent();
        //dd($checkoutFormInitialize);
        if ($checkoutFormInitialize->getStatus() == "failure")

        {

            print_r($checkoutFormInitialize->getErrorCode());
            print_r($checkoutFormInitialize->getErrorMessage());

        }


        return view('odeme_formu', [
            'paymentinput' => $paymentinput,
            'basket' => $basket,
            'customer' => $customer,
            'packages' => $packages,
            'marka' => $marka,
            'ucret' => $ucret,
            'ilave_hizmet_ucreti' => $ilave_hizmet_ucreti,
            'toplam' => $toplam,
            'ana_para' => $ana_para,
            'ana_kdv' => $ana_kdv
        ]);
    }
    public function basvuruStoreTmp($type, Request $request)
    {

        $rules=[
            'marka' => 'required',
            'inputName' => 'required',
            'inputTCNo' => 'required',
            'inputEmail' => 'required|email',
            'inputTelefon' => 'required',
            'address' => 'required',
        ];
        $messages = array(
            'marka.required' => 'Marka alanı gerekmektedir.',
            'inputName.required' => 'İsim alanı gerekmektedir.',
            'inputTCNo.required' => 'T.C. No alanı gerekmektedir.',
            'inputEmail.required' => 'E-posta alanı gerekmektedir.',
            'inputEmail.email' => 'E-posta alanı hatalıdır.',
            'inputTelefon.required' => 'Telefon alanı gerekmektedir.',
            'address.required' => 'Adres alanı gerekmektedir.',

        );
        $this->validate($request , $rules, $messages);
        try {
            //dd($request);
            $inputs = $request->all();
            $inputs['basvuru_aciklamasi'] = isset($request->basvuru_aciklamasi) ? $request->basvuru_aciklamasi : null;
            $inputs['sectors_yeni'] = isset($request->sectors) ? implode(',',$request->sectors) : null;
            $inputs['classes_yeni'] = isset($request->classes) ? implode(',',$request->classes) : null;
            $inputs['services_yeni'] = isset($request->services) ? implode(',',$request->services) : null;
            $inputs['op_date'] = Carbon::now()->format('Y-m-d');
            $inputs['kdv'] = str_replace(',', '', $request->kdv);
            $inputs['total'] = str_replace(',', '', $request->total);
            $inputs['is_paid'] = 0;

            //dd($inputs);
            $sell = Sell::create($inputs);



            $files =  $request->file('dosyalar');
            if(is_array($files)) {
                foreach($files as $file) {
                    $uploadedFile = $file;
                    $filename = time().$uploadedFile->getClientOriginalName();

                    Storage::disk('local')->putFileAs(
                        'basvuru_dosyalari/'.$sell->id.'/',
                        $uploadedFile,
                        $filename
                    );

                    $upload = new Upload();
                    $upload->name = $filename;
                    $upload->file_path = 'basvuru_dosyalari/'.$sell->id.'/'.$filename;
                    $upload->save();
                }
            }

            /////////


            $options = new Options();
            $options->setApiKey('ez8OKMA935Rg6KgMz7D5eAR1836n5TvI');
            $options->setSecretKey('w38KDNWdkFWy768Ny4skazB6iHe5uyto');
            $options->setBaseUrl('https://api.iyzipay.com');

            $request_iy = new CreateCheckoutFormInitializeRequest();
            $request_iy->setLocale(Locale::TR);
            $request_iy->setConversationId($sell->id);
            $request_iy->setPrice(number_format($sell->total ,1,'.',''));
            $request_iy->setPaidPrice(number_format($sell->total ,1,'.',''));
            $request_iy->setCurrency(Currency::TL);
            $request_iy->setBasketId($sell->id);
            $request_iy->setPaymentGroup(PaymentGroup::PRODUCT);
            $request_iy->setCallbackUrl(route('iyzicoSonuc'));
            $request_iy->setEnabledInstallments(array(2, 3, 6, 9));

            $buyer = new Buyer();
            $buyer->setId("BY".$sell->id);
            $buyer->setName($sell->inputName);
            $phone = str_replace('(', '', $sell->inputTelefon);
            $phone = str_replace(')', '', $phone);
            $phone = str_replace(' ', '', $phone);
            $phone = str_replace('-', '', $phone);
            $buyer->setSurname($sell->inputName);

            $buyer->setGsmNumber("+90".$phone);
            $buyer->setEmail($sell->inputEmail);
            $buyer->setIdentityNumber($sell->inputTCNo);
            $buyer->setLastLoginDate($sell->created_at->format('Y-m-d H:i:s'));
            $buyer->setRegistrationDate($sell->created_at->format('Y-m-d H:i:s'));
            $buyer->setRegistrationAddress($sell->address);
            $buyer->setIp( \Request::ip());
            $buyer->setCity("Bursa");
            $buyer->setCountry("Turkey");
            $buyer->setZipCode("16000");
            $request_iy->setBuyer($buyer);

            $shippingAddress = new Address();
            $shippingAddress->setContactName($sell->inputEmail);
            $shippingAddress->setCity("Bursa");
            $shippingAddress->setCountry("Turkey");
            $shippingAddress->setAddress($sell->address);
            $shippingAddress->setZipCode("16000");
            $request_iy->setShippingAddress($shippingAddress);

            $billingAddress = new Address();
            $billingAddress->setContactName($sell->inputName);
            $billingAddress->setCity("Bursa");
            $billingAddress->setCountry("Turkey");
            $billingAddress->setAddress($sell->address);
            $billingAddress->setZipCode("16000");
            $request_iy->setBillingAddress($billingAddress);

            $basketItems = array();

            $i = 0;
            if ($sell->current_service_price > 0 && $sell->as_count > 0) {
                $s = explode(',', $sell->services_yeni);
                $services = AdditionalService::whereIn('id', $s)->get();

                foreach ($services as $service) {
                    $firstBasketItem = new BasketItem();
                    $firstBasketItem->setId($sell->id.''.$service->id);
                    $firstBasketItem->setName($service->name);
                    $firstBasketItem->setCategory1("İlave Hizmetler");
                    $firstBasketItem->setItemType(BasketItemType::PHYSICAL);
                    $firstBasketItem->setPrice(number_format((($service->price * 0.18 ) + $service->price) ,1,'.',''));
                    $basketItems[$i] = $firstBasketItem;

                    $i++;
                }

            }

            if ($sell->ilave_sinif_ucretleri > 0 && $sell->class_count > 0) {
                $s = explode(',', $sell->classes_yeni);
                $classes = ProductClass::whereIn('name', $s)->get();

                foreach ($classes as $class) {
                    $firstBasketItem = new BasketItem();
                    $firstBasketItem->setId($sell->id.''.$class->id);
                    $firstBasketItem->setName($class->name);
                    $firstBasketItem->setCategory1("İlave Sınıflar");
                    $firstBasketItem->setItemType(BasketItemType::PHYSICAL);
                    $firstBasketItem->setPrice(number_format((($class->price * 0.18 ) + $class->price) ,1,'.',''));
                    $basketItems[$i] = $firstBasketItem;

                    $i++;
                }

            }

            if ($sell->basvuru_ucreti > 0) {
                $thirdBasketItem = new BasketItem();
                $thirdBasketItem->setId('BK'.$sell->id.'02');
                $thirdBasketItem->setName('Başvuru Ücreti');
                $thirdBasketItem->setCategory1('Başvuru Ücreti');
                $thirdBasketItem->setItemType(BasketItemType::PHYSICAL);
                $thirdBasketItem->setPrice(number_format((($sell->basvuru_ucreti * 0.18 ) + $sell->basvuru_ucreti) ,1,'.',''));
                $basketItems[$i] = $thirdBasketItem;
            }



            $request_iy->setBasketItems($basketItems);
            //dd($request_iy);
            $checkoutFormInitialize = CheckoutFormInitialize::create($request_iy, $options);
            $paymentinput = $checkoutFormInitialize->getCheckoutFormContent();
            //dd($checkoutFormInitialize);
            if ($checkoutFormInitialize->getStatus() == "failure")

            {

                print_r($checkoutFormInitialize->getErrorCode());
                print_r($checkoutFormInitialize->getErrorMessage());

            }
            return view('iyzico.form', compact('paymentinput'));

        } catch(\Exception $ex) {
            return ['success' => false, 'message' => '<span class="text-danger">Kayıt işlemi esnasında bir hata oluştu. Lütfen tekrar deneyiniz. '.$ex->getMessage().'</span>'];
        }
    }

    public function iyzicoSonuc(Request $request2)
    {

        $options = new Options();
        $options->setApiKey('ez8OKMA935Rg6KgMz7D5eAR1836n5TvI');
        $options->setSecretKey('w38KDNWdkFWy768Ny4skazB6iHe5uyto');
        $options->setBaseUrl('https://api.iyzipay.com');
        $token = $request2->token;

        $request = new \Iyzipay\Request\RetrieveCheckoutFormRequest();
        //$request = RetrieveCheckoutFormRequest();
        $request->setLocale(Locale::TR);

        $request->setToken($token);
        $checkoutForm = CheckoutForm::retrieve($request, $options);

        # print result
//print_r($checkoutForm);

        if ($checkoutForm->getStatus() == "success" && $checkoutForm->getPaymentStatus() == "SUCCESS" && $checkoutForm->getInstallment() > 1){
            $basket = Basket::find($checkoutForm->getbasketId());
            $basket->is_paid = 1;
            $basket->save();
            $taksit = true;
            return view('iyzico.odeme_basarili', compact('checkoutForm', 'basket', 'taksit'));
        }

        else if ($checkoutForm->getStatus() == "success" && $checkoutForm->getPaymentStatus() == "SUCCESS"){
            $taksit = false;
            $basket = Basket::find($checkoutForm->getbasketId());
            $basket->is_paid = 1;
            $basket->save();
            return view('iyzico.odeme_basarili', compact('checkoutForm', 'basket', 'taksit'));

        }

        else if ($checkoutForm->getStatus() == "success" && $checkoutForm->getPaymentStatus() != "SUCCESS"){
            return view('iyzico.odeme_hata', compact('checkoutForm'));

        }
        else{
            return view('iyzico.odeme_hata', compact('checkoutForm'));
        }
    }
}
