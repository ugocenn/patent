<?php

namespace App\Http\Controllers;

use App\Helpers\HashingSlug;
use App\Models\Basket;
use App\Models\PackageType;
use App\Models\Package;
use App\Models\Search;
use App\Models\Sector;
use App\Models\Customer;
use Carbon\Carbon;
use DB;
use Log;
use Revolution\Google\Sheets\Facades\Sheets;

class CronController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function excel()
    {
        // müşteri nerede?
        $whereIsCustomer = 'Aramada';

        $searchs = Search::where('excel', 0)->where('package_type_id',1)->get();
        foreach ($searchs as $search) {
            $append = null;
            $basket = Basket::where('search_id', $search->id)->first();
            if(isset($basket)){
                $whereIsCustomer = 'Sepette';
                if($basket->is_paid == 1){
                    $whereIsCustomer = 'Ödeme';
                }
            }
            $customer = Customer::where('id', $search->customer_id)->first();

            unset($append);
            if ($customer) {
                $beforeVisitCount = Search::where('customer_id',$customer->id)->count();
                $beforeVisit = 'HAYIR';
                if($beforeVisitCount > 1){
                    $beforeVisit = 'EVET';
                }
                $sector = Sector::where('id', $search->sector_id)->first();
                if (isset($basket)) {
                    $package = Package::where('id', $basket->packet_id)->first();
                } else {
                    $package = null;
                }

                $package_type = PackageType::where('id', $search->package_type_id)->first();

                $append = [
                    isset($search->mark) ? $search->mark : '',
                    isset($search->basvuru_aciklamasi) ? $search->basvuru_aciklamasi : '',
                    isset($search->sektor_ismi) ? $search->sektor_ismi : '',
                    isset($sector) ? $sector->sinif_degeri : '',
                    isset($customer->isim) ? $customer->isim : '',
                    isset($customer->tel) ? $customer->tel : '' ,
                    Carbon::parse($search->created_at)->format('Y-m-d H:i:s'),
                    isset($search->sonuc_sayisi) ? $search->sonuc_sayisi : '',
                    $customer->iletisim_izni == 1 ? 'EVET' : 'HAYIR',
                    '',
                    "".$customer->id."",
                    $beforeVisit.' ('. $beforeVisitCount  .' Kayıt )',
                    $whereIsCustomer,
                    isset($package) ? $package->name : '',

                    isset($basket) ? (isset($basket->secili_siniflar) ? $basket->secili_siniflar : '') : '',
                    isset($basket) ? $basket->sepet_toplami : '',
                    isset($basket) ? ($basket->ilave_hizmetler ? $basket->ilave_hizmetler : '') : '',

                ];
                if($customer->iletisim_izni == 1){
                    Sheets::spreadsheet(env('POST_SPREADSHEET_ID'))->sheetById(env('POST_SPREADSHEET_ID'))->range('A:R')->append([$append]);
                }else{
                    Sheets::spreadsheet(env('POST_SPREADSHEET_ID2'))->sheetById(env('POST_SPREADSHEET_ID2'))->range('A:R')->append([$append]);
                }

               $b = Search::find($search->id);
               $b->excel = 1;
               $b->save();
                Log::info('Cron gelme zamanı:'. Carbon::now()->format('Y-m-d H:i:s'));
            } else {
                die('sss');
            }


        }

    }

}
