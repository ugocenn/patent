<?php

namespace App\Http\Controllers;

use App\Helpers\HashingSlug;
use App\Http\Requests\PackageType\CreatePackageTypeRequest;
use App\Models\Basket;
use App\Models\Package;
use App\Models\PackageType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PackageTypeController extends Controller
{


    public function index()
    {
        $package_types = PackageType::orderBy('name', 'ASC')->get();

        return view('package_types.index', [
            'package_types' => $package_types
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('package_types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePackageTypeRequest $request)
    {
        try {
            $package_type = new PackageType();
            $package_type->name = $request->name;
            $package_type->slug = $request->slug;
            $package_type->description = $request->description;
            $package_type->icon = $request->icon;
            $package_type->active = $request->active;
            $package_type->save();
            return redirect(route('package-types.index'))->with('success', ' Paket türü bilgileri başarılı bir şekilde sisteme eklenmiştir.');
        } catch(\Exception $ex) {
            return redirect(route('package-types.index'))->with('danger', ' Güncelleme işlemi esnasında bir hata oluştu. Lütfen tekrar deneyiniz. <br/><pre> Hata:'.$ex->getMessage(). '</pre>');
        }


    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd('ss');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = HashingSlug::decodeHash($id);
        $package_type = PackageType::find($id);
        if (!$package_type) {
            return back()->with('warning', 'İlgili kayıt bulunamamıştır. Lütfen kontrol ederek tekrar deneyiniz.');
        }

        return view('package_types.edit', compact('package_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreatePackageTypeRequest $request, $id)
    {

        $id = HashingSlug::decodeHash($id);
        $package_type = PackageType::find($id);

        if (!$package_type) {
            return back()->with('warning', 'İlgili kayıt bulunamamıştır. Lütfen kontrol ederek tekrar deneyiniz.');
        }

        try {
            $package_type->name = $request->name;
            $package_type->slug = $request->slug;
            $package_type->description = $request->description;
            $package_type->icon = $request->icon;
            $package_type->active = $request->active;
            $package_type->save();
        } catch(\Exception $ex) {
            return redirect(route('sectors.index'))->with('danger', ' Güncelleme işlemi esnasında bir hata oluştu. Lütfen tekrar deneyiniz. <br/><pre> Hata:'.$ex->getMessage(). '</pre>');
        }
        return redirect(route('package-types.index'))->with('success', 'Paket türü bilgileri başarılı bir şekilde sisteme güncellenmiştir.');


    }

    public function destroy($id) {

        $id = HashingSlug::decodeHash($id);
        $package_type = PackageType::find($id);
        if (!$package_type) {
            return back()->with('warning', 'İlgili kayıt bulunamamıştır. Lütfen kontrol ederek tekrar deneyiniz.');
        }


        $package = Package::where('package_type_id', $package_type->id)->first();
        if ($package) {
            return back()->with('warning', 'Türe ait paket tanımlaması bulunmaktadır. Paket silim ardından tekrar deneyiniz.');
        }
        $package = Package::where('package_type_id', $package_type->id)->first();
        if ($package) {
            return back()->with('warning', 'Türe ait paket tanımlaması bulunmaktadır. Paket silim ardından tekrar deneyiniz.');
        }
        $basket = Basket::where('package_type_id', $package_type->id)->first();
        if ($basket) {
            return back()->with('warning', 'Türe ait sepet tanımlaması bulunmaktadır. Sepet silim ardından tekrar deneyiniz.');
        }

        $package_type->forceDelete();






        return redirect(route('package-types.index'))->with('success', 'Kayıt başarılı bir şekilde silinmiştir.');
    }
}
