<?php

namespace App\Http\Controllers;


use App\Helpers\HashingSlug;
use App\Http\Requests\Customer\CreateCustomerRequest;
use App\Models\Basket;
use App\Models\Customer;
use App\Models\Search;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Revolution\Google\Sheets\Facades\Sheets;

class CustomerController extends Controller
{




    public function index()
    {
        $customers = Customer::orderBy('isim', 'ASC')->get();
        return view('customers.index', [
            'customers' => $customers
        ]);
    }

//https://docs.google.com/spreadsheets/d/1rF53Ynfp80-NthXv94kkcgAfyFqt94kvlZUwBtyQqt4/edit?usp=sharing
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCustomerRequest $request)
    {


    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateCustomerRequest $request, $id)
    {


    }

    public function destroy($id) {

        $id = HashingSlug::decodeHash($id);
        $customer = Customer::find($id);
        if (!$customer) {
            return back()->with('warning', 'İlgili kayıt bulunamamıştır. Lütfen kontrol ederek tekrar deneyiniz.');
        }

        $basket = Basket::where('customer_id', $customer->id)->first();

        if ($basket) {
            return back()->with('warning', 'İlgili müşteriye ait sepet bulunmuştur. Lütfen öncelikle sepeti silerek tekrar deneyiniz.');
        }
        Search::where('customer_id', $customer->id)->forceDelete();

        return redirect(route('customers.index'))->with('success', 'Kayıt başarılı bir şekilde silinmiştir.');
    }
}
