<?php
namespace App\Services\Employee;
use App\Models\Department;
use App\Models\Employee;
use App\Models\ProfessionCode;

class EmployeeService {

    protected $employee;

    /**
     * EmployeeService constructor.
     * @param Employee $employee
     */
    public function __construct(Employee $employee){
        $this->employee = $employee;
    }

    public function getAllDepartments(){
        return Department::where('company_id',auth()->user()->company_id)->pluck('name', 'id');
    }

    public function getAllEmployees(){
        return Employee::where('company_id',auth()->user()->company_id)->pluck('name', 'id');
    }


    public function storeEmployee($request){
        $job_code_array = explode(' - ', $request->job_name);
        if (count($job_code_array) == 2) {
            $has = ProfessionCode::where('name', $job_code_array[0])->where('isco', $job_code_array[1])->first();
            if ($has) {
                $job_name = $has->id;
            } else {
                die('job_codes');
            }
        } else {
            die('job_codes');
        }

        $employee = new $this->employee;
        $employee->company_id = auth()->user()->company_id;
        $employee->registration_number = $request->registration_number;
        $employee->name = egt_turk_Ucword($request->name);
        $employee->identity_number = $request->identity_number;
        $employee->working_status = $request->working_status;
        $employee->department_code = $request->department_code;
        $employee->start_date = ezey_get_dateformat($request->start_date, 'toThem');
        $employee->gender = $request->gender;
        $employee->education = $request->education;
        $employee->cost_number = $request->cost_number;
        $employee->department_id = $request->department_id;
        $employee->job = $request->job;
        $employee->job_name = $job_name;
        $employee->chief_id = $request->chief_id > 0 ? $request->chief_id : 0;
        $employee->email = $request->email;
        $employee->birth_date = ezey_get_dateformat($request->birth_date, 'toThem');
        $employee->save();
        return $employee;
    }

    public function updateEmployee($request, $id){

        $employee = Employee::find($id);
        $job_code_array = explode(' - ', $request->job_name);
        if (count($job_code_array) == 2) {
            $has = ProfessionCode::where('name', $job_code_array[0])->where('isco', $job_code_array[1])->first();
            if ($has) {
                $job_name = $request->job_name;
            } else {
                $job_name = $employee->job_name;
            }
        } else {
            $job_name = $employee->job_name;
        }


        $employee->registration_number = $request->registration_number;
        $employee->name = egt_turk_Ucword($request->name);
        $employee->identity_number = $request->identity_number;
        $employee->working_status = $request->working_status;
        $employee->department_code = $request->department_code;
        $employee->start_date = ezey_get_dateformat($request->start_date, 'toThem');
        $employee->gender = $request->gender;
        $employee->education = $request->education;
        $employee->cost_number = $request->cost_number;
        $employee->department_id = $request->department_id;
        $employee->job = $request->job;
        $employee->chief_id = $request->chief_id;
        $employee->job_name = $job_name;
        $employee->email = $request->email;
        $employee->birth_date = ezey_get_dateformat($request->birth_date, 'toThem');
        $employee->save();
        return $employee;
    }



}
