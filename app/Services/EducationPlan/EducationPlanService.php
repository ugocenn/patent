<?php
namespace App\Services\EducationPlan;
use App\Models\Department;
use App\Models\Education;
use App\Models\EducationPlan;
use App\Models\Employee;
use App\Models\Firm;

class EducationPlanService {

    protected $education_plan;

    /**
     * EducationPlanService constructor.
     * @param EducationPlan $education_plan
     */
    public function __construct(EducationPlan $education_plan){
        $this->education_plan = $education_plan;
    }

    /**
     * @return mixed
     */
    public function getAllEducations(){
        return Education::where('company_id',auth()->user()->company_id)->pluck('name', 'id');
    }

    /**
     * @return mixed
     */
    public function getAllEducationScopes(){
        $departments = Department::where('company_id',auth()->user()->company_id)->pluck('name', 'id');
        $defaultSelection = ['0' => ' TÜM DEPARTMANLARA'];
        return $defaultSelection + $departments->toArray();

    }


    /**
     * @return mixed
     */
    public function getAllDepartments(){
        return Department::where('company_id',auth()->user()->company_id)->pluck('name', 'id');
    }

    /**
     * @return mixed
     */
    public function getAllFirms(){
        return Firm::where('company_id',auth()->user()->company_id)->pluck('name', 'id');
    }


    /**
     * @return mixed
     */
    public function getAllEmployees(){
        return Employee::where('company_id',auth()->user()->company_id)->pluck('name', 'id');
    }




}
