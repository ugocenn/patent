<?php
namespace App\Services\User;
use App\Models\User;

class UserService {

    protected $user;

    /**
     * UserService constructor.
     * @param User $user
     */
    public function __construct(User $user){
        $this->user = $user;
    }

    /**
     * @param $view
     * @param $users
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function indexView($view, $users){
        return view($view, [
            'users' => $users
        ]);
    }

    public function getUsers(){
        if( auth()->user()->role == 'superadmin') {
            return $this->user->where('company_id', auth()->user()->company_id)->orderBy('name', 'asc')
                ->get();
        }
        return $this->user->where('company_id', auth()->user()->company_id)->whereNotIn('role', ['superadmin'])
            ->orderBy('name', 'asc')
            ->get();
    }

    public function storeUser($request){
        $tb = new $this->user;
        $tb->name = $request->name;
        $tb->email = (!empty($request->email)) ? $request->email : '';
        $tb->password = bcrypt($request->password);
        $tb->role = $request->role;
        $tb->active = $request->active;
        $tb->pic_path = (!empty($request->pic_path)) ? $request->pic_path : '';
        $tb->verified = 1;
        $tb->company_id = 1;
        $tb->save();
        return $tb;
    }


}
