<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class AdditionalService extends Model
{
    protected $fillable = ['name', 'type', 'description', 'price', 'active'];
    protected $casts = [
        'additional_services' => 'array',
    ];

}
