<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = ['title','short_text', 'content', 'slug', 'image', 'location', 'active'];


    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }
}
