<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Search extends Model
{
    protected $table = 'searchs';

    protected $dates = [
        'search_date',
    ];

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }
    public function package_type()
    {
        return $this->belongsTo('App\Models\PackageType');
    }


}
