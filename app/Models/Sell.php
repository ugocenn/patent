<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Sell extends Model
{
    protected $fillable = ['marka', 'basvuru_aciklamasi', 'basvuru_turu', 'inputName', 'inputTCNo', 'inputEmail', 'inputTelefon', 'address', 'fatura_turu', 'sektorOrSinif', 'sozlesme_onay', 'current_packet', 'current_service_price', 'class_count', 'as_count', 'sinif_ucreti', 'ilave_sinif_ucretleri', 'ilave_hizmet_ucretleri', 'basvuru_ucreti', 'kdv', 'total', 'sectors_yeni', 'classes_yeni', 'services_yeni', 'op_date', 'is_paid'];

    protected $dates = [
        'created_at',
    ];

}
