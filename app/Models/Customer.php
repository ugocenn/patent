<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';



    public function searchs()
    {
        return $this->hasMany('App\Models\Search') ;
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Order') ;
    }



}
