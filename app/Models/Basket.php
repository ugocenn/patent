<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Basket extends Model
{
    protected $table = 'baskets';

    protected $dates = [
        'created_at',
    ];

    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }

    function getBazPrice($type){
        $paket = Package::where('package_type_id', $type)->orderby('price','ASC')->first();

        return $paket;
    }

    function getPacket($id){
        $paket = Package::where('id', $id)->first();
        return $paket;
    }

    function findPackageName($id){
        $paket = Package::where('id', $id)->first();
        return $paket;
    }

    function getSector($id){
        $sektor = Sector::find($id);
        if ($sektor) {
            return $sektor;
        } else {
            return new Sector();
        }

    }

}
