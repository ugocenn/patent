<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name','description', 'metadescription', 'keywords', 'text1', 'text2'];

}
