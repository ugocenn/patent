<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $table = 'packages';






    public function getSectors($str = '')
    {
       $r = explode(',', $this->sector_lists);
       $a = Sector::whereIn('id', $r)->pluck('name')->toArray();
       $t = '';
       if ($str == 'text') {
           foreach ($a as $value) {
               $t .= '<span class="service-name">.com.tr Domain Hizmeti</span>
                                    <span class="service-name">Marka İzleme Hizmeti</span>';
           }
           return '<div class="d-flex flex-column mt-4 mb-3"><span class="description">İLAVE HİZMETLER</span>'.$t.'</div>';
       }
       return implode(', ', $a);
    }

    public function getServices($str = 'name')
    {
        $r = json_decode($this->additional_services);

        $idCats = array();
        if(isset($r)){
            foreach ($r as $item) {
                array_push($idCats,$item->id);
            }

        }

        if ($str == 'price') {
            $a = AdditionalService::whereIn('id', $idCats)->sum('price');
            return $a;
        }

        $t = '';
        if ($str == 'text') {
            $a = AdditionalService::whereIn('id', $idCats)->pluck('short_name')->toArray();
            foreach ($a as $value) {
                $t .= '<span class="service-name">'.$value.'</span>';
            }
            if (strlen($t) > 0) {
                //return '<div class="d-flex flex-column mt-4 mb-3"><span class="description">İLAVE HİZMETLER</span>'.$t.'</div>';
                return $t;
            }
            return '';
        }

        $a = AdditionalService::whereIn('id', $idCats)->pluck('short_name')->toArray();
        return implode(', ', $a);
    }


    public function getPrice($type = 'class')
    {
        if ($type == 'class') {
            return 0;
        }

        if ($type == 'service') {
            return 0;
        }

    }

    public function getTotal($type = 'TOTAL', $a = 0, $b = 0, $c = 0)
    {
        $t = $a + $b + $c;
        $kdv = $t * 0.18;
        if ($type == 'KDV') {
            return $kdv;
        }
        return $t + $kdv;

    }

    public function package_type()
    {
        return $this->belongsTo('App\Models\PackageType');
    }


}
