<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';



    public function baskets()
    {
        return $this->hasMany('App\Models\Basket') ;
    }
    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }


}
