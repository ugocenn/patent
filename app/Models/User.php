<?php

namespace App\Models;

use App\Helpers\ImageHelper;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get picture url of user
     *
     * @return null|string
     */
    public function getPictureLinkAttribute()
    {
        if (! $this->pic_path) {
            $imageHelper = new ImageHelper();
            $imageHelper->createUserImage($this);
        }
        return $this->pic_path ? str_replace('uploads/', '', $this->pic_path) : asset('assets/images/avatar_2x.png');
    }

    public function getTotalEmployee()
    {
        return Employee::where('company_id', $this->company_id)->count();
    }

    public function company()
    {
        return $this->belongsTo('App\Models\Company');
    }

}
