<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSellsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sells', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('marka')->nullable();
            $table->string('inputName')->nullable();
            $table->string('inputTCNo')->nullable();
            $table->string('inputEmail')->nullable();
            $table->string('inputTelefon')->nullable();
            $table->text('address')->nullable();
            $table->string('fatura_turu')->nullable();
            $table->string('sektorOrSinif')->nullable();
            $table->string('sozlesme_onay')->nullable();
            $table->tinyInteger('current_packet')->nullable();
            $table->decimal('current_service_price', 10, 2)->nullable();
            $table->tinyInteger('class_count')->nullable();
            $table->tinyInteger('as_count')->nullable();
            $table->decimal('sinif_ucreti', 10, 2)->nullable();
            $table->decimal('ilave_sinif_ucretleri', 10, 2)->nullable();
            $table->decimal('ilave_hizmet_ucretleri', 10, 2)->nullable();
            $table->decimal('basvuru_ucreti', 10, 2)->nullable();
            $table->decimal('kdv', 10, 2)->nullable();
            $table->decimal('total', 10, 2)->nullable();
            $table->string('sectors_yeni')->nullable();
            $table->string('classes_yeni')->nullable();
            $table->string('services_yeni')->nullable();
            $table->date('op_date')->nullable();
            $table->tinyInteger('is_paid')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sells');
    }
}
